﻿using Microsoft.AspNetCore.Mvc;
using Phoenix.Api.Shared.Controllers;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Areas.Hris.Dtos;
using Phoenix.Shared.Core.Areas.Hris.Services;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Api.Areas.Hris.Controllers
{
    [Area(PhoenixModule.HRIS)]
    [Route(PhoenixModule.HRIS_ROUTE_API)]
    public class EmployeeController : BaseApiController<HrEmployeeBasicInfoDto, HremployeeBasicInfo, string>
    {
        private IHrEmployeeBasicInfoService _service;

        public EmployeeController(IHrEmployeeBasicInfoService service) : base(service)
        {
            _service = service;
        }

        //[HttpGet]
        // public IList<HrEmployeeBasicInfo> Search(PaginateExpressionParameter<HrEmployeeBasicInfo> parameter) 
        //{
        //
        //    return _service.SearchAll(parameter);
        //}

        [HttpGet]
        public GeneralResponseList<HremployeeBasicInfo> Search(SearchParameter parameter)
        {

            return _service.SearchAll(parameter);
        }
    }
}
