﻿using Microsoft.Extensions.DependencyInjection;
using Phoenix.Shared.Core.Areas.Hris.Repositories;
using Phoenix.Shared.Core.Areas.Hris.Services;

namespace Phoenix.Api.Areas.Hris
{
    public class ConfigServicesHris
    {
        public static void Init(IServiceCollection services)
        {
            /*
             * this repository only, initial repository first before service
             */
            services.AddScoped<IHrEmployeeBasicInfoRepository, HrEmployeeBasicInfoRepository>();


            /*
             * this service only
             */
            services.AddScoped<IHrEmployeeBasicInfoService, HrEmployeeBasicInfoService>();
        }
    }
}