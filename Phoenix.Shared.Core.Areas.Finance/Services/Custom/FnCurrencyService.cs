﻿using Phoenix.Shared.Core.Areas.Finance.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Core.Areas.Finance.Dtos.Response;

namespace Phoenix.Shared.Core.Areas.Finance.Services
{
    public partial interface IFnCurrencyService : IBaseService<FnCurrencyDto, FnCurrencySample, string>
    {
        GeneralResponse<FnCurrencyResponseDto> SearchSample(SearchParameter parameter);
        GeneralResponse<FnCurrencyResponseDto> SearchSampleWithQuery(SearchParameter parameter);
        GeneralResponseList<FnCurrencySample> SearchAll(SearchParameter parameter);
        FnCurrencySample SampleTransactionCreate(FnCurrencyDto data, string[] excepts);
        GeneralResponse<FnCurrencySample> SampleTransactionCreateWithResponse(FnCurrencyDto data, string[] excepts);
        GeneralResponse<FnCurrencySample> SampleTransactionCreateResponse(FnCurrencyDto data, string[] excepts);
        GeneralResponse<FnCurrencySample> SaveSampleWithoutTransaction(FnCurrencyDto dto);
    }
    public partial class FnCurrencyService : BaseService<FnCurrencyDto, FnCurrencySample, string>, IFnCurrencyService
    {
        public GeneralResponse<FnCurrencySample> SampleTransactionCreateWithResponse(FnCurrencyDto data, string[] excepts)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            TransactionProcess transactionProcess = delegate
            {
                GeneralResponse<FnCurrencySample> respTransaction = new GeneralResponse<FnCurrencySample>();
                try
                {
                    FnCurrencySample newData = NewInstance();
                    FnCurrencySample newData2 = NewInstance();
                    FnCurrencySample newData3 = NewInstance();

                    int x = 0;
                    
                    PropertyMapper.Except(excepts, data, newData);
                    PropertyMapper.Except(excepts, data, newData2);
                    PropertyMapper.Except(excepts, data, newData3);
                    _repo.Create(newData);
                    _repo.Create(newData2);
                    if (x == 0)
                    {
                        throw new Exception("Ga boleh 0");
                    }
                    respTransaction.Data = _repo.Create(newData3);
                    respTransaction.Success = true;
                }
                catch (Exception ex)
                {
                    respTransaction.Message = ex.Message;
                    respTransaction.Success = false;
                }
                return respTransaction;
            };

            resp = ProcessingTransaction<GeneralResponse<FnCurrencySample>>(transactionProcess);
            return resp;
        }

        public GeneralResponse<FnCurrencySample> SaveSampleWithoutTransaction(FnCurrencyDto dto)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                if (dto.NameCurrency == "Cent")
                {
                    throw new Exception("Ga boleh Cent");
                }
                resp.Data = Create(dto);
                resp.Message = "success";
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }

        public GeneralResponse<FnCurrencyResponseDto> SearchSample(SearchParameter parameter)
        {
            GeneralResponse<FnCurrencyResponseDto> response = new GeneralResponse<FnCurrencyResponseDto>();
            FnCurrencyResponseDto data = new FnCurrencyResponseDto();
            ExpressionParameter<FnCurrencySample> parentParameter = new ExpressionParameter<FnCurrencySample>();
            parentParameter.where = o => o.NameCurrency.Contains(parameter.GetSearch());
            data.parent = _repo.FindFirst(parentParameter);

            ExpressionParameter<FnCurrencySample> childsParameter = new ExpressionParameter<FnCurrencySample>();
            childsParameter.where = o => o.NameCurrency.Contains(parameter.GetSearch());
            data.childs = _repo.FindAll(childsParameter);
            response.Data = data;

            return response;
        }

        public GeneralResponse<FnCurrencyResponseDto> SearchSampleWithQuery(SearchParameter parameter)
        {
            GeneralResponse<FnCurrencyResponseDto> response = new GeneralResponse<FnCurrencyResponseDto>();
            FnCurrencyResponseDto data = new FnCurrencyResponseDto();
            QueryParameter<FnCurrencySample> parentParameter = new QueryParameter<FnCurrencySample>();
            parentParameter.dbQuery = from cust in _repo.GetContext().Set<FnCurrencySample>() 
                where cust.NameCurrency == parameter.GetSearch()
                select cust;
            data.parent = _repo.FindFirst(parentParameter);

            ExpressionParameter<FnCurrencySample> childsParameter = new ExpressionParameter<FnCurrencySample>();
            childsParameter.where = o => o.NameCurrency.Contains(parameter.GetSearch());
            data.childs = _repo.FindAll(childsParameter);
            response.Data = data;

            return response;
        }

        public GeneralResponseList<FnCurrencySample> SearchAll(SearchParameter parameter)
        {
            GeneralResponseList<FnCurrencySample> resp = new GeneralResponseList<FnCurrencySample>();
            try
            {
                PaginateExpressionParameter<FnCurrencySample> param = new PaginateExpressionParameter<FnCurrencySample>();
                param.where = o => o.NameCurrency.Contains(parameter.GetSearch());
                param.limit = parameter.GetLimit();
                param.offset = parameter.GetOffset();
                resp.Rows = _repo.FindAll(param);
                resp.RecordsTotal = resp.Rows.Count;
                resp.RecordsFiltered= _repo.Count(param);
                resp.Success = true;
            }
            catch (Exception ex) {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }

        public FnCurrencySample SampleTransactionCreate(FnCurrencyDto data, string[] excepts)
        {
            FnCurrencySample resp = new FnCurrencySample();
            TransactionProcess transactionProcess = delegate
            {
                FnCurrencySample newData = NewInstance();
                
                    
                    int x = 0;
                    if (x == 0)
                    {
                        throw new Exception("");
                    }
                    PropertyMapper.Except(excepts, data, newData);
                
                return _repo.Create(newData);
            };

            resp = ProcessingTransaction<FnCurrencySample>(transactionProcess);
            return resp;
        }

        public GeneralResponse<FnCurrencySample> SampleTransactionCreateResponse(FnCurrencyDto data, string[] excepts)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                resp.Data = SampleTransactionCreate(data, excepts);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;

        }
    }
}
