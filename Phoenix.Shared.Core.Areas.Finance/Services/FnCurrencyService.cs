﻿using Phoenix.Shared.Core.Areas.Finance.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.Finance.Services
{
    public partial interface IFnCurrencyService : IBaseService<FnCurrencyDto, FnCurrencySample, string>
    {
        
    }
    public partial class FnCurrencyService : BaseService<FnCurrencyDto, FnCurrencySample, string>, IFnCurrencyService
    {
        public FnCurrencyService(IEFRepository<FnCurrencySample, string> repo):base(repo)
        {
            _repo = repo;
        }
    }
}
