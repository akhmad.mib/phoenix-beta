﻿using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.Finance.Repositories
{
    public partial interface IFnCurrencyRepository : IEFRepository<FnCurrencySample,string>
    {
    }
    public partial class FnCurrencyRepository : EFRepository<FnCurrencySample, string>, IFnCurrencyRepository
    {
    }
}
