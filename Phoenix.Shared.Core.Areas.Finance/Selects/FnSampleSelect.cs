﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.Finance.Selects
{
    public class FnSampleSelect
    {
        public long CurrencyId {get;set;}
        public string CurrencyName { get; set; }
    }
}
