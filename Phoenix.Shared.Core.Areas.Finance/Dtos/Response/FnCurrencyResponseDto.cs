﻿using Phoenix.Shared.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.Finance.Dtos.Response
{
    public class FnCurrencyResponseDto
    {
        public FnCurrencySample parent { get; set; }
        public IList<FnCurrencySample> childs { get; set; }
    }
}
