﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Newtonsoft.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using Phoenix.Shared.Core.Areas.Finance.Repositories;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Phoenix.Api.Areas.Finance;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Libraries.SnowFlake;
using Phoenix.Shared.Core.Areas.Finance.Services;
using Phoenix.Api.Areas.ProjectManagement;
using Phoenix.Api.Areas.Hris;

namespace Phoenix.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EFContext>(options =>
                    //options.UseSqlServer(Configuration.GetConnectionString("AppDB"), ServiceLifetime.Transient)
                    //options.UseSqlServer(Configuration.GetConnectionString("AppDB")), ServiceLifetime.Scoped);
                    options.UseSqlServer(Configuration.GetConnectionString("AppDB")));
            //           );
            services.AddSingleton<IdWorker, IdWorker>();
            services.AddSingleton<Id64Generator, Id64Generator>();
            services.AddScoped<EFContext, Phoenix.Shared.Core.Entities.AppDbContext>();
            ConfigServicesFinance.Init(services);
            ConfigServicesProjectManagement.Init(services);
            ConfigServicesHris.Init(services);
            services.AddScoped(typeof(IEFRepository<,>), typeof(EFRepository<,>));

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Phoenix API",
                    Description = "A complete docs example Web API for Phoenix Apps",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Muhammad Tri Wibowo", Email = "", Url = "https://facebook.com/muhammad.wibowo" },
                    License = new License { Name = "Use under LICX", Url = "https://example.com/license" }
                });

                // Set the comments path for the Swagger JSON and UI.
//                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
//                var xmlPath = Path.Combine(basePath, "TodoApi.xml"); 
//                c.IncludeXmlComments(xmlPath);                
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                        builder =>
                        {
                            builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials();
                        });
                options.AddPolicy("AllowAllOrigins",
                        builder =>
                        {
                            builder.AllowAnyOrigin();
                        });
                options.AddPolicy("AllowAllHeaders",
                        builder =>
                        {
                            builder
                                    //.WithOrigins("http://example.com")
                                   .AllowAnyHeader();
                        });
                options.AddPolicy("AllowCredentials",
                        builder =>
                        {
                            builder
                                    //.WithOrigins("http://example.com")
                                   .AllowCredentials();
                        });
            });
            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowAll"));
            });
            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseMvc();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "area",
                    template: "api/{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "api/{controller=Home}/{action=Index}/{id?}");
            });

            app.UseCors("AllowAll");
            app.UseSwagger();
            app.UseStaticFiles();
            app.UseSwaggerUI(c =>
            {
                c.InjectOnCompleteJavaScript("../swagger-bearer-auth.js");
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
            });
        }
    }
}
