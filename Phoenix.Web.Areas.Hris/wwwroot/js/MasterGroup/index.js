﻿$(function () {
    InitTable(tableIdIndex, ajaxUrlIndex, paramsSearchIndex, columnsIndex);
});

var ajaxUrlIndex = API_FINANCE + "/CurrencyBase/Search";
var ajaxUrlForm = WEB_FINANCE + "/MasterGroup/Form";
var ajaxUrlSave = API_FINANCE + "/CurrencyBase/Create";
var ajaxUrlUpdate = API_FINANCE + "/CurrencyBase/Update";
var ajaxUrlGetId = "/CurrencyBase/Get";
var ajaxUrlDeleteId = "/CurrencyBase/Delete";

var tableIdIndex = "tableIndex";
var formIdIndex = "formIndex";
var searchBoxIdIndex = "searchBoxIndex";

var columnsIndex = [
    {
        "data": "Id",
        "title": "ID",
        "sClass": "ecol x20",
        orderable: false
    },
    {
        "data": "CodeCurrency",
        "title": "Code",
        "sClass": "lcol",
        orderable: false,
        "render": function (data, type, row) {
            return '<a href="javascript:void(0)" data-role="detail" data-id="' + row.id + '">' + data + '</a>';
        }
    },
    {
        "data": "NameCurrency",
        "title": "Name",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "Id",
        "title": "Action",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return "<a href='javascript:void(0)' class='btn btn-default' onclick=\"EditFormIndex('" + data + "');\"><span title='Ubah' class='glyphicon glyphicon-edit'></span>Edit</a> " +
                " <a href='javascript:void(0)' class='btn btn-danger' onclick=\"DeleteFormIndex('" + row.Id + "');\"><span title='Hapus' class='glyphicon glyphicon-trash'></span>Delete</a>";
        }
    }
];

var paramsSearchIndex = function () {
    var search = $('#searchTextIndex').val();
    var startDate = $("#searchTextStartDateIndex").val();
    var endDate = $("#searchTextEndDateIndex").val();
    return { search: search, startDateParam: startDate, endDateParam: endDate };
}




function ShowSearchTableIndex() {
    showCustomModal(searchBoxIdIndex);
}
function SearchTableIndex() {
    ReloadGridIndex();
    hideAllModal();
}

function ReloadGridIndex() {
    ReloadGrid(tableIdIndex);
}

