﻿using System;
using System.Net;

namespace Phoenix.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = @"http://localhost:62916/api/auth/cek";
            string credentials = "FeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFzZCIsIm5iZiI6MTUwOTcxODM2MSwiZXhwIjoxNTA5NzE5NTYxLCJpYXQiOjE1MDk3MTgzNjF9.yOHqPNdw4mzxwoOVawXmrpbZLSFeo3qmnqdG90k0mxM";
            WebClient webClient = new WebClient();            
            webClient.Headers[HttpRequestHeader.Authorization] = "pToken " +credentials;

            var result = webClient.DownloadString(url);

            Console.Write(result);
            Console.ReadKey();
        }
    }
}
