﻿using System;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Api.Shared.Controllers;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Areas.ProjectManagement.Services;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos.Response;

namespace Phoenix.Api.Areas.ProjectManagement.Controllers
{
    [Area(PhoenixModule.PM)]
    [Route(PhoenixModule.PM_ROUTE_API)]
    public class ClientBriefController : BaseApiController<PmClientBriefDto, PmClientBrief, string>
    {

        private IPmClientBriefService _service;

        public ClientBriefController(IPmClientBriefService service) : base(service)
        {
            _service = service;
        }


        [HttpGet]
        public GeneralResponseList<PmClientBrief> Search(SearchParameter parameter)
        {
            return _service.SearchAll(parameter);
        }

        [HttpGet]
        public GeneralResponse<PmClientBriefResponseDto> SearchSample(SearchParameter parameter)
        {
            return _service.SearchSample(parameter);
        }

        [HttpGet("{id}")]
        public GeneralResponse<PmClientBrief> Get(string id)
        {
            GeneralResponse<PmClientBrief> resp = new GeneralResponse<PmClientBrief>();
            try
            {
                resp.Data = _service.GetByID(id);
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        [HttpPost]
        public GeneralResponse<PmClientBriefResponseDto> CreateMultipleTable(PmClientBriefResponseDto dto)
        {

            GeneralResponse<PmClientBriefResponseDto> resp = new GeneralResponse<PmClientBriefResponseDto>();
            try
            {
                resp = _service.TransactionMultipleCreate(dto, GetCreateExcepts());
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        private string[] GetCreateExcepts()
        {
            return new string[] { "IsCreated" };
        }

        [HttpPost]
        public GeneralResponse<PmClientBrief> Update(PmClientBriefDto dto)
        {
            GeneralResponse<PmClientBrief> resp = new GeneralResponse<PmClientBrief>();
            try
            {
                resp.Data = _service.UpdateWithExcept(dto.Id, dto, GetUpdateExcepts());
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        private string[] GetUpdateExcepts()
        {
            return new string[] { "Id", "Isdeleted" };
        }

        [HttpGet("{id}")]
        public GeneralResponse<PmClientBrief> Delete(string id)
        {
            GeneralResponse<PmClientBrief> resp = new GeneralResponse<PmClientBrief>();
            try
            {
                _service.DeleteByID(id);
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }
    }
}