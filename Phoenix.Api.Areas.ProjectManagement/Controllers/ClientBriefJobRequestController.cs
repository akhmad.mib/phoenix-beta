﻿using System;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Areas.ProjectManagement.Services;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Constants;

namespace Phoenix.Api.Areas.ProjectManagement.Controllers
{
    [Area(PhoenixModule.PM)]
    [Route(PhoenixModule.PM_ROUTE_API)]
    public class ClientBriefJobRequestController : Controller
    {
        private IPmClientBriefJobRequestService _service;

        public ClientBriefJobRequestController(IPmClientBriefJobRequestService service)
        {
            _service = service;
        }

        [HttpGet]
        public GeneralResponseList<PmClientBriefJobRequest> Search(SearchParameter parameter)
        {
            return _service.SearchAll(parameter);
        }

        [HttpGet("{id}")]
        public GeneralResponse<PmClientBriefJobRequest> Get(string id)
        {
            GeneralResponse<PmClientBriefJobRequest> resp = new GeneralResponse<PmClientBriefJobRequest>();
            try
            {
                resp.Data = _service.GetByID(id);
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        [HttpPost]
        public GeneralResponse<PmClientBriefJobRequest> Create(PmClientBriefJobRequestDto dto)
        {
            GeneralResponse<PmClientBriefJobRequest> resp = new GeneralResponse<PmClientBriefJobRequest>();
            try
            {
                resp.Data = _service.CreateWithExcept(dto, GetCreateExcepts());
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }


        private string[] GetCreateExcepts()
        {
            return new string[] { "Isdeleted" };
        }

        [HttpPost]
        public GeneralResponse<PmClientBriefJobRequest> Update(PmClientBriefJobRequestDto dto)
        {
            GeneralResponse<PmClientBriefJobRequest> resp = new GeneralResponse<PmClientBriefJobRequest>();
            try
            {
                resp.Data = _service.UpdateWithExcept(dto.Id, dto, GetUpdateExcepts());
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        private string[] GetUpdateExcepts()
        {
            return new string[] { "Id", "Isdeleted" };
        }

        [HttpGet("{id}")]
        public GeneralResponse<PmClientBriefAccount> Delete(string id)
        {
            GeneralResponse<PmClientBriefAccount> resp = new GeneralResponse<PmClientBriefAccount>();
            try
            {
                _service.DeleteByID(id);
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }
    }
}