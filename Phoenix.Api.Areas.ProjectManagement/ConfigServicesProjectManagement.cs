﻿using Microsoft.Extensions.DependencyInjection;
using Phoenix.Shared.Core.Areas.ProjectManagement.Repositories;
using Phoenix.Shared.Core.Areas.ProjectManagement.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Phoenix.Api.Areas.ProjectManagement
{
    public class ConfigServicesProjectManagement
    {
        public static void Init(IServiceCollection services)
        {
            services.AddScoped<IPmClientBriefRepository, PmClientBriefRepository>();
            services.AddScoped<IPmClientBriefAccountRepository, PmClientBriefAccountRepository>();
            services.AddScoped<IPmClientBriefContentRepository, PmClientBriefContentRepository>();
            services.AddScoped<IPmClientBriefJobRequestRepository, PmClientBriefJobRequestRepository>();

            services.AddScoped<IPmClientBriefService, PmClientBriefService>();
            services.AddScoped<IPmClientBriefAccountService, PmClientBriefAccountService>();
            services.AddScoped<IPmClientBriefContentService, PmClientBriefContentService>();
            services.AddScoped<IPmClientBriefJobRequestService, PmClientBriefJobRequestService>();

        }
    }
}
