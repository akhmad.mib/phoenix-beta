﻿using Microsoft.Extensions.DependencyInjection;
using Phoenix.Shared.Core.Areas.Finance.Repositories;
using Phoenix.Shared.Core.Areas.Finance.Services;

namespace Phoenix.Api.Areas.Finance
{
    public class ConfigServicesFinance
    {
        public static void Init(IServiceCollection services)
        {
            services.AddScoped<IFnCurrencyRepository, FnCurrencyRepository>();
            services.AddScoped<IFnCurrencyService, FnCurrencyService>();
        }
    }
}