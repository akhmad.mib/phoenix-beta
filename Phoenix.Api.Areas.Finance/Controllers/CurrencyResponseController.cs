﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Areas.Finance.Services;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Core.Areas.Finance.Dtos;

namespace Phoenix.Api.Areas.Finance.Controllers
{
    [Area(PhoenixModule.FINANCE)]
    [Route(PhoenixModule.FINANCE_ROUTE_API)]
    public class CurrencyResponseController : Controller
    {

        private IFnCurrencyService _service;

        public CurrencyResponseController(IFnCurrencyService service)
        {
            _service = service;
        }

        [HttpGet]
        public GeneralResponseList<FnCurrencySample> Search(SearchParameter parameter)
        {
            return _service.SearchAll(parameter);
        }

        [HttpGet("{id}")]
        public GeneralResponse<FnCurrencySample> Get(string id)
        {
            return _service.GetByIDResponse(id);
        }

        [HttpPost]
        public GeneralResponse<FnCurrencySample> Create(FnCurrencyDto dto)
        {
            bool isTransaction = true;
            if (isTransaction)
            {
                return _service.SampleTransactionCreateResponse(dto, GetCreateExcepts());
            }
            else
            {
                return _service.CreateWithExceptResponse(dto, GetCreateExcepts());
            }


        }

        [HttpPost]
        public GeneralResponse<FnCurrencySample> CreateMassal(FnCurrencyDto dto)
        {
            bool isTransaction = true;
            for (int x = 0; x < 9; x++)
            {
                _service.SampleTransactionCreateResponse(dto, GetCreateExcepts());
            }
            if (isTransaction)
            {
                return _service.SampleTransactionCreateResponse(dto, GetCreateExcepts());
            }
            else
            {
                return _service.CreateWithExceptResponse(dto, GetCreateExcepts());
            }


        }

        private string[] GetCreateExcepts()
        {
            return new string[] { "Isdeleted" };
        }

        [HttpPost]
        public GeneralResponse<FnCurrencySample> Update(FnCurrencyDto dto)
        {
            return _service.UpdateWithExceptResponse(dto.Id, dto, GetUpdateExcepts());
        }

        private string[] GetUpdateExcepts()
        {
            return new string[] { "Id", "Isdeleted" };
        }

        [HttpGet("{id}")]
        public GeneralResponse<FnCurrencySample> Delete(string id)
        {
            return _service.DeleteByIDResponse(id);
        }


    }
}