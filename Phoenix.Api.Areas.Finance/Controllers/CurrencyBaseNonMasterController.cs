﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Api.Shared.Controllers;
using Phoenix.Shared.Core.Areas.Finance.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Areas.Finance.Services;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Areas.Finance.Dtos.Response;

namespace Phoenix.Api.Areas.Finance.Controllers
{
    [Area(PhoenixModule.FINANCE)]
    [Route(PhoenixModule.FINANCE_ROUTE_API)]
    public class CurrencyBaseNonMasterController : BaseApiController<FnCurrencyDto,FnCurrencySample,string>
    {

        private IFnCurrencyService _service;

        public CurrencyBaseNonMasterController(IFnCurrencyService service) : base(service)
        {
            _service = service;
        }


        [HttpGet]
        public GeneralResponseList<FnCurrencySample> Search(SearchParameter parameter)
        {
            return _service.SearchAll(parameter);
        }

        [HttpGet]
        public GeneralResponse<FnCurrencyResponseDto> SearchSample(SearchParameter parameter)
        {
            return _service.SearchSample(parameter);
        }

    }
}