﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Api.Areas.Finance.Models;
using Phoenix.Shared.Constants;

namespace Phoenix.Api.Areas.Finance.Controllers
{
    [Area(PhoenixModule.FINANCE)]
    [Route(PhoenixModule.FINANCE_ROUTE_API)]
    public class HomeController : Controller
    {
        
        [HttpGet]
        public string Index()
        {
            return "ini area finance";
        }

        [HttpGet]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        [HttpGet]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpGet]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
