﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Api.Shared.Controllers;
using Phoenix.Shared.Core.Areas.Finance.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Areas.Finance.Services;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Areas.Finance.Dtos.Response;

namespace Phoenix.Api.Areas.Finance.Controllers
{
    [Area(PhoenixModule.FINANCE)]
    [Route(PhoenixModule.FINANCE_ROUTE_API)]
    public class CurrencyBaseController : BaseMasterApiController<FnCurrencyDto,FnCurrencySample,string>
    {

        private IFnCurrencyService _service;

        public CurrencyBaseController(IFnCurrencyService service) : base(service)
        {
            _service = service;
            ModeCreate = ONLY;
            ModeUpdate = EXCEPT;
        }


        [HttpGet]
        public GeneralResponseList<FnCurrencySample> Search(SearchParameter parameter)
        {
            return _service.SearchAll(parameter);
        }

        [HttpGet]
        public GeneralResponse<FnCurrencyResponseDto> SearchSample(SearchParameter parameter)
        {
            return _service.SearchSample(parameter);
        }
        
        
        [HttpPost]
        public GeneralResponse<FnCurrencySample> ChangePassword(FnCurrencyDto dto)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                
                resp.Data = _service.UpdateWithOnly(dto.Id,dto, GetUpdateOnlyPassword());
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }
        
        [HttpPost]
        public GeneralResponse<FnCurrencySample> CreateTransactionWithGeneralResponse(FnCurrencyDto dto)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                resp = _service.SampleTransactionCreateWithResponse(dto, GetCreateExcepts());
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        protected override string[] GetCreateExcepts()
        {
            return new string[] { "Isdeleted" };
        }
        protected override string[] GetCreateOnly()
        {
            return new string[] { "Name" };
        }

        protected override string[] GetUpdateExcepts()
        {
            return new string[] { "Id", "Isdeleted" };
        }

        protected override string[] GetUpdateOnly()
        {
            return new string[] { "Id", "Name" };
        }
        protected string[] GetUpdateOnlyPassword()
        {
            return new string[] { "Password"};
        }
    }
}