﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Constants;
using Phoenix.Shared.Core.Areas.Finance.Services;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Core.Areas.Finance.Dtos;

namespace Phoenix.Api.Areas.Finance.Controllers
{
    [Area(PhoenixModule.FINANCE)]
    [Route(PhoenixModule.FINANCE_ROUTE_API)]
    public class CurrencyController : Controller
    {

        private IFnCurrencyService _service;

        public CurrencyController(IFnCurrencyService service) {
            _service = service;
        }

        [HttpGet]
        public GeneralResponseList<FnCurrencySample> Search(SearchParameter parameter)
        {
            return _service.SearchAll(parameter);
        }

        [HttpGet("{id}")]
        public GeneralResponse<FnCurrencySample> Get(string id)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                resp.Data = _service.GetByID(id);
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex) {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :"+ex.Message;
            }
            return resp;
        }

        [HttpPost]
        public GeneralResponse<FnCurrencySample> Create(FnCurrencyDto dto)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                resp.Data = _service.CreateWithExcept(dto, GetCreateExcepts());
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }


        private string[] GetCreateExcepts()
        {
            return new string[] { "Isdeleted" };
        }

        [HttpPost]
        public GeneralResponse<FnCurrencySample> Update(FnCurrencyDto dto)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                resp.Data = _service.UpdateWithExcept(dto.Id, dto, GetUpdateExcepts());
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }

        private string[] GetUpdateExcepts()
        {
            return new string[] { "Id", "Isdeleted" };
        }

        [HttpGet("{id}")]
        public GeneralResponse<FnCurrencySample> Delete(string id)
        {
            GeneralResponse<FnCurrencySample> resp = new GeneralResponse<FnCurrencySample>();
            try
            {
                _service.DeleteByID(id);
                resp.Success = true;
                resp.Code = "200";
            }
            catch (Exception ex)
            {
                resp.Code = "500";
                resp.Success = false;
                resp.Message = "An error occured, cause of :" + ex.Message;
            }
            return resp;
        }


    }
}