﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Phoenix.Web.Shared.Extensions
{
    public static class LocalizationExtension
    {
        public static HtmlString Msg(this IHtmlHelper helper, string message, params object[] parameters)
        {
            var msg = string.Format(message, parameters);
            return new HtmlString(msg);
        }
    }
}
