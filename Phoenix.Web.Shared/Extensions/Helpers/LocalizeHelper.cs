﻿// @yasinkuyu
// 05/08/2014

using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Insya.Localization.Helpers
{

    public static partial class HtmlHelperExtensions
    {
        /// <summary>
        /// Razor template using @Html.Localize("id")
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static HtmlString Localize(this IHtmlHelper helper, string id)
        {
            return new HtmlString(Localization.Localize(id));
        }

        /// <summary>
        /// Razor template using @Html.Localize("id")
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static HtmlString Localize(this IHtmlHelper helper, Inline lang)
        {
            return new HtmlString(Localization.Localize(lang));
        }

        /// <summary>
        /// or Razor template using @Html.Loc("id")
        /// </summary>
        /// <param id="helper"></param>
        /// <param id="id"></param>
        /// <returns></returns>
        public static HtmlString Loc(this IHtmlHelper helper, string id)
        {
            return new HtmlString(Localization.Localize(id));
        }

        /// <summary>
        /// or Razor template using @Html.Loc("id")
        /// </summary>
        /// <param id="helper"></param>
        /// <param id="id"></param>
        /// <returns></returns>
        public static HtmlString Get(this IHtmlHelper helper, string id)
        {
            return new HtmlString(Localization.Localize(id));
        }
    }

}