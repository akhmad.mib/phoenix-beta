﻿// @yasinkuyu
// 05/08/2014

using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Insya.Localization.Helpers
{
    public static partial class HtmlHelperExtensions
    {

        /// <summary>
        /// MVC Action link localization html helper
        /// Example @Html.ActionLinkLocalization("homepage", "Index", "Home") -> <item id="homepage">Home Page</item>
        /// Output : <a href="homapageurl"></a>
        /// </summary>
        /// <param id="htmlHelper"></param>
        /// <param id="linkText"></param>
        /// <param id="actionName"></param>
        /// <param id="controllerName"></param>
        /// <param id="routeValues"></param>
        /// <param id="htmlAttributes"></param>
        /// <returns></returns>
        public static HtmlString ActionLinkLocalization(this IHtmlHelper htmlHelper, string linkText, string actionName, string controllerName, object routeValues = null, object htmlAttributes = null)
        {
            var urlHelper = new UrlHelper(htmlHelper.ViewContext);
            var tagBuilder = new TagBuilder("a");

            tagBuilder.Attributes["href"] = urlHelper.Action(actionName.ToLowerInvariant(), controllerName.ToLowerInvariant(), routeValues);

            tagBuilder.MergeAttributes(new RouteValueDictionary(htmlAttributes));

            return new HtmlString(tagBuilder.ToString());
        }
    }
}