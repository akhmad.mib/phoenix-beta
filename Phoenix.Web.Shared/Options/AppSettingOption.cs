﻿using System;
using System.Linq;

namespace Phoenix.Web.Shared.Options
{
    public class AppSettingOption
    {
        public string ApiServer { get; set; }
        public string ApiGeneralServer { get; set; }
        public string ApiFinanceServer { get; set; }
        public string ApiHrisServer { get; set; }
        public string ApiPMServer { get; set; }
        public string WebServer { get; set; }
        public string WebPathsNotUsingAuthHeader { get; set; }
        public bool GetUsingAuthHeader(string controllerName,string actionName) {
            string[] webPathsNotUsingAuthHeader = WebPathsNotUsingAuthHeader.Split(',').Select(s => s.Trim()).ToArray();
            foreach (string webPath in webPathsNotUsingAuthHeader)
            {
                string[] wp = webPath.Split('/');
                string wpController = wp[0];
                string wpAction = wp[1];

                if (
                    controllerName.ToLower().Equals(wpController.ToLower()) &&
                    (actionName.ToLower().Equals(wpAction.ToLower()) || wpAction.Equals("*"))
                )
                {
                    return true;
                }
            }
            return false;
        }
    }
}