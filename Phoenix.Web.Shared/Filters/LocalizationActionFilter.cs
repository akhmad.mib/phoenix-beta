﻿using System;
using System.Linq;
using System.Threading;
using Phoenix.Web.Shared.Helpers;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Phoenix.Web.Shared.Filters
{
    public class LocalizationActionFilter : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            string cultureName = null;
            // Attempt to read the culture cookie from Request
            String cultureCookie = filterContext.HttpContext.Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie;
            else
                cultureName = CultureHelper.GetCurrentCulture();
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        }
    }
}
