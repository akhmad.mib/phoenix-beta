﻿using Microsoft.Extensions.Configuration;
using Phoenix.Web.Shared.Helpers;
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Phoenix.Web.Shared.Options;

/*
* Referensi
* 
* http://www.ryadel.com/en/asp-net-mvc-set-global-viewbag-properties-for-all-views/ 
* 
*/


namespace Phoenix.Web.Shared.Filters
{
    #region snippet_ActionFilter
    public class AppSettingActionFilter : IActionFilter
    {
        public static IConfigurationRoot Configuration { get; set; }

        private AppSettingOption option;

        public AppSettingActionFilter(IConfigurationRoot configuration,IOptions<AppSettingOption> settings)
        {
            Configuration = configuration;
            option = settings.Value;
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // do something before the action executes
            var controller = filterContext.Controller as Controller;
            var appSection = Configuration.GetSection("AppConfiguration");
            controller.ViewBag.NOT_USING_AUTH_HEADER = false;
            controller.ViewBag.NOW = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            
//            controller.ViewBag.API_URL = appSection["ApiServer"];
//            controller.ViewBag.WEB_URL = appSection["WebServer"];
//            string[] webPathsNotUsingAuthHeader = appSection["WebPathsNotUsingAuthHeader"].Split(',').Select(s => s.Trim()).ToArray();
            
            controller.ViewBag.API_URL = option.ApiServer;
            controller.ViewBag.API_GENERAL_URL = option.ApiGeneralServer;
            controller.ViewBag.API_FINANCE_URL = option.ApiFinanceServer;
            controller.ViewBag.API_HRIS_URL = option.ApiHrisServer;
            controller.ViewBag.API_PM_URL = option.ApiPMServer;
            controller.ViewBag.WEB_URL = option.WebServer;
            string[] webPathsNotUsingAuthHeader = option.WebPathsNotUsingAuthHeader.Split(',').Select(s => s.Trim()).ToArray();
            
            string actionName = filterContext.RouteData.Values["action"].ToString();
            string controllerName = filterContext.RouteData.Values["controller"].ToString();
            foreach (string webPath in webPathsNotUsingAuthHeader)
            {
                string[] wp = webPath.Split('/');
                string wpController = wp[0];
                string wpAction = wp[1];

                if (
                    controllerName.ToLower().Equals(wpController.ToLower()) &&
                    (actionName.ToLower().Equals(wpAction.ToLower()) || wpAction.Equals("*"))
                )
                {
                    controller.ViewBag.NOT_USING_AUTH_HEADER = true;
                    break;
                }
            }
            var request = controller.Request;
            string jwtToken = request.Cookies["JWT_TOKEN"];
            if (jwtToken != null && jwtToken != "")
            {
                controller.ViewBag.JWT_TOKEN = jwtToken;
            }
            controller.ViewBag.COOKIES = request.Cookies;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            // do something after the action executes
        }
    }
    #endregion

    public class AppSettingsActionFilterAttribute : ActionFilterAttribute
    {
        public static IConfigurationRoot Configuration { get; set; }
        
        private AppSettingOption option;

        public AppSettingsActionFilterAttribute(IConfigurationRoot configuration,IOptions<AppSettingOption> settings)
        {
            Configuration = configuration;
            option = settings.Value;
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            var controller = filterContext.Controller as Controller;
            var appSection = Configuration.GetSection("AppConfiguration");
            controller.ViewBag.NOT_USING_AUTH_HEADER = false;
            controller.ViewBag.NOW = DateTime.Now.ToString("yyyyMMddHHmmssffff");

            //            controller.ViewBag.API_URL = appSection["ApiServer"];
            //            controller.ViewBag.WEB_URL = appSection["WebServer"];
            //            string[] webPathsNotUsingAuthHeader = appSection["WebPathsNotUsingAuthHeader"].Split(',').Select(s => s.Trim()).ToArray();

            controller.ViewBag.API_URL = option.ApiServer;
            controller.ViewBag.API_GENERAL_URL = option.ApiGeneralServer;
            controller.ViewBag.API_FINANCE_URL = option.ApiFinanceServer;
            controller.ViewBag.API_HRIS_URL = option.ApiHrisServer;
            controller.ViewBag.API_PM_URL = option.ApiPMServer;
            controller.ViewBag.WEB_URL = option.WebServer;



            string[] webPathsNotUsingAuthHeader = option.WebPathsNotUsingAuthHeader.Split(',').Select(s => s.Trim()).ToArray();
            
            string actionName = filterContext.RouteData.Values["action"].ToString();
            string controllerName = filterContext.RouteData.Values["controller"].ToString();
            foreach (string webPath in webPathsNotUsingAuthHeader)
            {
                string[] wp = webPath.Split('/');
                string wpController = wp[0];
                string wpAction = wp[1];
                
                if (
                    controllerName.ToLower().Equals(wpController.ToLower()) &&
                    (actionName.ToLower().Equals(wpAction.ToLower()) || wpAction.Equals("*"))
                )
                {
                    controller.ViewBag.NOT_USING_AUTH_HEADER = true;
                    break;
                }
            }
            var request = controller.Request;
            string jwtToken = request.Cookies["JWT_TOKEN"];
            if (jwtToken != null && jwtToken != "")
            {
                controller.ViewBag.JWT_TOKEN = jwtToken;
            }
            controller.ViewBag.COOKIES = request.Cookies;


        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var controller = filterContext.Controller as Controller;
            var request = controller.Request;
            string jwtToken = request.Cookies["JWT_TOKEN"];
            if (jwtToken != null && jwtToken != "")
            {
                controller.ViewBag.JWT_TOKEN = jwtToken;
            }
            controller.ViewBag.COOKIES = request.Cookies;
        }
    }
}
