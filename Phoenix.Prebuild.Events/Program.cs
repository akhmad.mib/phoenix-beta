﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace Phoenix.Prebuild.Events
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            List<XCopy> list = getListXCopyOperations();
            foreach (XCopy copy in list)
            {
                DirectoryCopy(copy.sourceDir,copy.destinationDir,true);
            }
            Console.WriteLine("Finish!");
        }

        public static List<XCopy> getListXCopyOperations()
        {
            List<XCopy> list = new List<XCopy>();

            XCopy copy = null;
            string baseAreaDir = "Phoenix.Web.Areas";
            string projectDir = "";
            string webDir = "Phoenix.Web";
            string currentDir = Directory.GetCurrentDirectory();

            string[] listArea = {"Finance", "Hris", "General", "ProjectManagement"};

            foreach (string area in listArea)
            {
                projectDir = baseAreaDir + "." + area;
                copy = new XCopy();
                copy.sourceDir = currentDir + "/../" + projectDir + "/Views";
                copy.destinationDir = currentDir + "/../" + webDir + "/Areas/" + area + "/Views";
                list.Add(copy);
                copy = new XCopy();
                copy.sourceDir = currentDir + "/../" + projectDir + "/wwwroot/js";
                copy.destinationDir = currentDir + "/../" + webDir + "/wwwroot/assets/js/Areas/" + area;
                list.Add(copy);
                copy = new XCopy();
                copy.sourceDir = currentDir + "/../" + projectDir + "/wwwroot/css";
                copy.destinationDir = currentDir + "/../" + webDir + "/wwwroot/assets/css/Areas/" + area;
                list.Add(copy);
                copy = new XCopy();
                copy.sourceDir = currentDir + "/../" + projectDir + "/wwwroot/images";
                copy.destinationDir = currentDir + "/../" + webDir + "/wwwroot/assets/img/Areas/" + area;
                list.Add(copy);
            }

            return list;
        }

        public static bool isWindows()
        {
            return System.Runtime.InteropServices.RuntimeInformation
                .IsOSPlatform(OSPlatform.Windows);
        }

        public static bool isOSX()
        {
            return System.Runtime.InteropServices.RuntimeInformation
                .IsOSPlatform(OSPlatform.OSX);
        }

        public static bool isLinux()
        {
            return System.Runtime.InteropServices.RuntimeInformation
                .IsOSPlatform(OSPlatform.Linux);
        }
        
        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }
        
            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
    }
}