﻿namespace Phoenix.Prebuild.Events
{
    public class XCopy{
        public string sourceDir { get; set; }
        public string destinationDir { get; set; }
        public string options { get; set; }
    }
}