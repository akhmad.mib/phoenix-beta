﻿using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Repositories
{
    public interface IPmClientBriefContentRepository : IEFRepository<PmClientBriefContent, string>
    {
    }
    public class PmClientBriefContentRepository : EFRepository<PmClientBriefContent, string>, IPmClientBriefContentRepository
    {
        public PmClientBriefContentRepository(EFContext context) : base(context)
        {

        }
    }

}
