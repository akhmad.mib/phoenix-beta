﻿using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Repositories
{
    public interface IPmClientBriefAccountRepository : IEFRepository<PmClientBriefAccount, string>
    {
    }

    public class PmClientBriefAccountRepository : EFRepository<PmClientBriefAccount, string>, IPmClientBriefAccountRepository
    {
        public PmClientBriefAccountRepository(EFContext context) : base(context)
        {

        }
    }
}
