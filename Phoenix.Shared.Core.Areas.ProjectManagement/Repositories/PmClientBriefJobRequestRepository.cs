﻿using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Repositories
{
    public interface IPmClientBriefJobRequestRepository : IEFRepository<PmClientBriefJobRequest, string>
    {
    }
    public class PmClientBriefJobRequestRepository : EFRepository<PmClientBriefJobRequest, string>, IPmClientBriefJobRequestRepository
    {
        public PmClientBriefJobRequestRepository(EFContext context) : base(context)
        {

        }
    }
}
