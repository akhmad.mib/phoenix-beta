﻿using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Repositories
{
    public interface IPmClientBriefRepository : IEFRepository<PmClientBrief,string>
    {
    }
    public class PmClientBriefRepository : EFRepository<PmClientBrief, string>, IPmClientBriefRepository
    {
        public PmClientBriefRepository(EFContext context) : base(context)
        {
            
        }
    }
}
