﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Areas.ProjectManagement.Repositories;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefService : IBaseService<PmClientBriefDto, PmClientBrief, string>
    {
        
    }
    public partial class PmClientBriefService : BaseService<PmClientBriefDto, PmClientBrief, string>, IPmClientBriefService
    {
        //private IPmClientBriefAccountRepository _repoAccount;
        private IPmClientBriefAccountService _serviceAccount;
        private IPmClientBriefContentService _serviceContent;
        private IPmClientBriefJobRequestService _serviceJobRequest;
        public PmClientBriefService( 
            IPmClientBriefAccountService serviceAccount,
            IPmClientBriefContentService serviceContent,
            IPmClientBriefJobRequestService serviceJobRequest,
            IEFRepository<PmClientBrief, string> repo):base(repo)
        {
            _serviceAccount = serviceAccount;
            _serviceContent = serviceContent;
            _serviceJobRequest = serviceJobRequest;
            _repo = repo;
        }
    }
}
