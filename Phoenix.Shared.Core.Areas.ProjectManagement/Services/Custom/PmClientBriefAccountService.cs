﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos.Response;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefAccountService : IBaseService<PmClientBriefAccountDto, PmClientBriefAccount, string>
    {
        PmClientBriefAccount CreateFromMultiple(PmClientBriefAccount data, string[] excepts);
        GeneralResponseList<PmClientBriefAccount> SearchAll(SearchParameter parameter);
    }
    public partial class PmClientBriefAccountService : BaseService<PmClientBriefAccountDto, PmClientBriefAccount, string>, IPmClientBriefAccountService
    {
        public PmClientBriefAccount CreateFromMultiple(PmClientBriefAccount data, string[] excepts)
        {
            PmClientBriefAccount newData = NewInstance();
            PropertyMapper.Except(excepts, data, newData);
            return _repo.Create(newData);
        }
        public GeneralResponseList<PmClientBriefAccount> SearchAll(SearchParameter parameter)
        {
            GeneralResponseList<PmClientBriefAccount> resp = new GeneralResponseList<PmClientBriefAccount>();
            try
            {
                PaginateExpressionParameter<PmClientBriefAccount> param = new PaginateExpressionParameter<PmClientBriefAccount>();
                param.where = o => o.GnaccountManagementId.Contains(parameter.GetSearch());
                param.limit = parameter.GetLimit();
                param.offset = parameter.GetOffset();
                resp.Rows = _repo.FindAll(param);
                resp.RecordsTotal = resp.Rows.Count;
                resp.RecordsFiltered = _repo.Count(param);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }
    }
}
