﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos.Response;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefContentService : IBaseService<PmClientBriefContentDto, PmClientBriefContent, string>
    {
        PmClientBriefContent CreateFromMultiple(PmClientBriefContent data, string[] excepts);
        GeneralResponseList<PmClientBriefContent> SearchAll(SearchParameter parameter);
    }
    public partial class PmClientBriefContentService : BaseService<PmClientBriefContentDto, PmClientBriefContent, string>, IPmClientBriefContentService
    {
        public PmClientBriefContent CreateFromMultiple(PmClientBriefContent data, string[] excepts)
        {
            PmClientBriefContent newData = NewInstance();
            PropertyMapper.Except(excepts, data, newData);
            return _repo.Create(newData);
        }
        public GeneralResponseList<PmClientBriefContent> SearchAll(SearchParameter parameter)
        {
            GeneralResponseList<PmClientBriefContent> resp = new GeneralResponseList<PmClientBriefContent>();
            try
            {
                PaginateExpressionParameter<PmClientBriefContent> param = new PaginateExpressionParameter<PmClientBriefContent>();
                param.where = o => o.Contents.Contains(parameter.GetSearch());
                param.limit = parameter.GetLimit();
                param.offset = parameter.GetOffset();
                resp.Rows = _repo.FindAll(param);
                resp.RecordsTotal = resp.Rows.Count;
                resp.RecordsFiltered = _repo.Count(param);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }
    }
}
