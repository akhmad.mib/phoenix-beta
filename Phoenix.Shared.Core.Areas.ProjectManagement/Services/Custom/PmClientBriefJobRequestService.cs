﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos.Response;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefJobRequestService : IBaseService<PmClientBriefJobRequestDto, PmClientBriefJobRequest, string>
    {
        PmClientBriefJobRequest CreateFromMultiple(PmClientBriefJobRequest data, string[] excepts);
        GeneralResponseList<PmClientBriefJobRequest> SearchAll(SearchParameter parameter);
    }
    public partial class PmClientBriefJobRequestService : BaseService<PmClientBriefJobRequestDto, PmClientBriefJobRequest, string>, IPmClientBriefJobRequestService
    {
        public PmClientBriefJobRequest CreateFromMultiple(PmClientBriefJobRequest data, string[] excepts)
        {
            PmClientBriefJobRequest newData = NewInstance();
            PropertyMapper.Except(excepts, data, newData);
            return _repo.Create(newData);
        }
        public GeneralResponseList<PmClientBriefJobRequest> SearchAll(SearchParameter parameter)
        {
            GeneralResponseList<PmClientBriefJobRequest> resp = new GeneralResponseList<PmClientBriefJobRequest>();
            try
            {
                PaginateExpressionParameter<PmClientBriefJobRequest> param = new PaginateExpressionParameter<PmClientBriefJobRequest>();
                param.where = o => o.Code.Contains(parameter.GetSearch());
                param.limit = parameter.GetLimit();
                param.offset = parameter.GetOffset();
                resp.Rows = _repo.FindAll(param);
                resp.RecordsTotal = resp.Rows.Count;
                resp.RecordsFiltered = _repo.Count(param);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }
    }
}
