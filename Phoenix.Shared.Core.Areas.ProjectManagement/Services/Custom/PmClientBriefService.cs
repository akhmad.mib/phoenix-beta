﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using Phoenix.Shared.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Helpers;
using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos.Response;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefService : IBaseService<PmClientBriefDto, PmClientBrief, string>
    {
        GeneralResponse<PmClientBriefResponseDto> SearchSample(SearchParameter parameter);
        GeneralResponseList<PmClientBrief> SearchAll(SearchParameter parameter);
        PmClientBrief SampleTransactionCreate(PmClientBriefDto data, string[] excepts);
        GeneralResponse<PmClientBriefResponseDto> TransactionMultipleCreate(PmClientBriefResponseDto data, string[] excepts);
        GeneralResponse<PmClientBrief> SampleTransactionCreateResponse(PmClientBriefDto data, string[] excepts);
    }

    public partial class PmClientBriefService : BaseService<PmClientBriefDto, PmClientBrief, string>, IPmClientBriefService
    {
        
        public GeneralResponse<PmClientBriefResponseDto> SearchSample(SearchParameter parameter)
        {
            GeneralResponse<PmClientBriefResponseDto> response = new GeneralResponse<PmClientBriefResponseDto>();
            PmClientBriefResponseDto data = new PmClientBriefResponseDto();
            ExpressionParameter<PmClientBrief> parentParameter = new ExpressionParameter<PmClientBrief>();
            parentParameter.where = o => o.CampaignName.Contains(parameter.GetSearch());
            data.Parent = _repo.FindFirst(parentParameter);

            return response;
        }

        public GeneralResponseList<PmClientBrief> SearchAll(SearchParameter parameter)
        {
            GeneralResponseList<PmClientBrief> resp = new GeneralResponseList<PmClientBrief>();
            try
            {
                PaginateExpressionParameter<PmClientBrief> param = new PaginateExpressionParameter<PmClientBrief>();
                param.where = o => o.CampaignName.Contains(parameter.GetSearch());
                param.limit = parameter.GetLimit();
                param.offset = parameter.GetOffset();
                resp.Rows = _repo.FindAll(param);
                resp.RecordsTotal = resp.Rows.Count;
                resp.RecordsFiltered = _repo.Count(param);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }

        public PmClientBrief SampleTransactionCreate(PmClientBriefDto data, string[] excepts)
        {
            PmClientBrief resp = new PmClientBrief();
            TransactionProcess transactionProcess = delegate
            {
                PmClientBrief newData = NewInstance();
                PropertyMapper.Except(excepts, data, newData);
                return _repo.Create(newData);
            };

            resp = ProcessingTransaction<PmClientBrief>(transactionProcess);
            return resp;
        }

        public GeneralResponse<PmClientBriefResponseDto> TransactionMultipleCreate(PmClientBriefResponseDto data, string[] excepts)
        {
            GeneralResponse<PmClientBriefResponseDto> resp = new GeneralResponse<PmClientBriefResponseDto>();
            TransactionProcess transactionProcess = delegate
            {
                GeneralResponse<PmClientBriefResponseDto> respTrx = new GeneralResponse<PmClientBriefResponseDto>();
                PmClientBriefResponseDto result = new PmClientBriefResponseDto();
                IList<PmClientBriefAccount> childAccount = new List<PmClientBriefAccount>();
                IList<PmClientBriefContent> childContent = new List<PmClientBriefContent>();
                IList<PmClientBriefJobRequest> childJobRequest = new List<PmClientBriefJobRequest>();
                try
                {
                    PmClientBrief newData = NewInstance();
                    PropertyMapper.Except(excepts, data.Parent, newData);
                    _repo.Create(newData);
                    PmClientBriefAccount DataAccount = new PmClientBriefAccount();
                    PropertyMapper.Except(excepts, data.ChildAccount, DataAccount);
                    PmClientBriefContent DataContent = new PmClientBriefContent();
                    PropertyMapper.Except(excepts, data.ChildContent, DataContent);
                    PmClientBriefJobRequest DataJobRequest = new PmClientBriefJobRequest();
                    PropertyMapper.Except(excepts, data.ChildJobRequest, DataJobRequest);
                    DataJobRequest.PmclientBriefId = newData.Id;
                    DataContent.PmclientBriefId = newData.Id;
                    DataAccount.PmclientBriefId = newData.Id;
                    result.Parent = newData;
                    childAccount.Add(_serviceAccount.CreateFromMultiple(DataAccount, excepts));
                    childContent.Add(_serviceContent.CreateFromMultiple(DataContent, excepts));
                    childJobRequest.Add(_serviceJobRequest.CreateFromMultiple(DataJobRequest, excepts));
                    result.ChildAccount = childAccount;
                    result.ChildContent = childContent;
                    result.ChildJobRequest = childJobRequest;
                    respTrx.Success = true;
                    respTrx.Message = "Berhasil";
                }
                catch (Exception ex) {
                    respTrx.Success = false;
                    respTrx.Message = ex.Message;
                }
                return respTrx;
            };

            resp = ProcessingTransaction<GeneralResponse<PmClientBriefResponseDto>>(transactionProcess);
            return resp;
        }

        public GeneralResponse<PmClientBrief> SampleTransactionCreateResponse(PmClientBriefDto data, string[] excepts)
        {
            GeneralResponse<PmClientBrief> resp = new GeneralResponse<PmClientBrief>();
            try
            {
                resp.Data = SampleTransactionCreate(data, excepts);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;

        }
    }
}
