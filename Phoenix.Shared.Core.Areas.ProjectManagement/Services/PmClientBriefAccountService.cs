﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefAccountService : IBaseService<PmClientBriefAccountDto, PmClientBriefAccount, string>
    {

    }
    public partial class PmClientBriefAccountService : BaseService<PmClientBriefAccountDto, PmClientBriefAccount, string>, IPmClientBriefAccountService
    {
        public PmClientBriefAccountService(IEFRepository<PmClientBriefAccount, string> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
