﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefJobRequestService : IBaseService<PmClientBriefJobRequestDto, PmClientBriefJobRequest, string>
    {
    }
    public partial class PmClientBriefJobRequestService : BaseService<PmClientBriefJobRequestDto, PmClientBriefJobRequest, string>, IPmClientBriefJobRequestService
    {
        public PmClientBriefJobRequestService(IEFRepository<PmClientBriefJobRequest, string> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
