﻿using Phoenix.Shared.Core.Areas.ProjectManagement.Dtos;
using Phoenix.Shared.Core.Areas.ProjectManagement.Repositories;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Services
{
    public partial interface IPmClientBriefContentService : IBaseService<PmClientBriefContentDto, PmClientBriefContent, string>
    {
    }
    public partial class PmClientBriefContentService : BaseService<PmClientBriefContentDto, PmClientBriefContent, string>, IPmClientBriefContentService
    {
        public PmClientBriefContentService(IEFRepository<PmClientBriefContent, string> repo) : base(repo)
        {
            _repo = repo;
        }
    }
}
