﻿using Phoenix.Shared.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.ProjectManagement.Dtos.Response
{
    public class PmClientBriefResponseDto
    {
        public PmClientBrief Parent { get; set; }
        public IList<PmClientBriefAccount> ChildAccount { get; set; }
        public IList<PmClientBriefContent> ChildContent { get; set; }
        public IList<PmClientBriefJobRequest> ChildJobRequest { get; set; }

    }
}
