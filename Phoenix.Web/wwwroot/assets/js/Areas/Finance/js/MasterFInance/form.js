﻿function InitFormCreateIndex() {
    var optionsForm = GetOptionsForm(requestFormCreateIndex, responseFormCreateIndex);
    InitForm(formIdIndex, optionsForm);
}

function InitFormUpdateIndex() {
    var optionsForm = GetOptionsForm(requestFormUpdateIndex, responseFormUpdateIndex);
    InitForm(formIdIndex, optionsForm);
}



function AddFormIndex() {
    console.log(ajaxUrlForm);
    showModalAjaxGet("Add", ajaxUrlForm, {}, function (result) {
        setFormAction(formIdIndex, ajaxUrlSave);
        InitFormCreateIndex();
    });
}

function EditFormIndex(id) {
    ajaxGetApiFinance(ajaxUrlGetId + "/" + id, {}, function (response) {
        response = parseJson(response);
        //response = response.data;
        showModalAjaxGet("Edit", ajaxUrlForm, {}, function (result) {
            setFormAction(formIdIndex, ajaxUrlUpdate);
            InitFormUpdateIndex();
            FormLoadByDataUsingName(response.Data, formIdIndex);
        });
    });
}

function DeleteFormIndex(id) {
    var text = "Do you want to delete this data?";
    confirmDialog(text, function () {
        ajaxGetApiFinance(ajaxUrlDeleteId + "/" + id, {}, function (response) {
            response = parseJson(response);
            if (response.Success) {
                SuccessNotif("Delete data is successfully !");
                ReloadGridIndex();
                hideAllModal();
            } else {
                DangerNotif(response.Message);
            }
        });
    });
}




var responseFormCreateIndex = function (response, statusText, xhr, $form) {
    response = parseJson(response);
    if (response.Success) {
        SuccessNotif("Save data is successfully !");
        ReloadGridIndex();
        hideAllModal();
    } else {
        DangerNotif(response.Message);
    }
};

var requestFormCreateIndex = function (formData, jqForm, options) {
    return true;
}

var responseFormUpdateIndex = function (response, statusText, xhr, $form) {
    console.log(response);
    response = parseJson(response);
    if (response.Success) {
        SuccessNotif("Update data is successfully !");
        ReloadGridIndex();
        hideAllModal();
    } else {
        DangerNotif(response.Message);
    }
};

var requestFormUpdateIndex = function (formData, jqForm, options) {
    return true;
}
var secondIdModal = "secondModal";
function ShowModalSecond() {
    showCustomModal(secondIdModal, {modal:false,style:""});
}