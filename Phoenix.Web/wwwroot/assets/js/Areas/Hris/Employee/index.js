﻿var ajaxUrlIndex = API_HRIS + "/Employee/Search";
var ajaxUrlForm = WEB_FINANCE + "/MasterFinance/Form";
var ajaxUrlSave = API_FINANCE + "/CurrencyBase/Create";
var ajaxUrlUpdate = API_FINANCE + "/CurrencyBase/Update";
var ajaxUrlGetId = "/CurrencyBase/Get";
var ajaxUrlDeleteId = "/CurrencyBase/Delete";

var tableIdIndex = "tableIndex";
var formIdIndex = "formIndex";
var searchBoxIdIndex = "searchBoxIndex";

$(function () {
    InitTable(tableIdIndex, ajaxUrlIndex, paramsIndex, columnsIndex);
});

function ShowSearchTableIndex() {
    showCustomModal(searchBoxIdIndex);
}
function SearchTableIndex() {
    ReloadGridIndex();
    hideAllModal();
}

function InitFormCreateIndex() {
    var optionsForm = GetOptionsForm(requestFormCreateIndex, responseFormCreateIndex);
    InitForm(formIdIndex, optionsForm);
}

function InitFormUpdateIndex() {
    var optionsForm = GetOptionsForm(requestFormUpdateIndex, responseFormUpdateIndex);
    InitForm(formIdIndex, optionsForm);
}

function ReloadGridIndex() {
    ReloadGrid(tableIdIndex);
}

function AddFormIndex() {
    console.log(ajaxUrlForm);
    showModalAjaxGet("Add", ajaxUrlForm, {}, function (result) {
        setFormAction(formIdIndex, ajaxUrlSave);
        InitFormCreateIndex();
    });
}

function EditFormIndex(id) {
    ajaxGetApiFinance(ajaxUrlGetId + "/" + id, {}, function (response) {
        response = parseJson(response);
        //response = response.data;
        showModalAjaxGet("Edit", ajaxUrlForm, {}, function (result) {
            setFormAction(formIdIndex, ajaxUrlUpdate);
            InitFormUpdateIndex();
            FormLoadByDataUsingName(response.Data, formIdIndex);
        });
    });
}

function DeleteFormIndex(id) {
    var text = "Do you want to delete this data?";
    confirmDialog(text, function () {
        ajaxGetApiFinance(ajaxUrlDeleteId + "/" + id, {}, function (response) {
            response = parseJson(response);
            if (response.Success) {
                SuccessNotif("Delete data is successfully !");
                ReloadGridIndex();
                hideAllModal();
            } else {
                DangerNotif(response.Message);
            }
        });
    });
}
var columnsIndex = [
    {
        "data": "Id",
        "title": "ID",
        "sClass": "ecol x20",
        orderable: false
    },
    //{
    //    "data": "EmployeeCode",
    //    "title": "Code",
    //    "sClass": "lcol",
    //    orderable: false,
    //    "render": function (data, type, row) {
    //        return '<a href="javascript:void(0)" data-role="detail" data-id="' + row.id + '">' + data + '</a>';
    //    }
    //},
    {
        "data": "NameEmployee", 
        "title": "Name",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "JobGrade",
        "title": "Grade",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "WorkingStatus",
        "title": "Emp. Status",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "JoinDate",
        "title": "Join Date",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "Id",
        "title": "Action",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return "<a href='javascript:void(0)' class='btn btn-default' onclick=\"EditFormIndex('" + data + "');\"><span title='Ubah' class='glyphicon glyphicon-edit'></span>Edit</a> " +
                " <a href='javascript:void(0)' class='btn btn-danger' onclick=\"DeleteFormIndex('" + data + "');\"><span title='Hapus' class='glyphicon glyphicon-trash'></span>Delete</a>";
        }
    }
];

var paramsIndex = function () {
    var search = $('#searchTextIndex').val();
    return { search: search };
}



var responseFormCreateIndex = function (response, statusText, xhr, $form) {
    response = parseJson(response);
    if (response.Success) {
        SuccessNotif("Save data is successfully !");
        ReloadGridIndex();
        hideAllModal();
    } else {
        DangerNotif(response.Message);
    }
};

var requestFormCreateIndex = function (formData, jqForm, options) {
    return true;
}

var responseFormUpdateIndex = function (response, statusText, xhr, $form) {
    console.log(response);
    response = parseJson(response);
    if (response.Success) {
        SuccessNotif("Update data is successfully !");
        ReloadGridIndex();
        hideAllModal();
    } else {
        DangerNotif(response.Message);
    }
};

var requestFormUpdateIndex = function (formData, jqForm, options) {
    return true;
}

