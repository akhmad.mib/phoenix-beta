﻿$(function () {
    InitTable(tableIdIndex, ajaxUrlIndex, paramsSearchIndex, columnsIndex);
});

var ajaxUrlIndex = "https://api.myjson.com/bins/1alde7";//API_FINANCE + "/CurrencyBase/Search";
var ajaxUrlForm = WEB_FINANCE + "/MasterFinance/Form";
var ajaxUrlSave = API_FINANCE + "/CurrencyBase/Create";
var ajaxUrlUpdate = API_FINANCE + "/CurrencyBase/Update";
var ajaxUrlGetId = "/CurrencyBase/Get";
var ajaxUrlDeleteId = "/CurrencyBase/Delete";

var tableIdIndex = "JobList";
var formIdIndex = "formIndex";
var searchBoxIdIndex = "searchBoxIndex";

var columnsIndex = [
    {
        "data": "brand",
        "title": "Brand",
        "sClass": "ecol x20",
        orderable: true
    },
    {
        "data": "unit",
        "title": "Bussines Unit",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "central",
        "title": "Central Resources",
        "sClass": "lcol",
        orderable: false,
        "render": function (data, type, row) {
            var respon = '';
            debugger;
            for (var i = 0; i < row.central.length; i++) {
                respon += '<li>' + row.central[i].name + '</li>';
            }
            return '<ol>' + respon + '</ol>';
        }
    },
    {
        "data": "campaign",
        "title": "Campaign Name",
        "sClass": "rcol",
        orderable: false,
        "sClass": "ecol x100"
    },
    {
        "data": "job",
        "title": "Job Name",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "jobnumber",
        "title": "Job Number",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "deadline",
        "title": "Deadline",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return moment(data).format('DD/MM/YYYY');
        }
    },
    {
        "data": "final",
        "title": "Final Delivery",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return moment(data).format('DD/MM/YYYY');
        }
    },
    {
        "data": "status",
        "title": "Status",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return '<a href="Detail" data-role="detail">' + data + '</a>';
        }
    }
];

var paramsSearchIndex = function () {
    var brand = $('#searchBrand').val();
    var account = $("#searchAccountManagement").val();
    var campaign = $("#searchCampaignName").val();
    var job = $("#searchJobName").val();
    var jobnumber = $("#searchJobNumber").val();
    var unit = $("#searchBussinesUnit").val();
    var deadline = $("#searchDeadline").val();
    var final = $("#searchFinalDelivery").val();
    return { brand: brand, account: account, campaign: campaign, job: job, jobnumber: jobnumber, unit: unit, deadline: deadline, final: final };
}


function ShowSearchTableIndex() {
    showCustomModal(searchBoxIdIndex);
}
function SearchTableIndex() {
    ReloadGridIndex();
    hideAllModal();
}

function ReloadGridIndex() {
    ReloadGrid(tableIdIndex);
}



$('#JobList tbody').on('click', 'tr', function () {
    debugger;
    var data = table.row(this).data();
    window.location.href = "Detail";
});