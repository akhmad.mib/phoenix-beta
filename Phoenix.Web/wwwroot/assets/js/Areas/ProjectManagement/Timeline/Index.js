﻿$(function () {
    var date, start, finish;
    moment.locale('id');
    // Get locale data
    var localeData = moment.localeData();
    var res = moment().format('MMMM YYYY');
    $("#monthNow").html(res);

    //InitData();
});

function GetStart(month) {
    date = new Date(), y = date.getFullYear(), m = date.getMonth();
    start = formatDate(new Date(y, m + month, 1));
    finish = formatDate(new Date(y, (m + 1) + month, 0));

    if ($("#now").val().substr(5, 2) == "12") {
        var taun = Number($("#now").val().substr(0, 4));
        start = (taun + 1) + "-01-01";
        finish = moment(start).endOf('month').format('YYYY-MM-DD');
        $("#now").val(start);
    } else if ($("#now").val() != "") {
        var now = $("#now").val();
        if (now.substr(5, 2) == "01" && month == -1) {
            var taun = Number($("#now").val().substr(0, 4));
            start = (taun - 1) + "-12-01";
            finish = moment(start).endOf('month').format('YYYY-MM-DD');
            $("#now").val(start);
        } else {
            var bln = Number($("#now").val().substr(5, 2)) + month;
            var a = now.substr(0, 4) + "-" + bln + "-01";
            start = moment(a).format('YYYY-MM-DD');
            finish = moment(start).endOf('month').format('YYYY-MM-DD');
            $("#now").val(start);
        }
    }
    $("#now").val(start);
    moment.locale('id');
    // Get locale data
    localeData = moment.localeData();
    res = moment(start).format('MMMM YYYY');
    $("#monthNow").html(res);
}

function formatDate(date) {
    var year = date.getFullYear().toString();
    var month = (date.getMonth() + 101).toString().substring(1);
    var day = (date.getDate() + 100).toString().substring(1);
    return year + "-" + month + "-" + day;
}

function InitData() {
    //Timeline (visjs)
    date = new Date(), y = date.getFullYear(), m = date.getMonth();
    start = formatDate(new Date(y, m, 1));
    finish = formatDate(new Date(y, m + 1, 0));

    var container = document.getElementById('visualization');
    var data = [
        { id: 1, content: 'item 1', start: '2017-11-20', end: '2017-11-23', group: 'Supervisor', subgroup: '0' },
        { id: 2, content: 'item 2', start: '2017-11-14', end: '2017-11-19', group: 'Operational', subgroup: '0' },
        { id: 3, content: 'item 3', start: '2017-11-18', end: '2017-11-21', group: 'Supervisor', subgroup: '0' },
        { id: 4, content: 'item 4', start: '2017-11-16', end: '2017-11-24', group: 'Production', subgroup: '0' },
        { id: 5, content: 'item 5', start: '2017-11-25', end: '2017-11-27', group: 'Operational', subgroup: '0' },
        { id: 6, content: 'item 6', start: '2017-11-27', end: '2017-11-29', group: 'Production', subgroup: '0' }
    ];
    var groups = new vis.DataSet([
        { id: 'Supervisor', content: 'Supervisor', },
        { id: 'Production', content: 'Production', },
        { id: 'Operational', content: 'Operational', },
    ]);
    var options = {
        // start and end of timeline
        start: start,
        end: finish,
        height: '250px',
        editable: false
    };

    var timeline = new vis.Timeline(container, data, groups, options);

}
