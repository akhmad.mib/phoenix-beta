﻿$(function () {
    InitTable(tableIdIndex, ajaxUrlIndex, paramsSearchIndex, columnsIndex);
});

var ajaxUrlIndex = "https://api.myjson.com/bins/1h7pn3";//API_FINANCE + "/CurrencyBase/Search";
var ajaxUrlForm = WEB_FINANCE + "/MasterFinance/Form";
var ajaxUrlSave = API_FINANCE + "/CurrencyBase/Create";
var ajaxUrlUpdate = API_FINANCE + "/CurrencyBase/Update";
var ajaxUrlGetId = "/CurrencyBase/Get";
var ajaxUrlDeleteId = "/CurrencyBase/Delete";

var tableIdIndex = "PCEList";
var formIdIndex = "formIndex";
var searchBoxIdIndex = "searchBoxIndex";

var columnsIndex = [
    {
        "data": "client",
        "title": "Client Name",
        "sClass": "ecol x20",
        orderable: true
    },
    {
        "data": "brand",
        "title": "Brand",
        "sClass": "lcol",
        orderable: false
    },
    {
        "data": "jobnumber",
        "title": "Job Number",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "job",
        "title": "Job Name",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "budgetnumber",
        "title": "Budget Number",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "created",
        "title": "Created Date",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return moment(data).format('DD/MM/YYYY');
        }
    },
    {
        "data": "due",
        "title": "Due Date",
        "sClass": "rcol",
        orderable: false,
        "render": function (data, type, row) {
            return moment(data).format('DD/MM/YYYY');
        }
    },
    {
        "data": "status",
        "title": "Status",
        "sClass": "rcol",
        orderable: false,
    }
];

var paramsSearchIndex = function () {
    var brand = $('#searchBrand').val();
    var account = $("#searchAccountManagement").val();
    var campaign = $("#searchCampaignName").val();
    var job = $("#searchJobName").val();
    var jobnumber = $("#searchJobNumber").val();
    var unit = $("#searchBussinesUnit").val();
    var deadline = $("#searchDeadline").val();
    var final = $("#searchFinalDelivery").val();
    return { brand: brand, account: account, campaign: campaign, job: job, jobnumber: jobnumber, unit: unit, deadline: deadline, final: final };
}


function ShowSearchTableIndex() {
    showCustomModal(searchBoxIdIndex);
}
function SearchTableIndex() {
    ReloadGridIndex();
    hideAllModal();
}

function ReloadGridIndex() {
    ReloadGrid(tableIdIndex);
}

