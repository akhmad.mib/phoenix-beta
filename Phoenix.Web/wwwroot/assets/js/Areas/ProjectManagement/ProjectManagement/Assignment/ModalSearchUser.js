﻿$(function () {
    $('.icheckbox_md').on('ifChanged', function (event) {
        if (event.target.checked) {
            arrUserApproval.push(event.target.value);
        } else {
            arrUserApproval = remove(arrUserApproval, event.target.value);
        }
        arrUserApproval = remove(arrUserApproval, "on");
    });

    //    InitData();
});

function remove(array, element) {
    return array.filter(e => e !== element);
}

var arrUserApproval = [];

var task = "new_task";
var content = "content_task";
var searchUserApproval = "ModalSearchUserApproval";

// 24/11/2017
function AddNewTask() {
    showCustomModal(content, "modal:false");
}

function SearchUserApproval() {
    showCustomModal(searchUserApproval, "modal:false,center:false,stack:true");
}

function format(icon) {
    var originalOption = icon.element;
    return '<i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text;
}

function addUserApproval() {
    $("#ApproveBy").val(arrUser.toString().replace(/\s/g, ''));
    hideCustomModal(searchUser);
    AddNewTask();
}


function InitData() {
    var windowHeight = $(window).height();
    var boxHeight = $('.uk-modal-dialog').height();
    var modal1 = UIkit.modal("#new_task", { center: true, modal: false }),
        modal2 = UIkit.modal("#ModalAssignName", { center: false, modal: false }),
        modal3 = UIkit.modal("#ModalSearchUser", { center: false, modal: false });
    $("#tableEmployee").dataTable();
}

function setDataToList(id) {
    var table = $('#tableEmployee').DataTable();
    var data = table.rows(id).data();
    for (var i = 0; i < data.length; i++) {
        var nama = data[i][1];
    }
    $('#testx').append('<div class="">' + nama + ' &nbsp;&nbsp;<span onclick="rem(this)" class="uk-icon uk-icon-minus-circle xk"></span></div>');
}

function setApprovalDataToList(id) {
    var table = $('#tableApproval').DataTable();
    var data = table.rows(id).data();
    for (var i = 0; i < data.length; i++) {
        var nama = data[i][1];
    }
    $('#approval').append('<div class="">' + nama + ' &nbsp;&nbsp;<span onclick="rem(this)" class="uk-icon uk-icon-minus-circle xk"></span></div>');
}

function rem(e) {
    $(e).parent().remove();
}