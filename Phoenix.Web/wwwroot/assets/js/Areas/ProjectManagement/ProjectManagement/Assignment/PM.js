﻿//$(function () {
//    InitData();
//});

//function InitData() {
//    $("#tableEmployee").dataTable();
//}

function setData(id) {
    var table = $('#tableEmployee').DataTable();
    var data = table.rows(id).data();
    for (var i = 0; i < data.length; i++) {
        $("#campaignname").val(data[i][1]);
        $("#jobname").val(data[i][3]);
        $("#jobnumber").val(data[i][4]);
        $("#Deadline").val(data[i][5]);
        $("#DeadlineTime").val(data[i][6]);
        $("#FinalDelivery").val(data[i][7]);
        $("#FinalDeliveryTime").val(data[i][8]);

        var accountContent="";
        var account = data[i][2];
        var arrAcc = account.split(",");
        if (arrAcc.length > 0) {
            for (var i = 0; i < arrAcc.length; i++) {
                accountContent += "<li>" + arrAcc[i] + "</li>";
            }
        }
        document.getElementById("account").innerHTML = "<ol>" + accountContent + "</ol>";
    }
}

function setPM(id) {
    var table = $('#tablePM').DataTable();
    var data = table.rows(id).data();
    for (var i = 0; i < data.length; i++) {
        var newPM;
        var pm = $("#projectmanager").val();
        if (pm == "") {
            newPM = data[i][1];
        }
        else {
            newPM = pm + ", " + data[i][1];
        }
        $("#projectmanager").val(newPM);
    }
}