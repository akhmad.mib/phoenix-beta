﻿//$(function () {

Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

//    InitData();
//});

function InitData() {
    $("#tableEmployee").dataTable();

    $('#c1').on('ifChecked ifUnchecked', function (event) {
        var checkboxes = $('#1').find(":checkbox");
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    $('#c2').on('ifChecked ifUnchecked', function (event) {
        var checkboxes = $('#2').find(":checkbox");
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    $('#c3').on('ifChecked ifUnchecked', function (event) {
        var checkboxes = $('#3').find(":checkbox");
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    $('#c4').on('ifChecked ifUnchecked', function (event) {
        var checkboxes = $('#4').find(":checkbox");
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    $('#c5').on('ifChecked ifUnchecked', function (event) {
        var checkboxes = $('#5').find(":checkbox");
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });

    $('#c6').on('ifChecked ifUnchecked', function (event) {
        var checkboxes = $('#6').find(":checkbox");
        if (event.type == 'ifChecked') {
            checkboxes.iCheck('check');
        } else {
            checkboxes.iCheck('uncheck');
        }
    });
}

function getCR() {
    var arr = $("input:checked").map(function () {
        return $(this).val();
    }).toArray();
    arr.remove('on');
    $('#centralresource').val(arr);
}

function setData(id) {
    var table = $('#tableEmployee').DataTable();
    var data = table.rows(id).data();
    for (var i = 0; i < data.length; i++) {
        $("#campaignname").val(data[i][1]);
        $("#jobname").val(data[i][3]);
        $("#jobnumber").val(data[i][4]);
        $("#Deadline").val(data[i][5]);
        $("#DeadlineTime").val(data[i][6]);
        $("#FinalDelivery").val(data[i][7]);
        $("#FinalDeliveryTime").val(data[i][8]);

        var accountContent = "";
        var account = data[i][2];
        var arrAcc = account.split(",");
        if (arrAcc.length > 0) {
            for (var i = 0; i < arrAcc.length; i++) {
                accountContent += "<li>" + arrAcc[i] + "</li>";
            }
        }
        document.getElementById("account").innerHTML = "<ol>" + accountContent + "</ol>";
    }
}

function setPM(id) {
    var table = $('#tablePM').DataTable();
    var data = table.rows(id).data();
    for (var i = 0; i < data.length; i++) {
        var newPM;
        var pm = $("#projectmanager").val();
        if (pm == "") {
            newPM = data[i][1];
        }
        else {
            newPM = pm + ", " + data[i][1];
        }
        $("#projectmanager").val(newPM);
    }
}

function renderjsTree() {
    $('#tree1').jstree({
        'core': {
            'data': [
                { "id": "ajson1", "parent": "#", "text": "Simple root node" },
                { "id": "ajson2", "parent": "#", "text": "Root node 2" },
                { "id": "ajson3", "parent": "ajson2", "text": "Child 1" },
                { "id": "ajson4", "parent": "ajson2", "text": "Child 2" },
            ]
        }
    });
}