﻿$(function () {
    InitTable(tableIdIndex, ajaxUrlIndex, paramsSearchIndex, columnsIndex);
});

var ajaxUrlIndex = "https://api.myjson.com/bins/axpz3";//API_FINANCE + "/CurrencyBase/Search";
var ajaxUrlForm = WEB_FINANCE + "/MasterFinance/Form";
var ajaxUrlSave = API_FINANCE + "/CurrencyBase/Create";
var ajaxUrlUpdate = API_FINANCE + "/CurrencyBase/Update";
var ajaxUrlGetId = "/CurrencyBase/Get";
var ajaxUrlDeleteId = "/CurrencyBase/Delete";

var tableIdIndex = "ApprovalList";
var formIdIndex = "formIndex";
var searchBoxIdIndex = "searchBoxIndex";

var columnsIndex = [
    {
        "data": "campaign",
        "title": "Campaign Name",
        "sClass": "ecol x20",
        orderable: true
    },
    {
        "data": "job",
        "title": "Job Name",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "jobnumber",
        "title": "Job Number",
        "sClass": "rcol",
        orderable: false,
    },
    {
        "data": "type",
        "title": "Type",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "unit",
        "title": "Type Name",
        "sClass": "rcol",
        orderable: false
    },
    {
        "data": "pic",
        "title": "Submitted by",
        "sClass": "rcol",
        orderable: false
    }
];

var paramsSearchIndex = function () {
    var brand = $('#searchBrand').val();
    var account = $("#searchAccountManagement").val();
    var campaign = $("#searchCampaignName").val();
    var job = $("#searchJobName").val();
    var jobnumber = $("#searchJobNumber").val();
    var unit = $("#searchBussinesUnit").val();
    var deadline = $("#searchDeadline").val();
    var final = $("#searchFinalDelivery").val();
    return { brand: brand, account: account, campaign: campaign, job: job, jobnumber: jobnumber, unit: unit, deadline: deadline, final: final };
}


function ShowSearchTableIndex() {
    showCustomModal(searchBoxIdIndex);
}
function SearchTableIndex() {
    ReloadGridIndex();
    hideAllModal();
}

function ReloadGridIndex() {
    ReloadGrid(tableIdIndex);
}

