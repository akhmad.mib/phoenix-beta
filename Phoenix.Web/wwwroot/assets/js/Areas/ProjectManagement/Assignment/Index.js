﻿$(function () {
    $('#ModalSearchUserApproval').on({
        'show.uk.modal': function () {
            isApproval = true;
        },

        'hide.uk.modal': function () {
            isApproval = false;
        }
    });
    $('.icheckbox_md').on('ifChanged', function (event) {
        if (isApproval) {
            if (event.target.checked) {
                arrUserApproval.push(event.target.value);
            } else {
                arrUserApproval = remove(arrUserApproval, event.target.value);
            }
            arrUserApproval = remove(arrUserApproval, "on");
        } else {
            if (event.target.checked) {
                arrUser.push(event.target.value);
            } else {
                arrUser = remove(arrUser, event.target.value);
            }
            arrUser = remove(arrUser, "on");
        }
    });
    $('.iradio_md').on('ifChanged', function (event) {        
        if (event.target.checked) {
            priority = event.target.value;
        }
    });
});

function remove(array, element) {
    return array.filter(e => e !== element);
}

var priority = "";
var isApproval = false;
var arrUser = [];
var arrUserApproval = [];
var task = "new_task";
var content = "content_task";
var searchUser = "ModalSearchUser";
var searchUserApproval = "ModalSearchUserApproval";
var id = "";

function addBoard() {    
    var title = $("#title").val();
    var assignto = $("#AssignTo").val();
    var approveby = $("#ApproveBy").val();
    var description = $("#description").val();

    var newboard = "<div><div class='scrum_task " + priority + "'><h3 class='scrum_task_title'><a href='#' data-uk-modal='{ center:true }'>" + title + "</a></h3><p class='scrum_task_description'>" + description + "</p><p class='scrum_task_info'><span class='uk-text-muted'>Assign to: </span> <a href='#'>" + assignto + "</a><br /><span class='uk-text-muted'>Approve by: </span> <a href='#'>" + approveby + "</a></p></div></div>";
    $('#' + id + '').append(newboard);
    hideCustomModal(content);
}

// 24/11/2017
function AddNewTask(boardID) {
    if (typeof boardID == 'undefined') {
        id = id;
    } else {
        id = boardID;
    }
    showCustomModal(content, "modal:false");
}

function AddContentTask() {
    showCustomModal(searchUser, "background:none;");
}

function SearchUser() {
    showCustomModal(searchUser, "modal:false,center:false,stack:true");
}

function SearchUserApproval() {
    showCustomModal(searchUserApproval, "modal:false,center:false,stack:true");
}

function format(icon) {
    var originalOption = icon.element;
    return '<i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text;
}

function addUser() {
    $("#AssignTo").val(arrUser.toString().replace(/\s/g, ''));
    hideCustomModal(searchUser);
    AddNewTask();
}

function addUserApproval() {
    $("#ApproveBy").val(arrUserApproval.toString().replace(/\s/g, ''));
    hideCustomModal(searchUserApproval);
    AddNewTask();
}
