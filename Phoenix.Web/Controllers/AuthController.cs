﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Responses;
using Phoenix.Web.Shared.Options;
using Microsoft.Extensions.Options;
using System.Net;

namespace Phoenix.Web.Controllers
{
    public class AuthController : Controller
    {
        private AppSettingOption appSetting;
        public AuthController(IOptions<AppSettingOption> AppSetting) {
            appSetting = AppSetting.Value;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public GeneralResponse<string> SaveSession(string id)
        {
            string token = id;
            GeneralResponse<string> resp = new GeneralResponse<string>();
            GeneralResponse<string> respCheck = new GeneralResponse<string>();
            try
            {
                respCheck = CheckValidToken(token);
                resp.Success = respCheck.Success;
                if (resp.Success)
                {
                    Response.Cookies.Append("JWT_TOKEN", token);
                }
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }
        private GeneralResponse<string> CheckValidToken(string token)
        {
            GeneralResponse<string> resp = new GeneralResponse<string>();
            try
            {
                string url = @appSetting.ApiServer+"/auth/cek";
                //token = "FeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImFzZCIsIm5iZiI6MTUwOTcxODM2MSwiZXhwIjoxNTA5NzE5NTYxLCJpYXQiOjE1MDk3MTgzNjF9.yOHqPNdw4mzxwoOVawXmrpbZLSFeo3qmnqdG90k0mxM";
                WebClient webClient = new WebClient();
                webClient.Headers[HttpRequestHeader.Authorization] = "pToken " + token;

                var result = webClient.DownloadString(url);
                resp.Data = token;
                resp.Success = true;
            }
            catch (Exception ex) {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }
    }
}