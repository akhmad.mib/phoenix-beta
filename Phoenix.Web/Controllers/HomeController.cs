﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Web.Models;

namespace Phoenix.Web.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            string jwtToken = Request.Cookies["JWT_TOKEN"];
            if (jwtToken != null && jwtToken != "")
            {
                return View();
            }
            else {
                return new RedirectResult("/Auth/Login", false);
            }
            
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
