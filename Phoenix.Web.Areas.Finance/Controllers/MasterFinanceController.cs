﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Constants;

namespace Phoenix.Web.Areas.Finance.Controllers
{
    [Area(PhoenixModule.FINANCE)]
    public class MasterFinanceController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Form()
        {
            return View();
        }
    }
}