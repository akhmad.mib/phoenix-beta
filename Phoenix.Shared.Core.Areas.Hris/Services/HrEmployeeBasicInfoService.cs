﻿using Phoenix.Shared.Core.Areas.Hris.Dtos;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using Phoenix.Shared.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Phoenix.Shared.Core.Parameters;
using Phoenix.Shared.Responses;
using Phoenix.Shared.Helpers;

namespace Phoenix.Shared.Core.Areas.Hris.Services
{
    public partial interface IHrEmployeeBasicInfoService : IBaseService<HrEmployeeBasicInfoDto, HremployeeBasicInfo, string>
    {
        GeneralResponseList<HremployeeBasicInfo> SearchAll(SearchParameter parameter);
    }
    public partial class HrEmployeeBasicInfoService : BaseService<HrEmployeeBasicInfoDto, HremployeeBasicInfo, string>, IHrEmployeeBasicInfoService
    {
        public HrEmployeeBasicInfoService(IEFRepository<HremployeeBasicInfo, string> repo) : base(repo)
        {
            _repo = repo;
        }

        public GeneralResponseList<HremployeeBasicInfo> SearchAll(SearchParameter parameter)
        {
            GeneralResponseList<HremployeeBasicInfo> resp = new GeneralResponseList<HremployeeBasicInfo>();
            try
            {
                PaginateExpressionParameter<HremployeeBasicInfo> param = new PaginateExpressionParameter<HremployeeBasicInfo>();
                param.where = o => o.NameEmployee.Contains(parameter.GetSearch());
                param.limit = parameter.GetLimit();
                param.offset = parameter.GetOffset();
                resp.Rows = _repo.FindAll(param);
                resp.RecordsTotal = resp.Rows.Count;
                resp.RecordsFiltered = _repo.Count(param);
                resp.Success = true;
            }
            catch (Exception ex)
            {
                resp.Success = false;
                resp.Message = ex.Message;
            }
            return resp;
        }

    }
}
