﻿using Phoenix.Shared.Core.Contexts;
using Phoenix.Shared.Core.Entities;
using Phoenix.Shared.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Areas.Hris.Repositories
{
    public interface IHrEmployeeBasicInfoRepository : IEFRepository<HremployeeBasicInfo, string>
    {

    }
    public class HrEmployeeBasicInfoRepository : EFRepository<HremployeeBasicInfo, string>, IHrEmployeeBasicInfoRepository
    {
        public HrEmployeeBasicInfoRepository(EFContext context) : base(context)
        {

        }
    }
}
