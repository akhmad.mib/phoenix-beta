﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Phoenix.Shared.Core.Contexts
{
    public partial class AppDbContext
    {
        public AppDbContext() : base()
        {

        }
        public AppDbContext(DbContextOptions<EFContext> options) : base(options)
        {

        }
    }
}
