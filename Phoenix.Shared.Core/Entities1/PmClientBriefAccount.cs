﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities1
{
    public partial class PmclientBriefAccount : BaseEntity<long>
    {
        //public string Id { get; set; }
        public string HrjobPostingRequisitionId { get; set; }
        public string GnaccountManagementId { get; set; }
        public string FnapprovalStatusId { get; set; }
        public string PmclientBriefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
