﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Phoenix.Shared.Core.Entities1
{
    public class BaseEntity<PK>
    {
        //[Key]
        public PK Id { get; set; }
    }
}
