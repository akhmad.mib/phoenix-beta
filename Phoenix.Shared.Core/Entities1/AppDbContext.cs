﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Phoenix.Shared.Core.Entities1;

namespace Phoenix.Shared.Core.Contexts
{
    public partial class AppDbContext : EFContext
    {
        public virtual DbSet<FnaccountCat> FnAccountCat { get; set; }
        public virtual DbSet<FncurrencySample> FnCurrencySample { get; set; }
        public virtual DbSet<PmclientBrief> PmClientBrief { get; set; }
        public virtual DbSet<PmclientBriefAccount> PmClientBriefAccount { get; set; }
        public virtual DbSet<PmclientBriefContent> PmClientBriefContent { get; set; }
        public virtual DbSet<PmclientBriefJobRequest> PmClientBriefJobRequest { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"data source=139.255.87.235,49172;Initial Catalog=PhoenixDev;Persist Security Info=True;User ID=dev-user;password=d3v-u53r;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FnaccountCat>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                    .HasColumnName("account_id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Condition)
                    .HasColumnName("condition")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LevelId).HasColumnName("level_id");

                entity.Property(e => e.Name1)
                    .HasColumnName("name1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name2)
                    .HasColumnName("name2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name3)
                    .HasColumnName("name3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name4)
                    .HasColumnName("name4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name5)
                    .HasColumnName("name5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Report)
                    .HasColumnName("report")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FncurrencySample>(entity =>
            {
                entity.ToTable("FnCurrency_Sample");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CodeCurrency)
                    .IsRequired()
                    .HasColumnName("code_currency")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NameCurrency)
                    .IsRequired()
                    .HasColumnName("name_currency")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PmclientBrief>(entity =>
            {
                entity.ToTable("pmclient_brief");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CampaignName)
                .HasColumnName("campaign_name")
                .HasMaxLength(50);
                entity.Property(e => e.PmcampaignTypeId)
                .HasColumnName("pmcampaign_type_id")
                .HasMaxLength(50);
                entity.Property(e => e.PmproposeTypeId)
                .HasColumnName("pmpropose_type_id")
                .HasMaxLength(50);
                entity.Property(e => e.Brand)
                .HasColumnName("brand")
                .HasMaxLength(50);
                entity.Property(e => e.DateCreate)
                .HasColumnName("date_create");
                entity.Property(e => e.BusinessGroup)
                .HasColumnName("business_group")
                .HasMaxLength(50);
                entity.Property(e => e.Deadline)
                .HasColumnName("deadline");
                entity.Property(e => e.FinalDelivery)
                .HasColumnName("final_delivery");
                entity.Property(e => e.HrnationalityId)
                .HasColumnName("hrnationality_id")
                .HasMaxLength(50);
                entity.Property(e => e.PmjobCategoryId)
                .HasColumnName("pmjob_category_id")
                .HasMaxLength(50);
                entity.Property(e => e.PmclientBriefFileId)
                .HasColumnName("pmclient_brief_file_id")
                .HasMaxLength(50);
                entity.Property(e => e.GncentralResourceId)
                .HasColumnName("gncentral_resource_id")
                .HasMaxLength(50);
                entity.Property(e => e.PmAssigmentId)
                .HasColumnName("pm_assigment_id");
                entity.Property(e => e.PmcampaignProgressId)
                .HasColumnName("pmcampaign_progress_id");
                entity.Property(e => e.StartCampaign)
                .HasColumnName("start_campaign");
                entity.Property(e => e.FinishCampaign)
                .HasColumnName("finish_campaign");
                entity.Property(e => e.ResponsiblePerson)
                .HasColumnName("responsible_person");
                entity.Property(e => e.PmjobPeId)
                .HasColumnName("pmjob_pe_id");
                entity.Property(e => e.PmproposalFileId)
                .HasColumnName("pmproposal_file_id");
                entity.Property(e => e.CampaignProgressId)
                .HasColumnName("campaign_progress_id");
                entity.Property(e => e.CampaignStatusId)
                .HasColumnName("campaign_status_id");
                entity.Property(e => e.CreatedBy)
                .HasColumnName("created_by")
                .HasMaxLength(50);
                entity.Property(e => e.CreatedDate)
                .HasColumnName("created_date");
                entity.Property(e => e.UpdateBy)
                .HasColumnName("update_by")
                .HasMaxLength(50);
                entity.Property(e => e.UpdateDate)
                .HasColumnName("update_date");
                entity.Property(e => e.Isdeleted)
                .HasColumnName("isdeleted")
                .HasDefaultValueSql("((0))");

            });

            modelBuilder.Entity<PmclientBriefAccount>(entity =>
            {
                entity.ToTable("pmclient_brief_account");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.HrjobPostingRequisitionId)
                .HasColumnName("hrjob_posting_requisition_id")
                .HasMaxLength(50);
                entity.Property(e => e.GnaccountManagementId)
                .HasColumnName("gnaccount_management_id")
                .HasMaxLength(50);
                entity.Property(e => e.FnapprovalStatusId)
                .HasColumnName("fnapproval_status_id")
                .HasMaxLength(50);
                entity.Property(e => e.PmclientBriefId)
                .HasColumnName("pmclient_brief_id")
                .HasMaxLength(50);
                entity.Property(e => e.CreatedBy)
                .HasColumnName("created_by")
                .HasMaxLength(50);
                entity.Property(e => e.CreatedDate)
                .HasColumnName("created_date");
                entity.Property(e => e.UpdateBy)
                .HasColumnName("update_by")
                .HasMaxLength(50);
                entity.Property(e => e.UpdateDate)
                .HasColumnName("update_date");
                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

            });

            modelBuilder.Entity<PmclientBriefContent>(entity =>
            {
                entity.ToTable("pmclient_brief_content");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PmclientBriefId)
                .HasColumnName("pmclient_brief_id")
                .HasMaxLength(50);
                entity.Property(e => e.HrjobPostingRequisitionId)
                .HasColumnName("hrjob_posting_requisition_id")
                .HasMaxLength(50);
                entity.Property(e => e.Cover)
                .HasColumnName("cover")
                .HasMaxLength(50);
                entity.Property(e => e.Contents)
                .HasColumnName("contents")
                .HasMaxLength(250);
                entity.Property(e => e.Description)
                .HasColumnName("description")
                .HasMaxLength(250);
                entity.Property(e => e.CreatedBy)
                .HasColumnName("created_by")
                .HasMaxLength(50);
                entity.Property(e => e.CreatedDate)
                .HasColumnName("created_date");
                entity.Property(e => e.UpdateBy)
                .HasColumnName("update_by")
                .HasMaxLength(50);
                entity.Property(e => e.UpdateDate)
                .HasColumnName("update_date");
                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<PmclientBriefJobRequest>(entity =>
            {
                entity.ToTable("pmclient_brief_job_request");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PmclientBriefId)
                .HasColumnName("pmclient_brief_id")
                .HasMaxLength(50);
                entity.Property(e => e.Code)
                .HasColumnName("code")
                .HasMaxLength(20);
                entity.Property(e => e.JobTitle)
                .HasColumnName("job_title");
                entity.Property(e => e.JobType)
                .HasColumnName("job_type")
                .HasMaxLength(100);
                entity.Property(e => e.NumberOfPosition)
                .HasColumnName("number_of_position");
                entity.Property(e => e.CandidateAgeRangeStart)
                .HasColumnName("candidate_age_range_start");
                entity.Property(e => e.CandidateAgeRangeEnd)
                .HasColumnName("candidate_age_range_end");
                entity.Property(e => e.BranchcompanyId)
                .HasColumnName("branchcompany_id");
                entity.Property(e => e.SalaryStart)
                .HasColumnName("salary_start");
                entity.Property(e => e.SalaryEnd)
                .HasColumnName("salary_end");
                entity.Property(e => e.StartDate)
                .HasColumnName("start_date");
                entity.Property(e => e.CloseDate)
                .HasColumnName("close_date");
                entity.Property(e => e.CandidateQualification)
                .HasColumnName("candidate_qualification");
                entity.Property(e => e.CandidateExperience)
                .HasColumnName("candidate_experience");
                entity.Property(e => e.JobPostDescription)
                .HasColumnName("job_post_description");
                entity.Property(e => e.Notes)
                .HasColumnName("notes");
                entity.Property(e => e.FnapprovalStatusId)
                .HasColumnName("fnapproval_status_id")
                .HasMaxLength(50);
                entity.Property(e => e.CreatedBy)
                .HasColumnName("created_by")
                .HasMaxLength(50);
                entity.Property(e => e.CreatedDate)
                .HasColumnName("created_date");
                entity.Property(e => e.UpdateBy)
                .HasColumnName("update_by")
                .HasMaxLength(50);
                entity.Property(e => e.UpdateDate)
                .HasColumnName("update_date");
                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");
            });

        }
    }
}
