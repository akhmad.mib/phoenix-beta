﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities1
{
    public partial class PmclientBrief : BaseEntity<long>
    {
        //public string Id { get; set; }
        public string CampaignName { get; set; }
        public string PmcampaignTypeId { get; set; }
        public string PmproposeTypeId { get; set; }
        public string Brand { get; set; }
        public DateTime? DateCreate { get; set; }
        public string BusinessGroup { get; set; }
        public DateTime? Deadline { get; set; }
        public DateTime? FinalDelivery { get; set; }
        public string HrnationalityId { get; set; }
        public string PmjobCategoryId { get; set; }
        public string PmclientBriefFileId { get; set; }
        public string GncentralResourceId { get; set; }
        public long? PmAssigmentId { get; set; }
        public long? PmcampaignProgressId { get; set; }
        public DateTime? StartCampaign { get; set; }
        public DateTime? FinishCampaign { get; set; }
        public long? ResponsiblePerson { get; set; }
        public long? PmjobPeId { get; set; }
        public long? PmproposalFileId { get; set; }
        public long? CampaignProgressId { get; set; }
        public long? CampaignStatusId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
