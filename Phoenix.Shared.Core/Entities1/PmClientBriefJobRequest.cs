﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities1
{
    public partial class PmclientBriefJobRequest : BaseEntity<long>
    {
        //public string Id { get; set; }
        public string PmclientBriefId { get; set; }
        public string Code { get; set; }
        public int JobTitle { get; set; }
        public string JobType { get; set; }
        public int? NumberOfPosition { get; set; }
        public int? CandidateAgeRangeStart { get; set; }
        public int? CandidateAgeRangeEnd { get; set; }
        public int? BranchcompanyId { get; set; }
        public int? SalaryStart { get; set; }
        public int? SalaryEnd { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime CloseDate { get; set; }
        public string CandidateQualification { get; set; }
        public string CandidateExperience { get; set; }
        public string JobPostDescription { get; set; }
        public string Notes { get; set; }
        public string FnapprovalStatusId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
