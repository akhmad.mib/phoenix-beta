﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities1
{ 
    public partial class PmclientBriefContent : BaseEntity<long>
    {
        //public string Id { get; set; }
        public string PmclientBriefId { get; set; }
        public string HrjobPostingRequisitionId { get; set; }
        public string Cover { get; set; }
        public string Contents { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
