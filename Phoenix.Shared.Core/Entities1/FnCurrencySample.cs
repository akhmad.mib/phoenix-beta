﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities1
{
    public partial class FncurrencySample : BaseEntity<long>
    {
        //public string Id { get; set; }
        public string CodeCurrency { get; set; }
        public string NameCurrency { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
