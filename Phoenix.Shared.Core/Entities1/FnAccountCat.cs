﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities1
{
    public partial class FnaccountCat : BaseEntity<long>
    {
        //public string Id { get; set; }
        public string ParentId { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Name3 { get; set; }
        public string Name4 { get; set; }
        public string Name5 { get; set; }
        public long? LevelId { get; set; }
        public string GroupId { get; set; }
        public string Condition { get; set; }
        public string Report { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
