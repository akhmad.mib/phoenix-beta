﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrbankBranch : BaseEntity<string>
    {
        
        public string HrbankId { get; set; }
        public string BranchCode { get; set; }
        public string HrbankBranchId { get; set; }
        public string Phone { get; set; }
        public string Adress { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
