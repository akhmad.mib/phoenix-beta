﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Gntemplate : BaseEntity<string>
    {
        
        public string GntemplateTypeId { get; set; }
        public string TemplateName { get; set; }
        public string Extention { get; set; }
        public string Path { get; set; }
        public string Size { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
