﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeAbsence : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public DateTime DateAbsence { get; set; }
        public DateTime TimeCome { get; set; }
        public DateTime TimeBack { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
