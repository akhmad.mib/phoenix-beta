﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmcreativeBrief : BaseEntity<string> {
        
        public string PmclientBriefId { get; set; }
        public string AccountClientId { get; set; }
        public string PlannerId { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? Deadline { get; set; }
        public string PmstatusAssignedId { get; set; }
        public string PmcreativeBriefFileId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
