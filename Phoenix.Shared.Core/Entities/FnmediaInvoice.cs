﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FnmediaInvoice : BaseEntity<string>
    {
        
        public string FninvoiceId { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? InvoiceDueDate { get; set; }
        public string GnexternalId { get; set; }
        public long? ReferenceId { get; set; }
        public string GnexternalMediaVendorId { get; set; }
        public string Placement { get; set; }
        public int? GrossAmount { get; set; }
        public int? NetAmount { get; set; }
        public double? Discount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
