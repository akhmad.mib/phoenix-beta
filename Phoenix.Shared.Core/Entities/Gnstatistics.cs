﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Gnstatistics : BaseEntity<string>
    {
        
        public string GnmoduleId { get; set; }
        public double? Total { get; set; }
        public TimeSpan? HandlingTime { get; set; }
        public int? UsersTotal { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
