﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class Fntransaction : BaseEntity<string>
    {
        
        public int? TransactionName { get; set; }
        public string FntransactionTypeId { get; set; }
        public long? ReferenceId { get; set; }
        public string GnlegalEntityId { get; set; }
        public int? Qty { get; set; }
        public int? FlagPost { get; set; }
        public string GnbusinessUnitId { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? GnuserId { get; set; }
        public string FntransferStatusId { get; set; }
        public string FninvoiceId { get; set; }
        public string FiscalYears { get; set; }
        public string GnuserApprove1 { get; set; }
        public string GnuserApprove2 { get; set; }
        public string GnuserApprove3 { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
