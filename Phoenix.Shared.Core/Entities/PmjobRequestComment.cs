﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobRequestComment : BaseEntity<string> {
        
        public string PmjobRequestTaskId { get; set; }
        public string Comment { get; set; }
        public int? GnuserId { get; set; }
        public DateTime? CommentDate { get; set; }
        public string PmjobRequestCommentFileId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
