﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Gnlogin : BaseEntity<string>
    {
        
        public string GnuserId { get; set; }
        public string IpAddress { get; set; }
        public string Browser { get; set; }
        public DateTime? LoginDate { get; set; }
        public int? LoginStatus { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
