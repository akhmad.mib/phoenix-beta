﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmdcCoalProduction : BaseEntity<string> {
        
        public DateTime Date { get; set; }
        public string Shift { get; set; }
        public int UnitCode { get; set; }
        public int HaulingUnit { get; set; }
        public string Fleet { get; set; }
        public string Trip { get; set; }
        public string BucketTrip { get; set; }
        public string Tonase { get; set; }
        public string PmclientBriefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
