﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeAccount : BaseEntity<string>
    {
        public string EmployeeaccountId { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string HrbankbranchId { get; set; }
        public string AccountNo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
