﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeTraining : BaseEntity<string>
    {
        
        public string NameTraining { get; set; }
        public string HrinstitusionId { get; set; }
        public string Address { get; set; }
        public string StartTraining { get; set; }
        public string EndTraining { get; set; }
        public string NoCertification { get; set; }
        public DateTime? ExpiredCertificate { get; set; }
        public string Description { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string FileUpload { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
