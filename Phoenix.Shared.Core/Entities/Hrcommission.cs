﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrcommission : BaseEntity<string>
    {
        
        public long? HremployeeBasicInfoId { get; set; }
        public string Title { get; set; }
        public int? Amount { get; set; }
        public DateTime? DateCommission { get; set; }
        public string CommissionDescription { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
