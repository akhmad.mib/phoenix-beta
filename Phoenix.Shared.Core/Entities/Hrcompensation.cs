﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrcompensation : BaseEntity<string>
    {
        
        public long HremployeeBasicInfoId { get; set; }
        public DateTime DateCompensation { get; set; }
        public DateTime? EndCompensation { get; set; }
        public string HrcompensationId { get; set; }
        public int Value { get; set; }
        public string Type { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
