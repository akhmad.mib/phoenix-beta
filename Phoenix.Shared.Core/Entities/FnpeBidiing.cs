﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{ 
    public partial class FnpeBidiing : BaseEntity<string>
    {
        
        public string GnexternalId { get; set; }
        public string ItemDesc { get; set; }
        public DateTime? QuotationDueDate { get; set; }
        public int? Qty { get; set; }
        public double? TotalCost { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
