﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeBasicInfo : BaseEntity<string>
    {

        public string EmployeeCode { get; set; }
        public string NameEmployee { get; set; }
        public string HrpositionId { get; set; }
        public string HrstatusEmployeeId { get; set; }
        public string HrnationalityId { get; set; }
        public string BirthPlace { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Email { get; set; }
        public string AdressHome { get; set; }
        public string AdressIdentityCard { get; set; }
        public string PhoneNo { get; set; }
        public string MobilePhone { get; set; }
        public string Gender { get; set; }
        public string HrprovinceId { get; set; }
        public string GncityId { get; set; }
        public string Picture { get; set; }
        public string HrreligionId { get; set; }
        public string GnlegalEntityId { get; set; }
        public string GncentralResourceId { get; set; }
        public string Npwp { get; set; }
        public string Status { get; set; }
        public string Blood { get; set; }
        public string BpjsTk { get; set; }
        public string HrmarriedStatusId { get; set; }
        public string IdentityCard { get; set; }
        public string LastEducation { get; set; }
        public string Probation { get; set; }
        public string Approval { get; set; }
        public DateTime? JoinDate { get; set; }
        public string PayrollMethods { get; set; }
        public string ProcessSalary { get; set; }
        public string KitasNumber { get; set; }
        public string BpjsHealthy { get; set; }
        public string JobGrade { get; set; }
        public string TypeTax { get; set; }
        public DateTime? WarningDate { get; set; }
        public string WorkingStatus { get; set; }
        public DateTime? DateTermination { get; set; }
        public string TerminationReason { get; set; }
        public string FlagDiageo { get; set; }
        public string FileCv { get; set; }
        public DateTime? CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
