﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmheavyEquipment : BaseEntity<string> {
        
        public string HrtypeHeavyEquipmentId { get; set; }
        public string HrassetId { get; set; }
        public string SerialNumber { get; set; }
        public string OwnerOfHeavyEquipment { get; set; }
        public string YearOfHeavyEquipement { get; set; }
        public string Low { get; set; }
        public string Medium { get; set; }
        public string Height { get; set; }
        public string Spesification { get; set; }
        public string PmclientBriefId { get; set; }
        public string Bcm { get; set; }
        public string Ton { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
