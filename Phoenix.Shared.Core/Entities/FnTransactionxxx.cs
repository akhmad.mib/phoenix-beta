﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FnTransactionxxx : BaseEntity<string>
    {
        public long TransactionId { get; set; }
        public int? TransactionName { get; set; }
        public long? TransactionTypeId { get; set; }
        public long? ReferenceId { get; set; }
        public long? LegalEntityId { get; set; }
        public int? Qty { get; set; }
        public int? FlagPost { get; set; }
        public long? Bunit { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? UserId { get; set; }
        public int? TransactionStatus { get; set; }
        public long? InvoiceId { get; set; }
        public int? FiscalYears { get; set; }
        public int? UserApprove1 { get; set; }
        public int? UserApprove2 { get; set; }
        public int? UserApprove3 { get; set; }
        public int? Isdelete { get; set; }
    }
}
