﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobRequestTask : BaseEntity<string> {
        
        public string HrjobPostingRequisitionId { get; set; }
        public string TaskName { get; set; }
        public string Description { get; set; }
        public string PmtaskPriorityId { get; set; }
        public DateTime? DeadlineTask { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
