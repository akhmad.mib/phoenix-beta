﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Phoenix.Shared.Core.Contexts;

namespace Phoenix.Shared.Core.Entities
{
    public partial class AppDbContext : EFContext
    {
        public virtual DbSet<FnAccountCat> FnaccountCat { get; set; }
        public virtual DbSet<FnapprovalStatus> FnapprovalStatus { get; set; }
        public virtual DbSet<FnbankAccountType> FnbankAccountType { get; set; }
        public virtual DbSet<FncashAdvance> FncashAdvance { get; set; }
        public virtual DbSet<FncashAdvanceBidding> FncashAdvanceBidding { get; set; }
        public virtual DbSet<FncashSettlement> FncashSettlement { get; set; }
        public virtual DbSet<FncashTransfer> FncashTransfer { get; set; }
        public virtual DbSet<Fncurrency> Fncurrency { get; set; }
        public virtual DbSet<FnCurrencySample> FnCurrencySample { get; set; }
        public virtual DbSet<FnhistoricalBudget> FnhistoricalBudget { get; set; }
        public virtual DbSet<Fninvoice> Fninvoice { get; set; }
        public virtual DbSet<FninvoiceStatus> FninvoiceStatus { get; set; }
        public virtual DbSet<Fnjournal> Fnjournal { get; set; }
        public virtual DbSet<FnjournalTemp> FnjournalTemp { get; set; }
        public virtual DbSet<FnmediaCategory> FnmediaCategory { get; set; }
        public virtual DbSet<FnmediaEstimate> FnmediaEstimate { get; set; }
        public virtual DbSet<FnmediaInvoice> FnmediaInvoice { get; set; }
        public virtual DbSet<FnmediaOrder> FnmediaOrder { get; set; }
        public virtual DbSet<FnmediaPlan> FnmediaPlan { get; set; }
        public virtual DbSet<Fnpayment> Fnpayment { get; set; }
        public virtual DbSet<FnpeBidiing> FnpeBidiing { get; set; }
        public virtual DbSet<FnpeStatus> FnpeStatus { get; set; }
        public virtual DbSet<Fntransaction> Fntransaction { get; set; }
        public virtual DbSet<FntransactionTemp> FntransactionTemp { get; set; }
        public virtual DbSet<FntransactionType> FntransactionType { get; set; }
        public virtual DbSet<FnTransactionxxx> FnTransactionxxx { get; set; }
        public virtual DbSet<FntransferStatus> FntransferStatus { get; set; }
        public virtual DbSet<GnaccountManagement> GnaccountManagement { get; set; }
        public virtual DbSet<GnbranchCompany> GnbranchCompany { get; set; }
        public virtual DbSet<GnbusinessUnit> GnbusinessUnit { get; set; }
        public virtual DbSet<GnbuttonAction> GnbuttonAction { get; set; }
        public virtual DbSet<GnbuttonFlow> GnbuttonFlow { get; set; }
        public virtual DbSet<GnbuttonRole> GnbuttonRole { get; set; }
        public virtual DbSet<GnbuttonUser> GnbuttonUser { get; set; }
        public virtual DbSet<GncentralResource> GncentralResource { get; set; }
        public virtual DbSet<GncentralResourcesStaff> GncentralResourcesStaff { get; set; }
        public virtual DbSet<GnchatGroup> GnchatGroup { get; set; }
        public virtual DbSet<GnchatMessage> GnchatMessage { get; set; }
        public virtual DbSet<Gncity> Gncity { get; set; }
        public virtual DbSet<GnerrorReporting> GnerrorReporting { get; set; }
        public virtual DbSet<Gnexternal> Gnexternal { get; set; }
        public virtual DbSet<GnexternalType> GnexternalType { get; set; }
        public virtual DbSet<GnfileReportingError> GnfileReportingError { get; set; }
        public virtual DbSet<GnflowAction> GnflowAction { get; set; }
        public virtual DbSet<GnlegalEntity> GnlegalEntity { get; set; }
        public virtual DbSet<GnlogData> GnlogData { get; set; }
        public virtual DbSet<Gnlogin> Gnlogin { get; set; }
        public virtual DbSet<GnmasterStatus> GnmasterStatus { get; set; }
        public virtual DbSet<Gnmodule> Gnmodule { get; set; }
        public virtual DbSet<GnnotifByFlow> GnnotifByFlow { get; set; }
        public virtual DbSet<GnnotifEmail> GnnotifEmail { get; set; }
        public virtual DbSet<Gnpage> Gnpage { get; set; }
        public virtual DbSet<Gnrole> Gnrole { get; set; }
        public virtual DbSet<GnrolePage> GnrolePage { get; set; }
        public virtual DbSet<Gnstatistics> Gnstatistics { get; set; }
        public virtual DbSet<GnsubRole> GnsubRole { get; set; }
        public virtual DbSet<Gntemplate> Gntemplate { get; set; }
        public virtual DbSet<GntemplateType> GntemplateType { get; set; }
        public virtual DbSet<Gnuser> Gnuser { get; set; }
        public virtual DbSet<GnuserAccess> GnuserAccess { get; set; }
        public virtual DbSet<GnuserLog> GnuserLog { get; set; }
        public virtual DbSet<GnuserPage> GnuserPage { get; set; }
        public virtual DbSet<GnuserProfile> GnuserProfile { get; set; }
        public virtual DbSet<GnuserRole> GnuserRole { get; set; }
        public virtual DbSet<GnuserToken> GnuserToken { get; set; }
        public virtual DbSet<GnviewTable> GnviewTable { get; set; }
        public virtual DbSet<Hrachievement> Hrachievement { get; set; }
        public virtual DbSet<Hractivity> Hractivity { get; set; }
        public virtual DbSet<Hrasset> Hrasset { get; set; }
        public virtual DbSet<Hrbank> Hrbank { get; set; }
        public virtual DbSet<HrbankBranch> HrbankBranch { get; set; }
        public virtual DbSet<HrbasicSalary> HrbasicSalary { get; set; }
        public virtual DbSet<Hrbonus> Hrbonus { get; set; }
        public virtual DbSet<HrcandidateRecruitment> HrcandidateRecruitment { get; set; }
        public virtual DbSet<Hrcommission> Hrcommission { get; set; }
        public virtual DbSet<HrcompanyAccount> HrcompanyAccount { get; set; }
        public virtual DbSet<Hrcompensation> Hrcompensation { get; set; }
        public virtual DbSet<Hrcompetence> Hrcompetence { get; set; }
        public virtual DbSet<HrcompetenceLevel> HrcompetenceLevel { get; set; }
        public virtual DbSet<HrcostCenter> HrcostCenter { get; set; }
        public virtual DbSet<HrcostCenterCampaign> HrcostCenterCampaign { get; set; }
        public virtual DbSet<Hrdam> Hrdam { get; set; }
        public virtual DbSet<Hreducation> Hreducation { get; set; }
        public virtual DbSet<HreducationLevel> HreducationLevel { get; set; }
        public virtual DbSet<HremployeeAbsence> HremployeeAbsence { get; set; }
        public virtual DbSet<HremployeeAccount> HremployeeAccount { get; set; }
        public virtual DbSet<HremployeeBank> HremployeeBank { get; set; }
        public virtual DbSet<HremployeeBasicInfo> HremployeeBasicInfo { get; set; }
        public virtual DbSet<HremployeeCampaign> HremployeeCampaign { get; set; }
        public virtual DbSet<HremployeeComplaint> HremployeeComplaint { get; set; }
        public virtual DbSet<HremployeeContract> HremployeeContract { get; set; }
        public virtual DbSet<HremployeeExperience> HremployeeExperience { get; set; }
        public virtual DbSet<HremployeeIdentity> HremployeeIdentity { get; set; }
        public virtual DbSet<HremployeeOut> HremployeeOut { get; set; }
        public virtual DbSet<HremployeeTraining> HremployeeTraining { get; set; }
        public virtual DbSet<HremployeeTrainingRequisition> HremployeeTrainingRequisition { get; set; }
        public virtual DbSet<HremployeeWarning> HremployeeWarning { get; set; }
        public virtual DbSet<HremployementStatus> HremployementStatus { get; set; }
        public virtual DbSet<Hrfamily> Hrfamily { get; set; }
        public virtual DbSet<HrfamilyStatus> HrfamilyStatus { get; set; }
        public virtual DbSet<Hrhelocation> Hrhelocation { get; set; }
        public virtual DbSet<Hrholiday> Hrholiday { get; set; }
        public virtual DbSet<Hrinformation> Hrinformation { get; set; }
        public virtual DbSet<Hrinstitution> Hrinstitution { get; set; }
        public virtual DbSet<HrinstitutionCompetence> HrinstitutionCompetence { get; set; }
        public virtual DbSet<HrinstitutionLevel> HrinstitutionLevel { get; set; }
        public virtual DbSet<HrjobInterview> HrjobInterview { get; set; }
        public virtual DbSet<HrjobInterviewees> HrjobInterviewees { get; set; }
        public virtual DbSet<HrjobInterviewer> HrjobInterviewer { get; set; }
        public virtual DbSet<HrjobPostingRequisition> HrjobPostingRequisition { get; set; }
        public virtual DbSet<HrjobRepository> HrjobRepository { get; set; }
        public virtual DbSet<HrjobTest> HrjobTest { get; set; }
        public virtual DbSet<Hrleave> Hrleave { get; set; }
        public virtual DbSet<HrlevelPosition> HrlevelPosition { get; set; }
        public virtual DbSet<HrloanEmployee> HrloanEmployee { get; set; }
        public virtual DbSet<Hrlocation> Hrlocation { get; set; }
        public virtual DbSet<HrmarriedStatus> HrmarriedStatus { get; set; }
        public virtual DbSet<HrmasterTax> HrmasterTax { get; set; }
        public virtual DbSet<HrmedicalClaim> HrmedicalClaim { get; set; }
        public virtual DbSet<Hrnationality> Hrnationality { get; set; }
        public virtual DbSet<Hroperator> Hroperator { get; set; }
        public virtual DbSet<Hrovertime> Hrovertime { get; set; }
        public virtual DbSet<Hrposition> Hrposition { get; set; }
        public virtual DbSet<Hrpremiaskes> Hrpremiaskes { get; set; }
        public virtual DbSet<Hrprovince> Hrprovince { get; set; }
        public virtual DbSet<Hrreimburse> Hrreimburse { get; set; }
        public virtual DbSet<Hrreligion> Hrreligion { get; set; }
        public virtual DbSet<Hrresignation> Hrresignation { get; set; }
        public virtual DbSet<HrsalaryComponent> HrsalaryComponent { get; set; }
        public virtual DbSet<HrsalaryProcess> HrsalaryProcess { get; set; }
        public virtual DbSet<HrsalaryProcessComponent> HrsalaryProcessComponent { get; set; }
        public virtual DbSet<Hrseminar> Hrseminar { get; set; }
        public virtual DbSet<HrstatusEmployee> HrstatusEmployee { get; set; }
        public virtual DbSet<HrstatusTax> HrstatusTax { get; set; }
        public virtual DbSet<Hrtermination> Hrtermination { get; set; }
        public virtual DbSet<HrtrainingRequisition> HrtrainingRequisition { get; set; }
        public virtual DbSet<Hrtransport> Hrtransport { get; set; }
        public virtual DbSet<Hrtravel> Hrtravel { get; set; }
        public virtual DbSet<HrtravelDestination> HrtravelDestination { get; set; }
        public virtual DbSet<HrtypeHeavyEquipment> HrtypeHeavyEquipment { get; set; }
        public virtual DbSet<HrworkingEffective> HrworkingEffective { get; set; }
        public virtual DbSet<PmapprovalType> PmapprovalType { get; set; }
        public virtual DbSet<PmcampaignProgress> PmcampaignProgress { get; set; }
        public virtual DbSet<PmcampaignStatus> PmcampaignStatus { get; set; }
        public virtual DbSet<PmcampaignType> PmcampaignType { get; set; }
        public virtual DbSet<PmcentralResourceAssigment> PmcentralResourceAssigment { get; set; }
        public virtual DbSet<Pmchannel> Pmchannel { get; set; }
        public virtual DbSet<PmClientBrief> PmclientBrief { get; set; }
        public virtual DbSet<PmClientBriefAccount> PmClientBriefAccount { get; set; }
        public virtual DbSet<PmClientBriefContent> PmClientBriefContent { get; set; }
        public virtual DbSet<PmclientBriefFile> PmclientBriefFile { get; set; }
        public virtual DbSet<PmClientBriefJobRequest> PmClientBriefJobRequest { get; set; }
        public virtual DbSet<PmclientFeedback> PmclientFeedback { get; set; }
        public virtual DbSet<PmclientFeedbackFile> PmclientFeedbackFile { get; set; }
        public virtual DbSet<PmcreativeBrief> PmcreativeBrief { get; set; }
        public virtual DbSet<PmcreativeBriefFile> PmcreativeBriefFile { get; set; }
        public virtual DbSet<PmdcCoalProduction> PmdcCoalProduction { get; set; }
        public virtual DbSet<PmdcFuelConsumption> PmdcFuelConsumption { get; set; }
        public virtual DbSet<PmdcHeavyEquipment> PmdcHeavyEquipment { get; set; }
        public virtual DbSet<PmdcobProduction> PmdcobProduction { get; set; }
        public virtual DbSet<PmdeliverablesFile> PmdeliverablesFile { get; set; }
        public virtual DbSet<PmfileType> PmfileType { get; set; }
        public virtual DbSet<PmheavyEquipment> PmheavyEquipment { get; set; }
        public virtual DbSet<PmjobCategory> PmjobCategory { get; set; }
        public virtual DbSet<PmjobPa> PmjobPa { get; set; }
        public virtual DbSet<PmjobPe> PmjobPe { get; set; }
        public virtual DbSet<PmjobRequestApproval> PmjobRequestApproval { get; set; }
        public virtual DbSet<PmjobRequestComment> PmjobRequestComment { get; set; }
        public virtual DbSet<PmjobRequestCommentFile> PmjobRequestCommentFile { get; set; }
        public virtual DbSet<PmjobRequestCr> PmjobRequestCr { get; set; }
        public virtual DbSet<PmjobRequestFile> PmjobRequestFile { get; set; }
        public virtual DbSet<PmjobRequestTask> PmjobRequestTask { get; set; }
        public virtual DbSet<PmjobRequestTaskUser> PmjobRequestTaskUser { get; set; }
        public virtual DbSet<PmjobRequestTimesheet> PmjobRequestTimesheet { get; set; }
        public virtual DbSet<PmjobStatus> PmjobStatus { get; set; }
        public virtual DbSet<Pmpackage> Pmpackage { get; set; }
        public virtual DbSet<PmpeFile> PmpeFile { get; set; }
        public virtual DbSet<PmpeTemplate> PmpeTemplate { get; set; }
        public virtual DbSet<PmpettyCash> PmpettyCash { get; set; }
        public virtual DbSet<Pmpo> Pmpo { get; set; }
        public virtual DbSet<PmproposalFile> PmproposalFile { get; set; }
        public virtual DbSet<PmproposeType> PmproposeType { get; set; }
        public virtual DbSet<PmrateCard> PmrateCard { get; set; }
        public virtual DbSet<PmreportFile> PmreportFile { get; set; }
        public virtual DbSet<PmstatusAssigned> PmstatusAssigned { get; set; }
        public virtual DbSet<PmsubCampaign> PmsubCampaign { get; set; }
        public virtual DbSet<PmtaskPriority> PmtaskPriority { get; set; }
        public virtual DbSet<PmtaskStatus> PmtaskStatus { get; set; }

        // Unable to generate entity type for table 'dbo.nava_master'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.hremployee_basic_info'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.gnexternal_type'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.hrinformation'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.hrcompetence'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.hrjob_repository'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"data source=139.255.87.235,49172;Initial Catalog=PhoenixDev;Persist Security Info=True;User ID=dev-user;password=d3v-u53r;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FnAccountCat>(entity =>
            {
                entity.ToTable("fnaccount_cat");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Condition)
                    .HasColumnName("condition")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GroupId)
                    .HasColumnName("group_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LevelId).HasColumnName("level_id");

                entity.Property(e => e.Name1)
                    .HasColumnName("name1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name2)
                    .HasColumnName("name2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name3)
                    .HasColumnName("name3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name4)
                    .HasColumnName("name4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name5)
                    .HasColumnName("name5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParentId)
                    .HasColumnName("parent_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Report)
                    .HasColumnName("report")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnapprovalStatus>(entity =>
            {
                entity.ToTable("fnapproval_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ApprovalStatus)
                    .HasColumnName("approval_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnbankAccountType>(entity =>
            {
                entity.ToTable("fnbank_account_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BankAccountType)
                    .HasColumnName("bank_account_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FncashAdvance>(entity =>
            {
                entity.ToTable("fncash_advance");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AdvanceAmount).HasColumnName("advance_amount");

                entity.Property(e => e.AdvanceDate)
                    .HasColumnName("advance_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FntransferStatusId)
                    .HasColumnName("fntransfer_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalId)
                    .HasColumnName("gnexternal_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferBankAccname)
                    .HasColumnName("transfer_bank_accname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferBankAccno)
                    .HasColumnName("transfer_bank_accno")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FncashAdvanceBidding>(entity =>
            {
                entity.ToTable("fncash_advance_bidding");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AmountCash).HasColumnName("amount_cash");

                entity.Property(e => e.ApprovalStatusId).HasColumnName("approval_status_id");

                entity.Property(e => e.CashDate)
                    .HasColumnName("cash_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FinanceApprovalId).HasColumnName("finance_approval_id");

                entity.Property(e => e.FncashTransferId)
                    .HasColumnName("fncash_transfer_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnbusinessUnitApprovalId)
                    .HasColumnName("gnbusiness_unit_approval_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmjobPeId)
                    .HasColumnName("pmjob_pe_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FncashSettlement>(entity =>
            {
                entity.ToTable("fncash_settlement");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FlagSettlement).HasColumnName("flag_settlement");

                entity.Property(e => e.FntransactionId)
                    .HasColumnName("fntransaction_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SettlementDate)
                    .HasColumnName("settlement_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FncashTransfer>(entity =>
            {
                entity.ToTable("fncash_transfer");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FntransferStatusId)
                    .HasColumnName("fntransfer_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalD)
                    .HasColumnName("gnexternal_d")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferAmount).HasColumnName("transfer_amount");

                entity.Property(e => e.TransferBankAccname)
                    .HasColumnName("transfer_bank_accname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferBankAccno)
                    .HasColumnName("transfer_bank_accno")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferDate)
                    .HasColumnName("transfer_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Fncurrency>(entity =>
            {
                entity.ToTable("fncurrency");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CodeCurrency)
                    .IsRequired()
                    .HasColumnName("code_currency")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NameCurrency)
                    .IsRequired()
                    .HasColumnName("name_currency")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnCurrencySample>(entity =>
            {
                entity.ToTable("fncurrency_sample");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CodeCurrency)
                    .HasColumnName("code_currency")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NameCurrency)
                    .HasColumnName("name_currency")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnhistoricalBudget>(entity =>
            {
                entity.ToTable("fnhistorical_budget");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Balance).HasColumnName("balance");

                entity.Property(e => e.BeginCredit).HasColumnName("begin_credit");

                entity.Property(e => e.BeginDebit).HasColumnName("begin_debit");

                entity.Property(e => e.BudgedCredit).HasColumnName("budged_credit");

                entity.Property(e => e.BudgedDebit).HasColumnName("budged_debit");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndingCredit).HasColumnName("ending_credit");

                entity.Property(e => e.EndingDebet).HasColumnName("ending_debet");

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.FlagTime)
                    .HasColumnName("flag_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnaccountcatAccountId)
                    .HasColumnName("fnaccountcat_account_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnbusinessUnitId)
                    .HasColumnName("gnbusiness_unit_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MutasiCredit).HasColumnName("mutasi_credit");

                entity.Property(e => e.MutasiDebet).HasColumnName("mutasi_debet");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoId).HasColumnName("po_id");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.Quartal).HasColumnName("quartal");

                entity.Property(e => e.ReferenceDate)
                    .HasColumnName("reference_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ReferenceId)
                    .HasColumnName("reference_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Unit).HasColumnName("unit");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Year).HasColumnName("year");
            });

            modelBuilder.Entity<Fninvoice>(entity =>
            {
                entity.ToTable("fninvoice");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FninvoiceStatusId)
                    .HasColumnName("fninvoice_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalId)
                    .HasColumnName("gnexternal_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceAmount).HasColumnName("invoice_amount");

                entity.Property(e => e.InvoiceDate)
                    .HasColumnName("invoice_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvoiceDueDate)
                    .HasColumnName("invoice_due_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PaidDate)
                    .HasColumnName("paid_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PmjobPeId)
                    .HasColumnName("pmjob_pe_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FninvoiceStatus>(entity =>
            {
                entity.ToTable("fninvoice_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvoiceStatus)
                    .HasColumnName("invoice_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Fnjournal>(entity =>
            {
                entity.ToTable("fnjournal");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Balance).HasColumnName("balance");

                entity.Property(e => e.BeginCredit).HasColumnName("begin_credit");

                entity.Property(e => e.BeginDebit).HasColumnName("begin_debit");

                entity.Property(e => e.BudgedCredit).HasColumnName("budged_credit");

                entity.Property(e => e.BudgedDebit).HasColumnName("budged_debit");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndingCredit).HasColumnName("ending_credit");

                entity.Property(e => e.EndingDebet).HasColumnName("ending_debet");

                entity.Property(e => e.FiscalYears).HasColumnName("fiscal_years");

                entity.Property(e => e.FlagTime)
                    .HasColumnName("flag_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnaccountcatAccountId)
                    .HasColumnName("fnaccountcat_account_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnbusinessUnitId)
                    .HasColumnName("gnbusiness_unit_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MutasiCredit).HasColumnName("mutasi_credit");

                entity.Property(e => e.MutasiDebet).HasColumnName("mutasi_debet");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoId).HasColumnName("po_id");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.ReferenceDate)
                    .HasColumnName("reference_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Unit).HasColumnName("unit");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<FnjournalTemp>(entity =>
            {
                entity.ToTable("fnjournal_temp");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Balance).HasColumnName("balance");

                entity.Property(e => e.BeginCredit).HasColumnName("begin_credit");

                entity.Property(e => e.BeginDebit).HasColumnName("begin_debit");

                entity.Property(e => e.BudgedCredit).HasColumnName("budged_credit");

                entity.Property(e => e.BudgedDebit).HasColumnName("budged_debit");

                entity.Property(e => e.Bunit).HasColumnName("bunit");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndingCredit).HasColumnName("ending_credit");

                entity.Property(e => e.EndingDebet).HasColumnName("ending_debet");

                entity.Property(e => e.FlagTime)
                    .HasColumnName("flag_time")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnaccountcatAccountId)
                    .HasColumnName("fnaccountcat_account_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MutasiCredit).HasColumnName("mutasi_credit");

                entity.Property(e => e.MutasiDebet).HasColumnName("mutasi_debet");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoId).HasColumnName("po_id");

                entity.Property(e => e.Price).HasColumnName("price");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.ReferenceDate)
                    .HasColumnName("reference_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ReferenceId).HasColumnName("reference_id");

                entity.Property(e => e.Unit).HasColumnName("unit");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<FnmediaCategory>(entity =>
            {
                entity.ToTable("fnmedia_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Category)
                    .HasColumnName("category")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnmediaEstimate>(entity =>
            {
                entity.ToTable("fnmedia_estimate");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EstimateDate)
                    .HasColumnName("estimate_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnapprovalStatus)
                    .HasColumnName("fnapproval_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FnmediaPlanId)
                    .HasColumnName("fnmedia_plan_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FntransactionId)
                    .HasColumnName("fntransaction_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalId)
                    .HasColumnName("gnexternal_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnmediaInvoice>(entity =>
            {
                entity.ToTable("fnmedia_invoice");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnName("discount");

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalId)
                    .HasColumnName("gnexternal_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalMediaVendorId)
                    .HasColumnName("gnexternal_media_vendor_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrossAmount).HasColumnName("gross_amount");

                entity.Property(e => e.InvoiceDate)
                    .HasColumnName("invoice_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InvoiceDueDate)
                    .HasColumnName("invoice_due_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NetAmount).HasColumnName("net_amount");

                entity.Property(e => e.Placement)
                    .HasColumnName("placement")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceId).HasColumnName("reference_id");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnmediaOrder>(entity =>
            {
                entity.ToTable("fnmedia_order");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnexternalClientId)
                    .HasColumnName("gnexternal_client_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalVendorId)
                    .HasColumnName("gnexternal_vendor_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.OrderDate)
                    .HasColumnName("order_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.OrderDetail)
                    .HasColumnName("order_detail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmpoId)
                    .HasColumnName("pmpo_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenceId).HasColumnName("reference_id");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnmediaPlan>(entity =>
            {
                entity.ToTable("fnmedia_plan");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AgencyFee).HasColumnName("agency_fee");

                entity.Property(e => e.AiringDetail)
                    .HasColumnName("airing_detail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnName("discount");

                entity.Property(e => e.FinishDate)
                    .HasColumnName("finish_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnexternalCustomerId)
                    .HasColumnName("gnexternal_customer_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalMediaVendorId)
                    .HasColumnName("gnexternal_media_vendor_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GrossAmount).HasColumnName("gross_amount");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NetAmount).HasColumnName("net_amount");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Fnpayment>(entity =>
            {
                entity.ToTable("fnpayment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ChequeGiroDate)
                    .HasColumnName("cheque_giro_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ChequeGiroNo)
                    .HasColumnName("cheque_giro_no")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CollectionAmount).HasColumnName("collection_amount");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DueDate)
                    .HasColumnName("due_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EntryDate)
                    .HasColumnName("entry_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FntransactionId)
                    .HasColumnName("fntransaction_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FntransactionTypeId)
                    .HasColumnName("fntransaction_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrbankAccountId)
                    .HasColumnName("hrbank_account_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TransactionDate)
                    .HasColumnName("transaction_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnpeBidiing>(entity =>
            {
                entity.ToTable("fnpe_bidiing");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnexternalId)
                    .HasColumnName("gnexternal_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ItemDesc)
                    .HasColumnName("item_desc")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.QuotationDueDate)
                    .HasColumnName("quotation_due_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.TotalCost).HasColumnName("total_cost");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnpeStatus>(entity =>
            {
                entity.ToTable("fnpe_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PeStatus)
                    .HasColumnName("pe_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Fntransaction>(entity =>
            {
                entity.ToTable("fntransaction");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EntryDate)
                    .HasColumnName("entry_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FiscalYears)
                    .HasColumnName("fiscal_years")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FlagPost).HasColumnName("flag_post");

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FntransactionTypeId)
                    .HasColumnName("fntransaction_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FntransferStatusId)
                    .HasColumnName("fntransfer_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnbusinessUnitId)
                    .HasColumnName("gnbusiness_unit_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserApprove1)
                    .HasColumnName("gnuser_approve1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserApprove2)
                    .HasColumnName("gnuser_approve2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserApprove3)
                    .HasColumnName("gnuser_approve3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId).HasColumnName("gnuser_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.ReferenceId).HasColumnName("reference_id");

                entity.Property(e => e.TransactionName).HasColumnName("transaction_name");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FntransactionTemp>(entity =>
            {
                entity.ToTable("fntransaction_temp");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FlagPost).HasColumnName("flag_post");

                entity.Property(e => e.FninvoiceId)
                    .HasColumnName("fninvoice_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FntransferStatusId)
                    .HasColumnName("fntransfer_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnbusinessUnitId)
                    .HasColumnName("gnbusiness_unit_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.ReferenceId).HasColumnName("reference_id");

                entity.Property(e => e.TransactionName)
                    .HasColumnName("transaction_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FntransactionType>(entity =>
            {
                entity.ToTable("fntransaction_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TransactionType)
                    .HasColumnName("transaction_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<FnTransactionxxx>(entity =>
            {
                entity.HasKey(e => e.TransactionId);

                entity.Property(e => e.TransactionId).HasColumnName("transaction_id");

                entity.Property(e => e.Bunit).HasColumnName("bunit");

                entity.Property(e => e.EntryDate)
                    .HasColumnName("entry_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FiscalYears).HasColumnName("fiscal_years");

                entity.Property(e => e.FlagPost).HasColumnName("flag_post");

                entity.Property(e => e.InvoiceId).HasColumnName("invoice_id");

                entity.Property(e => e.Isdelete)
                    .HasColumnName("isdelete")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LegalEntityId).HasColumnName("legal_entity_id");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.ReferenceId).HasColumnName("reference_id");

                entity.Property(e => e.TransactionName).HasColumnName("transaction_name");

                entity.Property(e => e.TransactionStatus).HasColumnName("transaction_status");

                entity.Property(e => e.TransactionTypeId).HasColumnName("transaction_type_id");

                entity.Property(e => e.UserApprove1).HasColumnName("user_approve1");

                entity.Property(e => e.UserApprove2).HasColumnName("user_approve2");

                entity.Property(e => e.UserApprove3).HasColumnName("user_approve3");

                entity.Property(e => e.UserId).HasColumnName("user_id");
            });

            modelBuilder.Entity<FntransferStatus>(entity =>
            {
                entity.ToTable("fntransfer_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TransferStatus)
                    .HasColumnName("transfer_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnaccountManagement>(entity =>
            {
                entity.ToTable("gnaccount_management");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountManagement)
                    .HasColumnName("account_management")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnbranchCompany>(entity =>
            {
                entity.ToTable("gnbranch_company");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasColumnType("text");

                entity.Property(e => e.BranchCode)
                    .IsRequired()
                    .HasColumnName("branch_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasColumnName("branch_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DayWorking)
                    .IsRequired()
                    .HasColumnName("day_working")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .IsRequired()
                    .HasColumnName("fax")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Gncity)
                    .IsRequired()
                    .HasColumnName("gncity")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PayrollProcessDate)
                    .HasColumnName("payroll_process_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .IsRequired()
                    .HasColumnName("province")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnbusinessUnit>(entity =>
            {
                entity.ToTable("gnbusiness_unit");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.GnbusinessUnitId)
                    .HasColumnName("gnbusiness_unit_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnbuttonAction>(entity =>
            {
                entity.ToTable("gnbutton_action");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ButtonGroupUserId).HasColumnName("button_group_user_id");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnbuttonFlowId)
                    .HasColumnName("gnbutton_flow_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnbuttonUserId)
                    .HasColumnName("gnbutton_user_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnpageId)
                    .HasColumnName("gnpage_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnbuttonFlow>(entity =>
            {
                entity.ToTable("gnbutton_flow");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnbuttonActionId)
                    .HasColumnName("gnbutton_action_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnflowActionId)
                    .HasColumnName("gnflow_action_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnbuttonRole>(entity =>
            {
                entity.ToTable("gnbutton_role");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnbuttonActionId)
                    .HasColumnName("gnbutton_action_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnroleId)
                    .HasColumnName("gnrole_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnbuttonUser>(entity =>
            {
                entity.ToTable("gnbutton_user");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnbuttonUser1)
                    .HasColumnName("gnbutton_user")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserPage)
                    .HasColumnName("gnuser_page")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GncentralResource>(entity =>
            {
                entity.ToTable("gncentral_resource");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnbussinessUnitId)
                    .HasColumnName("gnbussiness_unit_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GncentralResourcesStaff>(entity =>
            {
                entity.ToTable("gncentral_resources_staff");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GncentralResourcesStaff1)
                    .HasColumnName("gncentral_resources_staff")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnchatGroup>(entity =>
            {
                entity.ToTable("gnchat_group");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GroupName)
                    .HasColumnName("group_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnchatMessage>(entity =>
            {
                entity.ToTable("gnchat_message");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ChatReadtime)
                    .HasColumnName("chat_readtime")
                    .HasColumnType("datetime");

                entity.Property(e => e.ChatSendtime)
                    .HasColumnName("chat_sendtime")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FromId)
                    .HasColumnName("from_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastSend)
                    .HasColumnName("last_send")
                    .HasColumnType("datetime");

                entity.Property(e => e.Messages)
                    .HasColumnName("messages")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToGroup)
                    .HasColumnName("to_group")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ToId)
                    .HasColumnName("to_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gncity>(entity =>
            {
                entity.ToTable("gncity");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnerrorReporting>(entity =>
            {
                entity.ToTable("gnerror_reporting");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ContentNote)
                    .HasColumnName("content_note")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Information)
                    .HasColumnName("information")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gnexternal>(entity =>
            {
                entity.ToTable("gnexternal");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountClient)
                    .HasColumnName("account_client")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingAddress)
                    .HasColumnName("billing_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CityId)
                    .HasColumnName("city_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailCp)
                    .HasColumnName("email_cp")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalName)
                    .HasColumnName("external_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnexternalTypeId)
                    .HasColumnName("gnexternal_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HpCp)
                    .HasColumnName("hp_cp")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LegalName)
                    .HasColumnName("legal_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MediaCategory)
                    .HasColumnName("media_category")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nation)
                    .HasColumnName("nation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PersonalInchage)
                    .HasColumnName("personal_inchage")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .HasColumnName("phone1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .HasColumnName("phone2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress)
                    .HasColumnName("shipping_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SocialMedia)
                    .HasColumnName("social_media")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TaxRegNo)
                    .HasColumnName("tax_reg_no")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Website)
                    .HasColumnName("website")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Zipcode)
                    .HasColumnName("zipcode")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GnexternalType>(entity =>
            {
                entity.ToTable("gnexternal_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExternalType)
                    .HasColumnName("external_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnfileReportingError>(entity =>
            {
                entity.ToTable("gnfile_reporting_error");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Eksistensi)
                    .HasColumnName("eksistensi")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileId)
                    .HasColumnName("file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Path)
                    .HasColumnName("path")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnflowAction>(entity =>
            {
                entity.ToTable("gnflow_action");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnbuttonFlowId)
                    .HasColumnName("gnbutton_flow_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnmasterStatusId)
                    .HasColumnName("gnmaster_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnlegalEntity>(entity =>
            {
                entity.ToTable("gnlegal_entity");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DirectorName)
                    .HasColumnName("director_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LegalEntityName)
                    .HasColumnName("legal_entity_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NavaStructureId)
                    .HasColumnName("nava_structure_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NpwpNo)
                    .HasColumnName("npwp_no")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnlogData>(entity =>
            {
                entity.ToTable("gnlog_data");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateLog)
                    .HasColumnName("date_log")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TextLog)
                    .HasColumnName("text_log")
                    .HasColumnType("text");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Gnlogin>(entity =>
            {
                entity.ToTable("gnlogin");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Browser)
                    .HasColumnName("browser")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpAddress)
                    .HasColumnName("ip_address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LoginDate)
                    .HasColumnName("login_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.LoginStatus).HasColumnName("login_status");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnmasterStatus>(entity =>
            {
                entity.ToTable("gnmaster_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnflowActionId)
                    .HasColumnName("gnflow_action_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gnmodule>(entity =>
            {
                entity.ToTable("gnmodule");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ModuleFunction)
                    .HasColumnName("module_function")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModuleName)
                    .HasColumnName("module_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParentsId).HasColumnName("parents_id");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnnotifByFlow>(entity =>
            {
                entity.ToTable("gnnotif_by_flow");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnflowActionId)
                    .HasColumnName("gnflow_action_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnnotifEmailId)
                    .HasColumnName("gnnotif_email_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnnotifEmail>(entity =>
            {
                entity.ToTable("gnnotif_email");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmailFrom)
                    .HasColumnName("email_from")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmailTo)
                    .HasColumnName("email_to")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gnpage>(entity =>
            {
                entity.ToTable("gnpage");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnmoduleId)
                    .HasColumnName("gnmodule_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Page)
                    .HasColumnName("page")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PageAddress)
                    .HasColumnName("page_address")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gnrole>(entity =>
            {
                entity.ToTable("gnrole");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Role)
                    .HasColumnName("role")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnrolePage>(entity =>
            {
                entity.ToTable("gnrole_page");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnpageId)
                    .HasColumnName("gnpage_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnroleId)
                    .HasColumnName("gnrole_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gnstatistics>(entity =>
            {
                entity.ToTable("gnstatistics");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GnmoduleId)
                    .HasColumnName("gnmodule_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HandlingTime).HasColumnName("handling_time");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Total).HasColumnName("total");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UsersTotal).HasColumnName("users_total");
            });

            modelBuilder.Entity<GnsubRole>(entity =>
            {
                entity.ToTable("gnsub_role");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.RoleId)
                    .HasColumnName("role_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubroleName)
                    .HasColumnName("subrole_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gntemplate>(entity =>
            {
                entity.ToTable("gntemplate");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Extention)
                    .HasColumnName("extention")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GntemplateTypeId)
                    .HasColumnName("gntemplate_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Path)
                    .HasColumnName("path")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TemplateName)
                    .HasColumnName("template_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GntemplateType>(entity =>
            {
                entity.ToTable("gntemplate_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TemplateExtention)
                    .HasColumnName("template_extention")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TemplateType)
                    .HasColumnName("template_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TemplateTypeName)
                    .HasColumnName("template_type_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Gnuser>(entity =>
            {
                entity.ToTable("gnuser");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DigitalSignature)
                    .HasColumnName("digital_signature")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FlagActive).HasColumnName("flag_active");

                entity.Property(e => e.GnbusinessUnitId).HasColumnName("gnbusiness_unit_id");

                entity.Property(e => e.GnexternalId).HasColumnName("gnexternal_id");

                entity.Property(e => e.GnroleId)
                    .HasColumnName("gnrole_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .HasColumnName("username")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GnuserAccess>(entity =>
            {
                entity.ToTable("gnuser_access");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ActionDelete).HasColumnName("action_delete");

                entity.Property(e => e.ActionInsert).HasColumnName("action_insert");

                entity.Property(e => e.ActionUpdate).HasColumnName("action_update");

                entity.Property(e => e.ActionView).HasColumnName("action_view");

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FntransactionId)
                    .HasColumnName("fntransaction_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnmoduleId)
                    .HasColumnName("gnmodule_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnroleId)
                    .HasColumnName("gnrole_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrpositionId)
                    .HasColumnName("hrposition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnuserLog>(entity =>
            {
                entity.ToTable("gnuser_log");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Action)
                    .HasColumnName("action")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Information)
                    .HasColumnName("information")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnuserPage>(entity =>
            {
                entity.ToTable("gnuser_page");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnpageId)
                    .HasColumnName("gnpage_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnuserProfile>(entity =>
            {
                entity.ToTable("gnuser_profile");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Experience)
                    .HasColumnName("experience")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrcompetenceId)
                    .HasColumnName("hrcompetence_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrtrainingRequisitionId)
                    .HasColumnName("hrtraining_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Photo)
                    .HasColumnName("photo")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnuserRole>(entity =>
            {
                entity.ToTable("gnuser_role");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnroleId)
                    .HasColumnName("gnrole_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<GnuserToken>(entity =>
            {
                entity.ToTable("gnuser_token");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Agent)
                    .IsRequired()
                    .HasColumnName("agent")
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Ipaddress)
                    .IsRequired()
                    .HasColumnName("ipaddress")
                    .HasMaxLength(50);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Token)
                    .IsRequired()
                    .HasColumnName("token")
                    .HasMaxLength(200);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasColumnName("user_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GnviewTable>(entity =>
            {
                entity.ToTable("gnview_table");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FieldName)
                    .HasColumnName("field_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrachievement>(entity =>
            {
                entity.ToTable("hrachievement");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AchievementDate)
                    .HasColumnName("achievement_date")
                    .HasColumnType("date");

                entity.Property(e => e.AchievementDescription)
                    .HasColumnName("achievement_description")
                    .HasColumnType("text");

                entity.Property(e => e.AchievementTitle)
                    .HasColumnName("achievement_title")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hractivity>(entity =>
            {
                entity.ToTable("hractivity");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrasset>(entity =>
            {
                entity.ToTable("hrasset");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Asset)
                    .HasColumnName("asset")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrbank>(entity =>
            {
                entity.ToTable("hrbank");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BankCode)
                    .IsRequired()
                    .HasColumnName("bank_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasColumnName("bank_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrbankBranch>(entity =>
            {
                entity.ToTable("hrbank_branch");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Adress)
                    .IsRequired()
                    .HasColumnName("adress")
                    .HasColumnType("text");

                entity.Property(e => e.BranchCode)
                    .IsRequired()
                    .HasColumnName("branch_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrbankBranchId)
                    .IsRequired()
                    .HasColumnName("hrbank_branch_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrbankId)
                    .IsRequired()
                    .HasColumnName("hrbank_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrbasicSalary>(entity =>
            {
                entity.ToTable("hrbasic_salary");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Hrbonus>(entity =>
            {
                entity.ToTable("hrbonus");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.BonusDescription)
                    .HasColumnName("bonus_description")
                    .HasColumnType("text");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateBonus)
                    .HasColumnName("date_bonus")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrcandidateRecruitment>(entity =>
            {
                entity.ToTable("hrcandidate_recruitment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Achievement)
                    .HasColumnName("achievement")
                    .HasColumnType("text");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasColumnType("text");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateBirth)
                    .HasColumnName("date_birth")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasColumnName("first_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Fullname)
                    .IsRequired()
                    .HasColumnName("fullname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GncityId)
                    .HasColumnName("gncity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdentityCard)
                    .HasColumnName("identity_card")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Institusion)
                    .IsRequired()
                    .HasColumnName("institusion")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Interest)
                    .HasColumnName("interest")
                    .HasColumnType("text");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastEducation)
                    .IsRequired()
                    .HasColumnName("last_education")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasColumnName("last_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.LookingJob).HasColumnName("looking_job");

                entity.Property(e => e.Major)
                    .HasColumnName("major")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MaritialStatus)
                    .HasColumnName("maritial_status")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Married).HasColumnName("married");

                entity.Property(e => e.MobileNumber)
                    .HasColumnName("mobile_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Nasionality)
                    .HasColumnName("nasionality")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasColumnName("phone_number")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProfileFile)
                    .IsRequired()
                    .HasColumnName("profile_file")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .HasColumnName("province")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RelationPeople)
                    .IsRequired()
                    .HasColumnName("relation_people")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Religion).HasColumnName("religion");

                entity.Property(e => e.ResumeScreening).HasColumnName("resume_screening");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.WorkNumber)
                    .HasColumnName("work_number")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ZipCode)
                    .HasColumnName("zip_code")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Hrcommission>(entity =>
            {
                entity.ToTable("hrcommission");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.CommissionDescription)
                    .HasColumnName("commission_description")
                    .HasColumnType("text");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateCommission)
                    .HasColumnName("date_commission")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrcompanyAccount>(entity =>
            {
                entity.ToTable("hrcompany_account");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BankAccountId)
                    .IsRequired()
                    .HasColumnName("bank_account_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasColumnName("branch_name")
                    .HasColumnType("text");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrbankId)
                    .IsRequired()
                    .HasColumnName("hrbank_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrbankbranchId)
                    .IsRequired()
                    .HasColumnName("hrbankbranch_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrbranchcomapanyId)
                    .IsRequired()
                    .HasColumnName("hrbranchcomapany_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrcompensation>(entity =>
            {
                entity.ToTable("hrcompensation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateCompensation)
                    .HasColumnName("date_compensation")
                    .HasColumnType("date");

                entity.Property(e => e.EndCompensation)
                    .HasColumnName("end_compensation")
                    .HasColumnType("date");

                entity.Property(e => e.HrcompensationId)
                    .IsRequired()
                    .HasColumnName("hrcompensation_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Hrcompetence>(entity =>
            {
                entity.ToTable("hrcompetence");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CompetenceName)
                    .HasColumnName("competence_name")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Level1)
                    .HasColumnName("level1")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Level2)
                    .HasColumnName("level2")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Level3)
                    .HasColumnName("level3")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Level4)
                    .HasColumnName("level4")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Level5)
                    .HasColumnName("level5")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrcompetenceLevel>(entity =>
            {
                entity.ToTable("hrcompetence_level");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrcompetenceId)
                    .HasColumnName("hrcompetence_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrlevelPositionId)
                    .HasColumnName("hrlevel_position_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LevelReq).HasColumnName("level_req");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrcostCenter>(entity =>
            {
                entity.ToTable("hrcost_center");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ActivityId)
                    .IsRequired()
                    .HasColumnName("activity_id")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CostId)
                    .IsRequired()
                    .HasColumnName("cost_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndPeriode)
                    .HasColumnName("end_periode")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Percentage)
                    .IsRequired()
                    .HasColumnName("percentage")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Periode)
                    .HasColumnName("periode")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrcostCenterCampaign>(entity =>
            {
                entity.ToTable("hrcost_center_campaign");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Cost).HasColumnName("cost");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.GnexternalClientId).HasColumnName("gnexternal_client_id");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.PhoneNo)
                    .HasColumnName("phone_no")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrdam>(entity =>
            {
                entity.ToTable("hrdam");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dam)
                    .HasColumnName("dam")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileFinal).HasColumnName("file_final");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hreducation>(entity =>
            {
                entity.ToTable("hreducation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndEducation)
                    .IsRequired()
                    .HasColumnName("end_education")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HreducationLevelId)
                    .IsRequired()
                    .HasColumnName("hreducation_level_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.HrinstitutionId)
                    .IsRequired()
                    .HasColumnName("hrinstitution_id")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Majors)
                    .HasColumnName("majors")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StartEducation)
                    .IsRequired()
                    .HasColumnName("start_education")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HreducationLevel>(entity =>
            {
                entity.ToTable("hreducation_level");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EducationName)
                    .HasColumnName("education_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeAbsence>(entity =>
            {
                entity.ToTable("hremployee_absence");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateAbsence)
                    .HasColumnName("date_absence")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TimeBack)
                    .HasColumnName("time_back")
                    .HasColumnType("datetime");

                entity.Property(e => e.TimeCome)
                    .HasColumnName("time_come")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeAccount>(entity =>
            {
                entity.HasKey(e => e.EmployeeaccountId);

                entity.ToTable("hremployee_account");

                entity.Property(e => e.EmployeeaccountId)
                    .HasColumnName("employeeaccount_id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountNo)
                    .IsRequired()
                    .HasColumnName("account_no")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrbankbranchId)
                    .IsRequired()
                    .HasColumnName("hrbankbranch_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeBank>(entity =>
            {
                entity.ToTable("hremployee_bank");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountName)
                    .HasColumnName("account_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AccountNo)
                    .HasColumnName("account_no")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.FncurrencyId)
                    .HasColumnName("fncurrency_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrbankId)
                    .HasColumnName("hrbank_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrsubBankId)
                    .HasColumnName("hrsub_bank_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeBasicInfo>(entity =>
            {
                entity.ToTable("hremployee_basic_info");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AdressHome)
                    .HasColumnName("adress_home")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.AdressIdentityCard)
                    .HasColumnName("adress_identity_card")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Approval)
                    .HasColumnName("approval")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BirthDate)
                    .HasColumnName("birth_date")
                    .HasColumnType("date");

                entity.Property(e => e.BirthPlace)
                    .HasColumnName("birth_place")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Blood)
                    .HasColumnName("blood")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.BpjsHealthy)
                    .HasColumnName("bpjs_healthy")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BpjsTk)
                    .HasColumnName("bpjs_tk")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasMaxLength(255);

                entity.Property(e => e.DateTermination)
                    .HasColumnName("date_termination")
                    .HasColumnType("date");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeCode)
                    .HasColumnName("employee_code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileCv)
                    .HasColumnName("file_cv")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.FlagDiageo)
                    .HasColumnName("flag_diageo")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GncityId)
                    .HasColumnName("gncity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrmarriedStatusId)
                    .HasColumnName("hrmarried_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrnationalityId)
                    .HasColumnName("hrnationality_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrpositionId)
                    .HasColumnName("hrposition_id")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HrprovinceId)
                    .HasColumnName("hrprovince_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrreligionId)
                    .HasColumnName("hrreligion_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrstatusEmployeeId)
                    .HasColumnName("hrstatus_employee_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdentityCard)
                    .HasColumnName("identity_card")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobGrade)
                    .HasColumnName("job_grade")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.JoinDate)
                    .HasColumnName("join_date")
                    .HasColumnType("date");

                entity.Property(e => e.KitasNumber)
                    .HasColumnName("kitas_number")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastEducation)
                    .HasColumnName("last_education")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MobilePhone)
                    .HasColumnName("mobile_phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NameEmployee)
                    .HasColumnName("name_employee")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Npwp)
                    .HasColumnName("npwp")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayrollMethods)
                    .HasColumnName("payroll_methods")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .HasColumnName("phone_no")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Picture)
                    .HasColumnName("picture")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Probation)
                    .HasColumnName("probation")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProcessSalary)
                    .HasColumnName("process_salary")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TerminationReason)
                    .HasColumnName("termination_reason")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.TypeTax)
                    .HasColumnName("type_tax")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.WarningDate)
                    .HasColumnName("warning_date")
                    .HasColumnType("date");

                entity.Property(e => e.WorkingStatus)
                    .HasColumnName("working_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HremployeeCampaign>(entity =>
            {
                entity.ToTable("hremployee_campaign");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.FileUpload)
                    .HasColumnName("file_upload")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmsubcampaignId)
                    .HasColumnName("pmsubcampaign_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeComplaint>(entity =>
            {
                entity.ToTable("hremployee_complaint");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ComplaintDate)
                    .HasColumnName("complaint_date")
                    .HasColumnType("date");

                entity.Property(e => e.ComplaintDescription)
                    .HasColumnName("complaint_description")
                    .HasColumnType("text");

                entity.Property(e => e.ComplaintTitle)
                    .HasColumnName("complaint_title")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeContract>(entity =>
            {
                entity.ToTable("hremployee_contract");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Active).HasColumnName("active");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeExperience>(entity =>
            {
                entity.ToTable("hremployee_experience");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .IsRequired()
                    .HasColumnName("end_date")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .IsRequired()
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrpositionId)
                    .IsRequired()
                    .HasColumnName("hrposition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate)
                    .IsRequired()
                    .HasColumnName("start_date")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeIdentity>(entity =>
            {
                entity.ToTable("hremployee_identity");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Expired)
                    .HasColumnName("expired")
                    .HasColumnType("date");

                entity.Property(e => e.FileUpload)
                    .HasColumnName("file_upload")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NameCard)
                    .HasColumnName("name_card")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoCard)
                    .HasColumnName("no_card")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Published)
                    .HasColumnName("published")
                    .HasColumnType("date");

                entity.Property(e => e.PublishedBy)
                    .HasColumnName("published_by")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeOut>(entity =>
            {
                entity.ToTable("hremployee_out");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateOut)
                    .HasColumnName("date_out")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("text");

                entity.Property(e => e.TimeComeback)
                    .IsRequired()
                    .HasColumnName("time_comeback")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TimeOut)
                    .IsRequired()
                    .HasColumnName("time_out")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeTraining>(entity =>
            {
                entity.ToTable("hremployee_training");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasColumnName("address")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndTraining)
                    .IsRequired()
                    .HasColumnName("end_training")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExpiredCertificate)
                    .HasColumnName("expired_certificate")
                    .HasColumnType("date");

                entity.Property(e => e.FileUpload)
                    .HasColumnName("file_upload")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrinstitusionId)
                    .IsRequired()
                    .HasColumnName("hrinstitusion_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NameTraining)
                    .IsRequired()
                    .HasColumnName("name_training")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoCertification)
                    .IsRequired()
                    .HasColumnName("no_certification")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StartTraining)
                    .IsRequired()
                    .HasColumnName("start_training")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeTrainingRequisition>(entity =>
            {
                entity.ToTable("hremployee_training_requisition");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HremployeeWarning>(entity =>
            {
                entity.ToTable("hremployee_warning");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.WarningBy)
                    .HasColumnName("warning_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WarningDate)
                    .HasColumnName("warning_date")
                    .HasColumnType("date");

                entity.Property(e => e.WarningTo)
                    .HasColumnName("warning_to")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<HremployementStatus>(entity =>
            {
                entity.ToTable("hremployement_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EmploymentStatusName)
                    .HasColumnName("employment_status_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrfamily>(entity =>
            {
                entity.ToTable("hrfamily");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Birthday)
                    .IsRequired()
                    .HasColumnName("birthday")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("text");

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasColumnName("gender")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.HreducationId)
                    .IsRequired()
                    .HasColumnName("hreducation_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployementStatusId)
                    .IsRequired()
                    .HasColumnName("hremployement_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrfamilyStatusId)
                    .IsRequired()
                    .HasColumnName("hrfamily_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrinstitutionId)
                    .IsRequired()
                    .HasColumnName("hrinstitution_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrmarriedStatusId)
                    .IsRequired()
                    .HasColumnName("hrmarried_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrreligionId)
                    .IsRequired()
                    .HasColumnName("hrreligion_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MarriegeDate)
                    .IsRequired()
                    .HasColumnName("marriege_date")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PlaceBirth)
                    .IsRequired()
                    .HasColumnName("place_birth")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrfamilyStatus>(entity =>
            {
                entity.ToTable("hrfamily_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FamilyStatusName)
                    .HasColumnName("family_status_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrhelocation>(entity =>
            {
                entity.ToTable("hrhelocation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateLocation)
                    .HasColumnName("date_location")
                    .HasColumnType("date");

                entity.Property(e => e.HrheavyEquipmentId)
                    .IsRequired()
                    .HasColumnName("hrheavy_equipment_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Lattitude)
                    .IsRequired()
                    .HasColumnName("lattitude")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasColumnName("longitude")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeLocation)
                    .IsRequired()
                    .HasColumnName("time_location")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrholiday>(entity =>
            {
                entity.ToTable("hrholiday");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrinformation>(entity =>
            {
                entity.ToTable("hrinformation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Body)
                    .HasColumnName("body")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateInformation)
                    .HasColumnName("date_information")
                    .HasColumnType("date");

                entity.Property(e => e.FlagActive).HasColumnName("flag_active");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Title)
                    .HasColumnName("title")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrinstitution>(entity =>
            {
                entity.ToTable("hrinstitution");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.InstitutionAddress)
                    .IsRequired()
                    .HasColumnName("institution_address")
                    .HasColumnType("text");

                entity.Property(e => e.InstitutionName)
                    .IsRequired()
                    .HasColumnName("institution_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrinstitutionCompetence>(entity =>
            {
                entity.ToTable("hrinstitution_competence");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HinstitutionId)
                    .IsRequired()
                    .HasColumnName("hinstitution_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrcompetenceId)
                    .IsRequired()
                    .HasColumnName("hrcompetence_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrinstitutionLevel>(entity =>
            {
                entity.ToTable("hrinstitution_level");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CompetenceId)
                    .IsRequired()
                    .HasColumnName("competence_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrlevelPositionId)
                    .IsRequired()
                    .HasColumnName("hrlevel_position_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LevelReq)
                    .IsRequired()
                    .HasColumnName("level_req")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrjobInterview>(entity =>
            {
                entity.ToTable("hrjob_interview");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InterviewDate)
                    .HasColumnName("interview_date")
                    .HasColumnType("date");

                entity.Property(e => e.InterviewTime).HasColumnName("interview_time");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobInterviewDescription)
                    .HasColumnName("job_interview_description")
                    .HasColumnType("text");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.PlaceOfInterview)
                    .HasColumnName("place_of_interview")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrjobInterviewees>(entity =>
            {
                entity.ToTable("hrjob_interviewees");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrcandidateRecruitmentId)
                    .HasColumnName("hrcandidate_recruitment_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrjobInterviewer>(entity =>
            {
                entity.ToTable("hrjob_interviewer");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrjobPostingRequisition>(entity =>
            {
                entity.ToTable("hrjob_posting_requisition");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CandidateAgeRangeEnd).HasColumnName("candidate_age_range_end");

                entity.Property(e => e.CandidateAgeRangeStart).HasColumnName("candidate_age_range_start");

                entity.Property(e => e.CandidateExperience)
                    .HasColumnName("candidate_experience")
                    .HasColumnType("text");

                entity.Property(e => e.CandidateQualification)
                    .IsRequired()
                    .HasColumnName("candidate_qualification")
                    .HasColumnType("text");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnlegalEntityId)
                    .HasColumnName("gnlegal_entity_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobPostDescription)
                    .HasColumnName("job_post_description")
                    .HasColumnType("text");

                entity.Property(e => e.JobTitle)
                    .IsRequired()
                    .HasColumnName("job_title")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.NumberOfPosition).HasColumnName("number_of_position");

                entity.Property(e => e.PmcampaignTypeId)
                    .HasColumnName("pmcampaign_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryEnd).HasColumnName("salary_end");

                entity.Property(e => e.SalaryStart).HasColumnName("salary_start");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrjobRepository>(entity =>
            {
                entity.ToTable("hrjob_repository");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.GnexternalId)
                    .HasColumnName("gnexternal_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmpeFileId)
                    .HasColumnName("pmpe_file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Remark).HasColumnName("remark");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Version).HasColumnName("version");
            });

            modelBuilder.Entity<HrjobTest>(entity =>
            {
                entity.ToTable("hrjob_test");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobTestDescription)
                    .HasColumnName("job_test_description")
                    .HasColumnType("text");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.TestTitle)
                    .HasColumnName("test_title")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrleave>(entity =>
            {
                entity.ToTable("hrleave");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("approved_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("text");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrlevelPosition>(entity =>
            {
                entity.ToTable("hrlevel_position");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PositionLevel)
                    .IsRequired()
                    .HasColumnName("position_level")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrloanEmployee>(entity =>
            {
                entity.ToTable("hrloan_employee");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CountRepayment).HasColumnName("count_repayment");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateLoan)
                    .HasColumnName("date_loan")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MonthlyRepayment).HasColumnName("monthly_repayment");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("text");

                entity.Property(e => e.RepaymentEndDate)
                    .HasColumnName("repayment_end_date")
                    .HasColumnType("date");

                entity.Property(e => e.RepaymentStartDate)
                    .HasColumnName("repayment_start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Hrlocation>(entity =>
            {
                entity.ToTable("hrlocation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Lattitude).HasColumnName("lattitude");

                entity.Property(e => e.LocationName)
                    .IsRequired()
                    .HasColumnName("location_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude).HasColumnName("longitude");

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmarriedStatus>(entity =>
            {
                entity.ToTable("hrmarried_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.MarriedName)
                    .HasColumnName("married_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrmasterTax>(entity =>
            {
                entity.ToTable("hrmaster_tax");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BpjsTk)
                    .IsRequired()
                    .HasColumnName("bpjs_tk")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.BrutoCalculate)
                    .HasColumnName("bruto_calculate")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Calculatetax)
                    .IsRequired()
                    .HasColumnName("calculatetax")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PercentValue)
                    .HasColumnName("percent_value")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.TaxName)
                    .IsRequired()
                    .HasColumnName("tax_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasColumnName("value")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('0')");
            });

            modelBuilder.Entity<HrmedicalClaim>(entity =>
            {
                entity.ToTable("hrmedical_claim");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateClaim)
                    .HasColumnName("date_claim")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("text");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");

                entity.Property(e => e.YearPeriode)
                    .IsRequired()
                    .HasColumnName("year_periode")
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Hrnationality>(entity =>
            {
                entity.ToTable("hrnationality");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NationalityName)
                    .IsRequired()
                    .HasColumnName("nationality_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hroperator>(entity =>
            {
                entity.ToTable("hroperator");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasColumnName("username")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Hrovertime>(entity =>
            {
                entity.ToTable("hrovertime");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.Approval)
                    .HasColumnName("approval")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovalBy)
                    .HasColumnName("approval_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ApprovalDate)
                    .HasColumnName("approval_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ApprovedBy)
                    .HasColumnName("approved_by")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HourOvertime)
                    .IsRequired()
                    .HasColumnName("hour_overtime")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LastUpdate)
                    .HasColumnName("last_update")
                    .HasColumnType("datetime");

                entity.Property(e => e.OvertimeDate)
                    .HasColumnName("overtime_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Hrposition>(entity =>
            {
                entity.ToTable("hrposition");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Head).HasColumnName("head");

                entity.Property(e => e.HrlevelPositionId)
                    .IsRequired()
                    .HasColumnName("hrlevel_position_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PositionName)
                    .IsRequired()
                    .HasColumnName("position_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrpremiaskes>(entity =>
            {
                entity.ToTable("hrpremiaskes");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Hrprovince>(entity =>
            {
                entity.ToTable("hrprovince");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Code).HasColumnName("code");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Province)
                    .IsRequired()
                    .HasColumnName("province")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrreimburse>(entity =>
            {
                entity.ToTable("hrreimburse");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount).HasColumnName("amount");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Receipt)
                    .HasColumnName("receipt")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReimburseDate)
                    .HasColumnName("reimburse_date")
                    .HasColumnType("date");

                entity.Property(e => e.ReimburseDescription)
                    .HasColumnName("reimburse_description")
                    .HasColumnType("text");

                entity.Property(e => e.TypeReimburse)
                    .HasColumnName("type_reimburse")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrreligion>(entity =>
            {
                entity.ToTable("hrreligion");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ReligionName)
                    .IsRequired()
                    .HasColumnName("religion_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrresignation>(entity =>
            {
                entity.ToTable("hrresignation");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.NoticeDate)
                    .HasColumnName("notice_date")
                    .HasColumnType("date");

                entity.Property(e => e.ResignationDate)
                    .HasColumnName("resignation_date")
                    .HasColumnType("date");

                entity.Property(e => e.ResignationReason)
                    .HasColumnName("resignation_reason")
                    .HasColumnType("text");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrsalaryComponent>(entity =>
            {
                entity.ToTable("hrsalary_component");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.SalaryComponent)
                    .IsRequired()
                    .HasColumnName("salary_component")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrsalaryProcess>(entity =>
            {
                entity.ToTable("hrsalary_process");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BasicSalary).HasColumnName("basic_salary");

                entity.Property(e => e.Bonus).HasColumnName("bonus");

                entity.Property(e => e.Bruto).HasColumnName("bruto");

                entity.Property(e => e.BrutoOfyears).HasColumnName("bruto_ofyears");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HremployementStatusId)
                    .HasColumnName("hremployement_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrstatusEmployeeId)
                    .HasColumnName("hrstatus_employee_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Loan).HasColumnName("loan");

                entity.Property(e => e.Location).HasColumnName("location");

                entity.Property(e => e.Month).HasColumnName("month");

                entity.Property(e => e.Netto).HasColumnName("netto");

                entity.Property(e => e.NettoOfyears).HasColumnName("netto_ofyears");

                entity.Property(e => e.PkpOfyears).HasColumnName("pkp_ofyears");

                entity.Property(e => e.PositionCost).HasColumnName("position_cost");

                entity.Property(e => e.PtkpOfmonths).HasColumnName("ptkp_ofmonths");

                entity.Property(e => e.PtkpOfyears).HasColumnName("ptkp_ofyears");

                entity.Property(e => e.TaxNoNpwp).HasColumnName("tax_no_npwp");

                entity.Property(e => e.TaxOfmonths).HasColumnName("tax_ofmonths");

                entity.Property(e => e.TaxOfyears).HasColumnName("tax_ofyears");

                entity.Property(e => e.Transport).HasColumnName("transport");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Year).HasColumnName("year");
            });

            modelBuilder.Entity<HrsalaryProcessComponent>(entity =>
            {
                entity.ToTable("hrsalary_process_component");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ComponentName)
                    .HasColumnName("component_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.HremployeeBasicInfoId).HasColumnName("hremployee_basic_info_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Month).HasColumnName("month");

                entity.Property(e => e.TaxId).HasColumnName("tax_id");

                entity.Property(e => e.Type)
                    .HasColumnName("type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("date");

                entity.Property(e => e.Value).HasColumnName("value");

                entity.Property(e => e.ValueYear).HasColumnName("value_year");

                entity.Property(e => e.Year).HasColumnName("year");
            });

            modelBuilder.Entity<Hrseminar>(entity =>
            {
                entity.ToTable("hrseminar");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ContactNo)
                    .IsRequired()
                    .HasColumnName("contact_no")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CoverSeminar)
                    .IsRequired()
                    .HasColumnName("cover_seminar")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.HrcompetenceId)
                    .IsRequired()
                    .HasColumnName("hrcompetence_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PersonInCharge)
                    .IsRequired()
                    .HasColumnName("person_in_charge")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SeminarAddress)
                    .IsRequired()
                    .HasColumnName("seminar_address")
                    .HasColumnType("text");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrstatusEmployee>(entity =>
            {
                entity.ToTable("hrstatus_employee");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Compare)
                    .HasColumnName("compare")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrlevelPositionId)
                    .HasColumnName("hrlevel_position_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Month)
                    .HasColumnName("month")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ptkp)
                    .HasColumnName("ptkp")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Tax)
                    .HasColumnName("tax")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrstatusTax>(entity =>
            {
                entity.ToTable("hrstatus_tax");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrmasterTaxId)
                    .HasColumnName("hrmaster_tax_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TaxStatus)
                    .HasColumnName("tax_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrtermination>(entity =>
            {
                entity.ToTable("hrtermination");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Reason)
                    .IsRequired()
                    .HasColumnName("reason")
                    .HasColumnType("text");

                entity.Property(e => e.TerminationDate)
                    .HasColumnName("termination_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrtrainingRequisition>(entity =>
            {
                entity.ToTable("hrtraining_requisition");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Preferlocation)
                    .HasColumnName("preferlocation")
                    .HasColumnType("text");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.TrainingDescription)
                    .HasColumnName("training_description")
                    .HasColumnType("text");

                entity.Property(e => e.Trainingname)
                    .HasColumnName("trainingname")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Hrtransport>(entity =>
            {
                entity.ToTable("hrtransport");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate)
                    .HasColumnName("end_date")
                    .HasColumnType("date");

                entity.Property(e => e.HremployeeBasicInfoId)
                    .IsRequired()
                    .HasColumnName("hremployee_basic_info_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<Hrtravel>(entity =>
            {
                entity.ToTable("hrtravel");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ActualTravelBudget).HasColumnName("actual_travel_budget");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExpectedTravelBudget).HasColumnName("expected_travel_budget");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PurposeOfVisit)
                    .HasColumnName("purpose_of_visit")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TravelDescription)
                    .HasColumnName("travel_description")
                    .HasColumnType("text");

                entity.Property(e => e.TravelEndDate)
                    .HasColumnName("travel_end_date")
                    .HasColumnType("date");

                entity.Property(e => e.TravelStartDate)
                    .HasColumnName("travel_start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrtravelDestination>(entity =>
            {
                entity.ToTable("hrtravel_destination");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ArrangementType)
                    .HasColumnName("arrangement_type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrtravelId)
                    .HasColumnName("hrtravel_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PlaceOfVisit)
                    .HasColumnName("place_of_visit")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TravelMode)
                    .HasColumnName("travel_mode")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrtypeHeavyEquipment>(entity =>
            {
                entity.ToTable("hrtype_heavy_equipment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HeavyEquipmentType)
                    .IsRequired()
                    .HasColumnName("heavy_equipment_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<HrworkingEffective>(entity =>
            {
                entity.ToTable("hrworking_effective");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FromTime)
                    .IsRequired()
                    .HasColumnName("from_time")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GnbranchCompanyId)
                    .IsRequired()
                    .HasColumnName("gnbranch_company_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ToTime)
                    .IsRequired()
                    .HasColumnName("to_time")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmapprovalType>(entity =>
            {
                entity.ToTable("pmapproval_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ApprovalType)
                    .HasColumnName("approval_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmcampaignProgress>(entity =>
            {
                entity.ToTable("pmcampaign_progress");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CampaignProgress)
                    .HasColumnName("campaign_progress")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmcampaignStatus>(entity =>
            {
                entity.ToTable("pmcampaign_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CampaignStatus)
                    .HasColumnName("campaign_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.GnroleId)
                    .HasColumnName("gnrole_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmcampaignType>(entity =>
            {
                entity.ToTable("pmcampaign_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CampaignType)
                    .HasColumnName("campaign_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmcentralResourceAssigment>(entity =>
            {
                entity.ToTable("pmcentral_resource_assigment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnapprovalStatusId)
                    .HasColumnName("fnapproval_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Pmchannel>(entity =>
            {
                entity.ToTable("pmchannel");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Channel)
                    .HasColumnName("channel")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GncentralResourceId).HasColumnName("gncentral_resource_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Note)
                    .HasColumnName("note")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<PmClientBrief>(entity =>
            {
                entity.ToTable("pmclient_brief");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Brand)
                    .HasColumnName("brand")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BusinessGroup)
                    .HasColumnName("business_group")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CampaignName)
                    .HasColumnName("campaign_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CampaignProgressId).HasColumnName("campaign_progress_id");

                entity.Property(e => e.CampaignStatusId).HasColumnName("campaign_status_id");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCreate)
                    .HasColumnName("date_create")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deadline)
                    .HasColumnName("deadline")
                    .HasColumnType("datetime");

                entity.Property(e => e.FinalDelivery)
                    .HasColumnName("final_delivery")
                    .HasColumnType("datetime");

                entity.Property(e => e.FinishCampaign)
                    .HasColumnName("finish_campaign")
                    .HasColumnType("datetime");

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrnationalityId)
                    .IsRequired()
                    .HasColumnName("hrnationality_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmAssigmentId).HasColumnName("pm_assigment_id");

                entity.Property(e => e.PmcampaignProgressId).HasColumnName("pmcampaign_progress_id");

                entity.Property(e => e.PmcampaignTypeId)
                    .HasColumnName("pmcampaign_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefFileId)
                    .HasColumnName("pmclient_brief_file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobCategoryId)
                    .HasColumnName("pmjob_category_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobPeId).HasColumnName("pmjob_pe_id");

                entity.Property(e => e.PmproposalFileId).HasColumnName("pmproposal_file_id");

                entity.Property(e => e.PmproposeTypeId)
                    .HasColumnName("pmpropose_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ResponsiblePerson).HasColumnName("responsible_person");

                entity.Property(e => e.StartCampaign)
                    .HasColumnName("start_campaign")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmClientBriefAccount>(entity =>
            {
                entity.ToTable("pmclient_brief_account");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnapprovalStatusId)
                    .HasColumnName("fnapproval_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnaccountManagementId)
                    .HasColumnName("gnaccount_management_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmClientBriefContent>(entity =>
            {
                entity.ToTable("pmclient_brief_content");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Contents)
                    .HasColumnName("contents")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Cover)
                    .HasColumnName("cover")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmclientBriefFile>(entity =>
            {
                entity.ToTable("pmclient_brief_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmClientBriefJobRequest>(entity =>
            {
                entity.ToTable("pmclient_brief_job_request");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BranchcompanyId).HasColumnName("branchcompany_id");

                entity.Property(e => e.CandidateAgeRangeEnd).HasColumnName("candidate_age_range_end");

                entity.Property(e => e.CandidateAgeRangeStart).HasColumnName("candidate_age_range_start");

                entity.Property(e => e.CandidateExperience)
                    .HasColumnName("candidate_experience")
                    .HasColumnType("text");

                entity.Property(e => e.CandidateQualification)
                    .IsRequired()
                    .HasColumnName("candidate_qualification")
                    .HasColumnType("text");

                entity.Property(e => e.CloseDate)
                    .HasColumnName("close_date")
                    .HasColumnType("date");

                entity.Property(e => e.Code)
                    .HasColumnName("code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FnapprovalStatusId)
                    .HasColumnName("fnapproval_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobPostDescription)
                    .HasColumnName("job_post_description")
                    .HasColumnType("text");

                entity.Property(e => e.JobTitle).HasColumnName("job_title");

                entity.Property(e => e.JobType)
                    .HasColumnName("job_type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.NumberOfPosition).HasColumnName("number_of_position");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalaryEnd).HasColumnName("salary_end");

                entity.Property(e => e.SalaryStart).HasColumnName("salary_start");

                entity.Property(e => e.StartDate)
                    .HasColumnName("start_date")
                    .HasColumnType("date");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmclientFeedback>(entity =>
            {
                entity.ToTable("pmclient_feedback");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmapprovalTypeId)
                    .HasColumnName("pmapproval_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientFeedbackFileId)
                    .HasColumnName("pmclient_feedback_file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmclientFeedbackFile>(entity =>
            {
                entity.ToTable("pmclient_feedback_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmcreativeBrief>(entity =>
            {
                entity.ToTable("pmcreative_brief");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountClientId)
                    .HasColumnName("account_client_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCreate)
                    .HasColumnName("date_create")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deadline)
                    .HasColumnName("deadline")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PlannerId)
                    .HasColumnName("planner_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmcreativeBriefFileId)
                    .HasColumnName("pmcreative_brief_file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmstatusAssignedId)
                    .HasColumnName("pmstatus_assigned_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmcreativeBriefFile>(entity =>
            {
                entity.ToTable("pmcreative_brief_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmdcCoalProduction>(entity =>
            {
                entity.ToTable("pmdc_coal_production");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BucketTrip)
                    .IsRequired()
                    .HasColumnName("bucket_trip")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Fleet)
                    .IsRequired()
                    .HasColumnName("fleet")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HaulingUnit).HasColumnName("hauling_unit");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shift)
                    .IsRequired()
                    .HasColumnName("shift")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Tonase)
                    .IsRequired()
                    .HasColumnName("tonase")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Trip)
                    .IsRequired()
                    .HasColumnName("trip")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode).HasColumnName("unit_code");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmdcFuelConsumption>(entity =>
            {
                entity.ToTable("pmdc_fuel_consumption");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Fuel).HasColumnName("fuel");

                entity.Property(e => e.HmDifference).HasColumnName("hm_difference");

                entity.Property(e => e.HmFinish).HasColumnName("hm_finish");

                entity.Property(e => e.HmStart).HasColumnName("hm_start");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shift)
                    .IsRequired()
                    .HasColumnName("shift")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode).HasColumnName("unit_code");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmdcHeavyEquipment>(entity =>
            {
                entity.ToTable("pmdc_heavy_equipment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.HourFinish).HasColumnName("hour_finish");

                entity.Property(e => e.HourStart).HasColumnName("hour_start");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Lattitude).HasColumnName("lattitude");

                entity.Property(e => e.Location).HasColumnName("location");

                entity.Property(e => e.Longitude).HasColumnName("longitude");

                entity.Property(e => e.Operator).HasColumnName("operator");

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shift)
                    .IsRequired()
                    .HasColumnName("shift")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TimeDifference).HasColumnName("time_difference");

                entity.Property(e => e.UnitCode).HasColumnName("unit_code");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Working).HasColumnName("working");
            });

            modelBuilder.Entity<PmdcobProduction>(entity =>
            {
                entity.ToTable("pmdcob_production");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BucketTrip)
                    .IsRequired()
                    .HasColumnName("bucket_trip")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Date)
                    .HasColumnName("date")
                    .HasColumnType("date");

                entity.Property(e => e.Fleet)
                    .IsRequired()
                    .HasColumnName("fleet")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HaulingUnit).HasColumnName("hauling_unit");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ObVolume)
                    .IsRequired()
                    .HasColumnName("ob_volume")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shift)
                    .IsRequired()
                    .HasColumnName("shift")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Trip)
                    .IsRequired()
                    .HasColumnName("trip")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UnitCode).HasColumnName("unit_code");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmdeliverablesFile>(entity =>
            {
                entity.ToTable("pmdeliverables_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmfileType>(entity =>
            {
                entity.ToTable("pmfile_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmheavyEquipment>(entity =>
            {
                entity.ToTable("pmheavy_equipment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Bcm)
                    .IsRequired()
                    .HasColumnName("bcm")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Height)
                    .IsRequired()
                    .HasColumnName("height")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.HrassetId)
                    .IsRequired()
                    .HasColumnName("hrasset_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrtypeHeavyEquipmentId)
                    .IsRequired()
                    .HasColumnName("hrtype_heavy_equipment_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Low)
                    .IsRequired()
                    .HasColumnName("low")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.Medium)
                    .IsRequired()
                    .HasColumnName("medium")
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.OwnerOfHeavyEquipment)
                    .IsRequired()
                    .HasColumnName("owner_of_heavy_equipment")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PmclientBriefId)
                    .IsRequired()
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNumber)
                    .IsRequired()
                    .HasColumnName("serial_number")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Spesification)
                    .IsRequired()
                    .HasColumnName("spesification")
                    .HasColumnType("text");

                entity.Property(e => e.Ton)
                    .IsRequired()
                    .HasColumnName("ton")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.YearOfHeavyEquipement)
                    .IsRequired()
                    .HasColumnName("year_of_heavy_equipement")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PmjobCategory>(entity =>
            {
                entity.ToTable("pmjob_category");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateBy)
                    .HasColumnName("create_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Jobcategory)
                    .HasColumnName("jobcategory")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobPa>(entity =>
            {
                entity.ToTable("pmjob_pa");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FnapprovalStatusId)
                    .HasColumnName("fnapproval_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Margin).HasColumnName("margin");

                entity.Property(e => e.PaCost)
                    .HasColumnName("pa_cost")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PaDate)
                    .HasColumnName("pa_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PaNumber).HasColumnName("pa_number");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobPe>(entity =>
            {
                entity.ToTable("pmjob_pe");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreateDate)
                    .HasColumnName("create_date")
                    .HasColumnType("date");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Detail)
                    .HasColumnName("detail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DueDate)
                    .HasColumnName("due_date")
                    .HasColumnType("date");

                entity.Property(e => e.FnapprovalStatusId).HasColumnName("fnapproval_status_id");

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Margin).HasColumnName("margin");

                entity.Property(e => e.PeCost)
                    .HasColumnName("pe_cost")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.PeDate)
                    .HasColumnName("pe_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PeLineItem).HasColumnName("pe_line_item");

                entity.Property(e => e.PeNumber).HasColumnName("pe_number");

                entity.Property(e => e.PmpeFileId)
                    .HasColumnName("pmpe_file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SubmittedBy)
                    .HasColumnName("submitted_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestApproval>(entity =>
            {
                entity.ToTable("pmjob_request_approval");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FnapprovalStatusId)
                    .HasColumnName("fnapproval_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnaccountManagementId)
                    .HasColumnName("gnaccount_management_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmapprovalTypeId)
                    .HasColumnName("pmapproval_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestComment>(entity =>
            {
                entity.ToTable("pmjob_request_comment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Comment)
                    .HasColumnName("comment")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CommentDate)
                    .HasColumnName("comment_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnuserId).HasColumnName("gnuser_id");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmjobRequestCommentFileId)
                    .HasColumnName("pmjob_request_comment_file_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobRequestTaskId)
                    .HasColumnName("pmjob_request_task_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestCommentFile>(entity =>
            {
                entity.ToTable("pmjob_request_comment_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmjobRequestCommentId)
                    .HasColumnName("pmjob_request_comment_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadBy)
                    .HasColumnName("upload_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestCr>(entity =>
            {
                entity.ToTable("pmjob_request_cr");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AccountClient)
                    .HasColumnName("account_client")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DateCreate)
                    .HasColumnName("date_create")
                    .HasColumnType("datetime");

                entity.Property(e => e.Deadline)
                    .HasColumnName("deadline")
                    .HasColumnType("datetime");

                entity.Property(e => e.GncentralResourceId)
                    .HasColumnName("gncentral_resource_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobRequestCrName)
                    .HasColumnName("job_request_cr_name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmId)
                    .HasColumnName("pm_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmcampaignProgressId)
                    .HasColumnName("pmcampaign_progress_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmcreativeBriefId)
                    .HasColumnName("pmcreative_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmfileTypeId)
                    .HasColumnName("pmfile_type_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobCategoryId)
                    .HasColumnName("pmjob_category_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobStatusId).HasColumnName("pmjob_status_id");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestFile>(entity =>
            {
                entity.ToTable("pmjob_request_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestTask>(entity =>
            {
                entity.ToTable("pmjob_request_task");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.DeadlineTask)
                    .HasColumnName("deadline_task")
                    .HasColumnType("date");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmtaskPriorityId)
                    .HasColumnName("pmtask_priority_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TaskName)
                    .HasColumnName("task_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestTaskUser>(entity =>
            {
                entity.ToTable("pmjob_request_task_user");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.GnuserId)
                    .HasColumnName("gnuser_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmjobRequestTaskId)
                    .HasColumnName("pmjob_request_task_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TaskUser)
                    .HasColumnName("task_user")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmjobRequestTimesheet>(entity =>
            {
                entity.ToTable("pmjob_request_timesheet");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateWorksheet)
                    .HasColumnName("date_worksheet")
                    .HasColumnType("date");

                entity.Property(e => e.EndTime).HasColumnName("end_time");

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text");

                entity.Property(e => e.StartTime).HasColumnName("start_time");

                entity.Property(e => e.Task)
                    .HasColumnName("task")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.WorksheetDescription)
                    .HasColumnName("worksheet_description")
                    .HasColumnType("text");
            });

            modelBuilder.Entity<PmjobStatus>(entity =>
            {
                entity.ToTable("pmjob_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.JobStatus)
                    .HasColumnName("job_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Pmpackage>(entity =>
            {
                entity.ToTable("pmpackage");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Package)
                    .HasColumnName("package")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobCategoryId).HasColumnName("pmjob_category_id");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<PmpeFile>(entity =>
            {
                entity.ToTable("pmpe_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy).HasColumnName("created_by");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy).HasColumnName("update_by");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmpeTemplate>(entity =>
            {
                entity.ToTable("pmpe_template");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PeCostPerqty).HasColumnName("pe_cost_perqty");

                entity.Property(e => e.PeItemQty).HasColumnName("pe_item_qty");

                entity.Property(e => e.PeLineItem).HasColumnName("pe_line_item");

                entity.Property(e => e.PeNumber).HasColumnName("pe_number");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmpettyCash>(entity =>
            {
                entity.ToTable("pmpetty_cash");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FntransferStatusId)
                    .HasColumnName("fntransfer_status_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PettyAmount).HasColumnName("petty_amount");

                entity.Property(e => e.PettyDate)
                    .HasColumnName("petty_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.PmjobRequestTaskId)
                    .HasColumnName("pmjob_request_task_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferBankAccname)
                    .HasColumnName("transfer_bank_accname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TransferBankAccno)
                    .HasColumnName("transfer_bank_accno")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<Pmpo>(entity =>
            {
                entity.ToTable("pmpo");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Discount).HasColumnName("discount");

                entity.Property(e => e.GnexternalVendorId)
                    .HasColumnName("gnexternal_vendor_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Payment)
                    .HasColumnName("payment")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoDate)
                    .HasColumnName("po_date")
                    .HasColumnType("date");

                entity.Property(e => e.PoDueDate)
                    .HasColumnName("po_due_date")
                    .HasColumnType("date");

                entity.Property(e => e.PoLineItem).HasColumnName("po_line_item");

                entity.Property(e => e.PricePerQty).HasColumnName("price_per_qty");

                entity.Property(e => e.Qty).HasColumnName("qty");

                entity.Property(e => e.Tax).HasColumnName("tax");

                entity.Property(e => e.Total).HasColumnName("total");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmproposalFile>(entity =>
            {
                entity.ToTable("pmproposal_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmproposeType>(entity =>
            {
                entity.ToTable("pmpropose_type");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.ProposeType)
                    .HasColumnName("propose_type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmrateCard>(entity =>
            {
                entity.ToTable("pmrate_card");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Job)
                    .HasColumnName("job")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobCategoryPeId)
                    .HasColumnName("pmjob_category_pe_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PmjobPeId)
                    .HasColumnName("pmjob_pe_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RateCardCode)
                    .HasColumnName("rate_card_code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Value).HasColumnName("value");
            });

            modelBuilder.Entity<PmreportFile>(entity =>
            {
                entity.ToTable("pmreport_file");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FileName)
                    .HasColumnName("file_name")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasColumnName("file_path")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize).HasColumnName("file_size");

                entity.Property(e => e.FileType)
                    .HasColumnName("file_type")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Flag).HasColumnName("flag");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.UploadDate)
                    .HasColumnName("upload_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmstatusAssigned>(entity =>
            {
                entity.ToTable("pmstatus_assigned");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.StatusAssigned)
                    .HasColumnName("status_assigned")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmsubCampaign>(entity =>
            {
                entity.ToTable("pmsub_campaign");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.HrjobPostingRequisitionId)
                    .HasColumnName("hrjob_posting_requisition_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PmclientBriefId)
                    .HasColumnName("pmclient_brief_id")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subcampaign)
                    .HasColumnName("subcampaign")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmtaskPriority>(entity =>
            {
                entity.ToTable("pmtask_priority");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TaskPriority)
                    .HasColumnName("task_priority")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<PmtaskStatus>(entity =>
            {
                entity.ToTable("pmtask_status");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("created_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Isdeleted)
                    .HasColumnName("isdeleted")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.TaskStatus)
                    .HasColumnName("task_status")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateBy)
                    .HasColumnName("update_by")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("update_date")
                    .HasColumnType("datetime");
            });
        }
    }
}
