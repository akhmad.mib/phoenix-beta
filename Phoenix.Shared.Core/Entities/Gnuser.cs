﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Gnuser : BaseEntity<string>
    {
        
        public string GnroleId { get; set; }
        public long? HremployeeBasicInfoId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string DigitalSignature { get; set; }
        public string Email { get; set; }
        public long? GnbusinessUnitId { get; set; }
        public long? GnexternalId { get; set; }
        public int? FlagActive { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
