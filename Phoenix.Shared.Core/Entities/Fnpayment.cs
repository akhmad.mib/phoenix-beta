﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class Fnpayment : BaseEntity<string>
    {
        
        public string FntransactionId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public string FntransactionTypeId { get; set; }
        public DateTime? EntryDate { get; set; }
        public string GnuserId { get; set; }
        public string FninvoiceId { get; set; }
        public int? CollectionAmount { get; set; }
        public string HrbankAccountId { get; set; }
        public string ChequeGiroNo { get; set; }
        public DateTime? ChequeGiroDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
