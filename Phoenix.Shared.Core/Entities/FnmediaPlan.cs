﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FnmediaPlan : BaseEntity<string>
    {
        
        public string GnexternalMediaVendorId { get; set; }
        public string GnexternalCustomerId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
        public int? AgencyFee { get; set; }
        public string AiringDetail { get; set; }
        public int? GrossAmount { get; set; }
        public double? Discount { get; set; }
        public double? NetAmount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
