﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobPa : BaseEntity<string> {
        
        public string HrjobPostingRequisitionId { get; set; }
        public int? PaNumber { get; set; }
        public string Detail { get; set; }
        public decimal? PaCost { get; set; }
        public double? Margin { get; set; }
        public DateTime? PaDate { get; set; }
        public string FnapprovalStatusId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
