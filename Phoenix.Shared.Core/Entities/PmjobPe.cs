﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobPe : BaseEntity<string> {
        
        public string HrjobPostingRequisitionId { get; set; }
        public string Detail { get; set; }
        public decimal? PeCost { get; set; }
        public double? Margin { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? FnapprovalStatusId { get; set; }
        public string SubmittedBy { get; set; }
        public int? PeNumber { get; set; }
        public DateTime? PeDate { get; set; }
        public int? PeLineItem { get; set; }
        public DateTime? DueDate { get; set; }
        public string PmpeFileId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
