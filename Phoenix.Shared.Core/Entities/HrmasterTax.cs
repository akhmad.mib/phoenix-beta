﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrmasterTax : BaseEntity<string>
    {
        
        public string Code { get; set; }
        public string TaxName { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Calculatetax { get; set; }
        public string BpjsTk { get; set; }
        public string PercentValue { get; set; }
        public string BrutoCalculate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
