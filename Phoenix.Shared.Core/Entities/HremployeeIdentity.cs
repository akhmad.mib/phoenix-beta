﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeIdentity : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public string NameCard { get; set; }
        public string NoCard { get; set; }
        public DateTime? Published { get; set; }
        public DateTime? Expired { get; set; }
        public string PublishedBy { get; set; }
        public string FileUpload { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
