﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FncashSettlement : BaseEntity<string>
    {
        
        public DateTime? SettlementDate { get; set; }
        public int? FlagSettlement { get; set; }
        public string FntransactionId { get; set; }
        public string PmclientBriefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
