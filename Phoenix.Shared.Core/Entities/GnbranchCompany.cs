﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnbranchCompany : BaseEntity<string>
    {
        
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string Province { get; set; }
        public string Gncity { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string DayWorking { get; set; }
        public DateTime? PayrollProcessDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
