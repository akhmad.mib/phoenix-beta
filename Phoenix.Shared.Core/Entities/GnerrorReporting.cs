﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnerrorReporting : BaseEntity<string>
    {
        
        public string GnuserId { get; set; }
        public string ContentNote { get; set; }
        public string Information { get; set; }
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
