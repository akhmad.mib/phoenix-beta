﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnuserToken : BaseEntity<string>
    {
        
        public string UserId { get; set; }
        public string Token { get; set; }
        public string Ipaddress { get; set; }
        public string Agent { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
