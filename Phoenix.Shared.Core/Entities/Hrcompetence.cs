﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities 
{
    public partial class Hrcompetence : BaseEntity<string>
    {
        
        public string CompetenceName { get; set; }
        public string Level1 { get; set; }
        public string Level2 { get; set; }
        public string Level3 { get; set; }
        public string Level4 { get; set; }
        public string Level5 { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
