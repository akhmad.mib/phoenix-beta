﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrovertime : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public DateTime OvertimeDate { get; set; }
        public string HourOvertime { get; set; }
        public int? Amount { get; set; }
        public string ApprovedBy { get; set; }
        public string Approval { get; set; }
        public string ApprovalBy { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? LastUpdate { get; set; }
        public string UpdateBy { get; set; }
        public int? Isdeleted { get; set; }
    }
}
