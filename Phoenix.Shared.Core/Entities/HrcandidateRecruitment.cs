﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrcandidateRecruitment : BaseEntity<string>
    {
        
        public string Fullname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateBirth { get; set; }
        public string Gender { get; set; }
        public string Nasionality { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string WorkNumber { get; set; }
        public string Address { get; set; }
        public string GncityId { get; set; }
        public string Province { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string Interest { get; set; }
        public string Achievement { get; set; }
        public string LastEducation { get; set; }
        public string Institusion { get; set; }
        public string Major { get; set; }
        public int LookingJob { get; set; }
        public string RelationPeople { get; set; }
        public string Description { get; set; }
        public string ProfileFile { get; set; }
        public string Notes { get; set; }
        public int ResumeScreening { get; set; }
        public string IdentityCard { get; set; }
        public string MaritialStatus { get; set; }
        public string Password { get; set; }
        public long? Religion { get; set; }
        public long? Married { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
