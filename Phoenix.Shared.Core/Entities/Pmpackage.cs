﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class Pmpackage : BaseEntity<string> {
        
        public string Package { get; set; }
        public long? PmjobCategoryId { get; set; }
        public string Description { get; set; }
        public int? Value { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
