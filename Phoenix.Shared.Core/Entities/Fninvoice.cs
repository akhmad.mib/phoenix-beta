﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class Fninvoice : BaseEntity<string>
    {
        
        public string HrjobPostingRequisitionId { get; set; }
        public string PmjobPeId { get; set; }
        public string GnexternalId { get; set; }
        public int? InvoiceAmount { get; set; }
        public string FninvoiceStatusId { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? InvoiceDueDate { get; set; }
        public DateTime? PaidDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
