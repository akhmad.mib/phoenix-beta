﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrbonus : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public string Title { get; set; }
        public int? Amount { get; set; }
        public DateTime? DateBonus { get; set; }
        public string BonusDescription { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
