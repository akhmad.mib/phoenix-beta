﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FnmediaOrder : BaseEntity<string>
    {
        
        public string GnexternalClientId { get; set; }
        public string PmpoId { get; set; }
        public DateTime? OrderDate { get; set; }
        public string GnexternalVendorId { get; set; }
        public long? ReferenceId { get; set; }
        public string OrderDetail { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
