﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hroperator : BaseEntity<string>
    {
        
        public string Username { get; set; }
        public string Password { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string PmclientBriefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int Isdeleted { get; set; }
    }
}
