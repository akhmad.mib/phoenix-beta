﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FncashAdvance : BaseEntity<string>
    {
        
        public int? AdvanceAmount { get; set; }
        public DateTime? AdvanceDate { get; set; }
        public string TransferBankAccno { get; set; }
        public string TransferBankAccname { get; set; }
        public string FntransferStatusId { get; set; }
        public string PmclientBriefId { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string GnexternalId { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
