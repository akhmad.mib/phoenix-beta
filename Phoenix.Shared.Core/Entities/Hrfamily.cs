﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrfamily : BaseEntity<string>
    {
        
        public string Name { get; set; }
        public string PlaceBirth { get; set; }
        public string Birthday { get; set; }
        public string Gender { get; set; }
        public string HrreligionId { get; set; }
        public string HrfamilyStatusId { get; set; }
        public string MarriegeDate { get; set; }
        public string HreducationId { get; set; }
        public string HremployementStatusId { get; set; }
        public string HrinstitutionId { get; set; }
        public string Description { get; set; }
        public string HrmarriedStatusId { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
