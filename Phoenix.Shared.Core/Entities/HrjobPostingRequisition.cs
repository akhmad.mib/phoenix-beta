﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrjobPostingRequisition : BaseEntity<string>
    {
        
        public string Code { get; set; }
        public string JobTitle { get; set; }
        public string PmcampaignTypeId { get; set; }
        public int? NumberOfPosition { get; set; }
        public int? CandidateAgeRangeStart { get; set; }
        public int? CandidateAgeRangeEnd { get; set; }
        public string GnlegalEntityId { get; set; }
        public int? SalaryStart { get; set; }
        public int? SalaryEnd { get; set; }
        public string CandidateQualification { get; set; }
        public string CandidateExperience { get; set; }
        public string JobPostDescription { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public string GncentralResourceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
