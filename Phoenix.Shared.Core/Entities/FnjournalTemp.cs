﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FnjournalTemp : BaseEntity<string>
    {
        
        public long? ReferenceId { get; set; }
        public DateTime? ReferenceDate { get; set; }
        public int? Qty { get; set; }
        public int? Unit { get; set; }
        public int? Price { get; set; }
        public string PmclientBriefId { get; set; }
        public string GnlegalEntityId { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string FnaccountcatAccountId { get; set; }
        public string FninvoiceId { get; set; }
        public long? PoId { get; set; }
        public int? BeginDebit { get; set; }
        public int? BeginCredit { get; set; }
        public int? BudgedDebit { get; set; }
        public int? BudgedCredit { get; set; }
        public int? MutasiDebet { get; set; }
        public int? MutasiCredit { get; set; }
        public int? EndingDebet { get; set; }
        public int? EndingCredit { get; set; }
        public int? Balance { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? FlagTime { get; set; }
        public int? UserId { get; set; }
        public int? Bunit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
