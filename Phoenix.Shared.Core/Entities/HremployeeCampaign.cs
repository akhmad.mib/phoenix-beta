﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeCampaign : BaseEntity<string>
    {
        
        public string PmclientBriefId { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string PmsubcampaignId { get; set; }
        public string Notes { get; set; }
        public string FileUpload { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
