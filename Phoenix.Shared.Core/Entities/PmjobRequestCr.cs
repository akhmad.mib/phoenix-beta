﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobRequestCr : BaseEntity<string> {
        
        public string HrjobPostingRequisitionId { get; set; }
        public string JobRequestCrName { get; set; }
        public long? PmjobStatusId { get; set; }
        public DateTime? Deadline { get; set; }
        public string PmfileTypeId { get; set; }
        public string PmcampaignProgressId { get; set; }
        public string PmjobCategoryId { get; set; }
        public DateTime? DateCreate { get; set; }
        public string GncentralResourceId { get; set; }
        public string AccountClient { get; set; }
        public string GnuserId { get; set; }
        public string PmId { get; set; }
        public string PmcreativeBriefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
