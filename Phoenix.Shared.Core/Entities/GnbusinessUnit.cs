﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnbusinessUnit : BaseEntity<string>
    {
        
        public string GnbusinessUnitId { get; set; }
        public string GnlegalEntityId { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
