﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnfileReportingError : BaseEntity<string>
    {
        
        public string FileId { get; set; }
        public string FileName { get; set; }
        public string Eksistensi { get; set; }
        public string Path { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
