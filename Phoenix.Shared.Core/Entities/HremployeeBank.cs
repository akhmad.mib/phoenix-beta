﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeBank : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string HrbankId { get; set; }
        public string HrsubBankId { get; set; }
        public string FncurrencyId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
