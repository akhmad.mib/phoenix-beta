﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrjobRepository : BaseEntity<string>
    {
        
        public string HrjobPostingRequisitionId { get; set; }
        public string PmpeFileId { get; set; }
        public int? Version { get; set; }
        public int? Remark { get; set; }
        public string GnexternalId { get; set; }
        public DateTime? Date { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
