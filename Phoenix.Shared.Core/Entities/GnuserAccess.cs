﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnuserAccess : BaseEntity<string>
    {
        
        public string GnmoduleId { get; set; }
        public string HrpositionId { get; set; }
        public string GnroleId { get; set; }
        public int? ActionInsert { get; set; }
        public int? ActionUpdate { get; set; }
        public int? ActionDelete { get; set; }
        public int? ActionView { get; set; }
        public string FntransactionId { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
