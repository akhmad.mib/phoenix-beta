﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeWarning : BaseEntity<string>
    {
        
        public string WarningTo { get; set; }
        public string WarningBy { get; set; }
        public DateTime? WarningDate { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
