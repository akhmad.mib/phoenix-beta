﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrcostCenterCampaign : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public long? GnexternalClientId { get; set; }
        public int? Cost { get; set; }
        public string PhoneNo { get; set; }
        public string Address { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
