﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FntransactionTemp : BaseEntity<string>
    {
        
        public string TransactionName { get; set; }
        public long? ReferenceId { get; set; }
        public string GnlegalEntityId { get; set; }
        public int? Qty { get; set; }
        public int? FlagPost { get; set; }
        public string GnbusinessUnitId { get; set; }
        public string GnuserId { get; set; }
        public string FntransferStatusId { get; set; }
        public string FninvoiceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
