﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FncashAdvanceBidding : BaseEntity<string>
    {
        
        public string HrjobPostingRequisitionId { get; set; }
        public string FncashTransferId { get; set; }
        public string PmjobPeId { get; set; }
        public int? AmountCash { get; set; }
        public DateTime? CashDate { get; set; }
        public string GnbusinessUnitApprovalId { get; set; }
        public long? FinanceApprovalId { get; set; }
        public long? ApprovalStatusId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
