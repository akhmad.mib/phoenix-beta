﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobRequestFile : BaseEntity<string> {
        
        public string FileName { get; set; }
        public string FileType { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public int? Flag { get; set; }
        public string FilePath { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
