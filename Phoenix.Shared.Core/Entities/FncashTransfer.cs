﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FncashTransfer : BaseEntity<string>
    {
        
        public int? TransferAmount { get; set; }
        public DateTime? TransferDate { get; set; }
        public string TransferBankAccno { get; set; }
        public string TransferBankAccname { get; set; }
        public string FntransferStatusId { get; set; }
        public string PmclientBriefId { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string GnexternalD { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
