﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Gnexternal : BaseEntity<string>
    {
        
        public string GnexternalTypeId { get; set; }
        public string ExternalName { get; set; }
        public string LegalName { get; set; }
        public string TaxRegNo { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public string CityId { get; set; }
        public string Nation { get; set; }
        public string Email { get; set; }
        public string PersonalInchage { get; set; }
        public string EmailCp { get; set; }
        public string HpCp { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string ShippingAddress { get; set; }
        public string BillingAddress { get; set; }
        public string Website { get; set; }
        public string SocialMedia { get; set; }
        public string MediaCategory { get; set; }
        public string AccountClient { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
