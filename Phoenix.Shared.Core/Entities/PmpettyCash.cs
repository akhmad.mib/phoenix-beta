﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmpettyCash : BaseEntity<string> {
        
        public int? PettyAmount { get; set; }
        public DateTime? PettyDate { get; set; }
        public string TransferBankAccno { get; set; }
        public string TransferBankAccname { get; set; }
        public string FntransferStatusId { get; set; }
        public string PmjobRequestTaskId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
