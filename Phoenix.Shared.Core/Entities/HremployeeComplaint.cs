﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeComplaint : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public string ComplaintTitle { get; set; }
        public DateTime? ComplaintDate { get; set; }
        public string ComplaintDescription { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
