﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrhelocation : BaseEntity<string>
    {
        
        public string HrheavyEquipmentId { get; set; }
        public string Lattitude { get; set; }
        public string Longitude { get; set; }
        public DateTime DateLocation { get; set; }
        public string TimeLocation { get; set; }
        public string PmclientBriefId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
