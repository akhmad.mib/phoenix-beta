﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrtravelDestination : BaseEntity<string>
    {
        
        public string HrtravelId { get; set; }
        public string PlaceOfVisit { get; set; }
        public string TravelMode { get; set; }
        public string ArrangementType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
