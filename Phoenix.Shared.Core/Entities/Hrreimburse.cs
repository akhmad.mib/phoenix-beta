﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrreimburse : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public int? Amount { get; set; }
        public DateTime? ReimburseDate { get; set; }
        public string TypeReimburse { get; set; }
        public string Receipt { get; set; }
        public string ReimburseDescription { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
