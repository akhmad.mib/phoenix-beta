﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrsalaryProcessComponent : BaseEntity<string>
    {
        
        public long? HremployeeBasicInfoId { get; set; }
        public string ComponentName { get; set; }
        public long? Value { get; set; }
        public long? ValueYear { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string Type { get; set; }
        public long? TaxId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
