﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrtravel : BaseEntity<string>
    {
        
        public string PurposeOfVisit { get; set; }
        public DateTime? TravelStartDate { get; set; }
        public DateTime? TravelEndDate { get; set; }
        public int? ExpectedTravelBudget { get; set; }
        public int? ActualTravelBudget { get; set; }
        public string TravelDescription { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
