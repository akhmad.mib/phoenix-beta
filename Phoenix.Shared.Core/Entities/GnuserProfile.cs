﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnuserProfile : BaseEntity<string>
    {
        
        public string GnuserId { get; set; }
        public string HrcompetenceId { get; set; }
        public string Photo { get; set; }
        public string HrtrainingRequisitionId { get; set; }
        public string Experience { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
