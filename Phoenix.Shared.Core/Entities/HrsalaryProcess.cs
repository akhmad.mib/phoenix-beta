﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrsalaryProcess : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public string HremployementStatusId { get; set; }
        public string HrstatusEmployeeId { get; set; }
        public int? BasicSalary { get; set; }
        public int? Transport { get; set; }
        public int? Loan { get; set; }
        public int? Bonus { get; set; }
        public int? Bruto { get; set; }
        public int? BrutoOfyears { get; set; }
        public int? PositionCost { get; set; }
        public int? Netto { get; set; }
        public int? NettoOfyears { get; set; }
        public int? PtkpOfmonths { get; set; }
        public int? PtkpOfyears { get; set; }
        public int? PkpOfyears { get; set; }
        public int? TaxOfyears { get; set; }
        public int? TaxOfmonths { get; set; }
        public int? TaxNoNpwp { get; set; }
        public int? Location { get; set; }
        public int? Month { get; set; }
        public int? Year { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
