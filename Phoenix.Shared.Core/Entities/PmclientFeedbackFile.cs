﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmclientFeedbackFile : BaseEntity<string> {
        
        public string FileName { get; set; }
        public string FileType { get; set; }
        public long? FileSize { get; set; }
        public DateTime? UploadDate { get; set; }
        public int? Flag { get; set; }
        public string FilePath { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
