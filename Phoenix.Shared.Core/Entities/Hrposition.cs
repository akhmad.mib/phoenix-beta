﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrposition : BaseEntity<string>
    {
        
        public string PositionName { get; set; }
        public string HrlevelPositionId { get; set; }
        public string GncentralResourceId { get; set; }
        public int? Head { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
