﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrcostCenter : BaseEntity<string>
    {
        
        public string CostId { get; set; }
        public string ActivityId { get; set; }
        public DateTime Periode { get; set; }
        public string Percentage { get; set; }
        public DateTime EndPeriode { get; set; }
        public string HremployeeBasicInfoId { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
