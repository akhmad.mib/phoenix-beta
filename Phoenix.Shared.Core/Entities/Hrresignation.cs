﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrresignation : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public DateTime? NoticeDate { get; set; }
        public DateTime? ResignationDate { get; set; }
        public string ResignationReason { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
