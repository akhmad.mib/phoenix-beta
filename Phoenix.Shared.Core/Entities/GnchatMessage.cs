﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnchatMessage : BaseEntity<string>
    {
        
        public string Messages { get; set; }
        public string FromId { get; set; }
        public string ToId { get; set; }
        public string ToGroup { get; set; }
        public DateTime? ChatSendtime { get; set; }
        public DateTime? ChatReadtime { get; set; }
        public DateTime? LastSend { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
