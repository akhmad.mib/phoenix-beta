﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobRequestTaskUser : BaseEntity<string> {
        
        public string PmjobRequestTaskId { get; set; }
        public string HrjobPostingRequisitionId { get; set; }
        public string TaskUser { get; set; }
        public string GnuserId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
