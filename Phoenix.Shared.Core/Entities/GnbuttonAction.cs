﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnbuttonAction : BaseEntity<string>
    {
        
        public string GnbuttonFlowId { get; set; }
        public string GnbuttonUserId { get; set; }
        public long? ButtonGroupUserId { get; set; }
        public string GnpageId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
