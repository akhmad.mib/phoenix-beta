﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities 
{
    public partial class Hrinformation : BaseEntity<string>
    {
        
        public string Title { get; set; }
        public string Body { get; set; }
        public DateTime? DateInformation { get; set; }
        public int? FlagActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
