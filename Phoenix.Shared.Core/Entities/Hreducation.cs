﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hreducation : BaseEntity<string>
    {
        
        public string HreducationLevelId { get; set; }
        public string HrinstitutionId { get; set; }
        public string Majors { get; set; }
        public string Address { get; set; }
        public string StartEducation { get; set; }
        public string EndEducation { get; set; }
        public long HremployeeBasicInfoId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
