﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class Pmpo : BaseEntity<string> {
        
        public DateTime? PoDate { get; set; }
        public DateTime? PoDueDate { get; set; }
        public string Payment { get; set; }
        public string GnexternalVendorId { get; set; }
        public int? Tax { get; set; }
        public double? Discount { get; set; }
        public int? PoLineItem { get; set; }
        public int? Qty { get; set; }
        public int? PricePerQty { get; set; }
        public int? Total { get; set; }
        public string HrjobPostingRequisitionId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
