﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class GnlegalEntity : BaseEntity<string>
    {
        
        public string LegalEntityName { get; set; }
        public string NavaStructureId { get; set; }
        public string NpwpNo { get; set; }
        public DateTime? Date { get; set; }
        public string DirectorName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
