﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HremployeeOut : BaseEntity<string>
    {
        
        public string HremployeeBasicInfoId { get; set; }
        public DateTime DateOut { get; set; }
        public string TimeOut { get; set; }
        public string TimeComeback { get; set; }
        public string Type { get; set; }
        public string Reason { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
