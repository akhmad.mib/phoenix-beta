﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class Hrachievement : BaseEntity<string>
    {
        
        public long? HremployeeBasicInfoId { get; set; }
        public string AchievementTitle { get; set; }
        public DateTime? AchievementDate { get; set; }
        public string AchievementDescription { get; set; }
        public string Notes { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
