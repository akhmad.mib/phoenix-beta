﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrcompetenceLevel : BaseEntity<string>
    {
        
        public int? LevelReq { get; set; }
        public string HrcompetenceId { get; set; }
        public string HrlevelPositionId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
