﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmdcFuelConsumption : BaseEntity<string> {
        
        public DateTime Date { get; set; }
        public string Shift { get; set; }
        public int UnitCode { get; set; }
        public int HmStart { get; set; }
        public int HmFinish { get; set; }
        public int Fuel { get; set; }
        public string PmclientBriefId { get; set; }
        public int HmDifference { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
