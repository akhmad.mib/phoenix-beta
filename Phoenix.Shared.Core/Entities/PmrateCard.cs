﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmrateCard : BaseEntity<string> {
        
        public string RateCardCode { get; set; }
        public string PmjobCategoryPeId { get; set; }
        public string Job { get; set; }
        public string Description { get; set; }
        public double? Value { get; set; }
        public string PmjobPeId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
