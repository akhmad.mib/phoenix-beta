﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmjobRequestCommentFile : BaseEntity<string> {
        
        public string PmjobRequestCommentId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public int? FileSize { get; set; }
        public int? Flag { get; set; }
        public string FilePath { get; set; }
        public string UploadBy { get; set; }
        public DateTime? UploadDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
