﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmpeTemplate : BaseEntity<string> {
        
        public int? PeItemQty { get; set; }
        public int? PeCostPerqty { get; set; }
        public int? PeLineItem { get; set; }
        public int? PeNumber { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
