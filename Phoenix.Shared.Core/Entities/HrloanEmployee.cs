﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrloanEmployee : BaseEntity<string>
    {
        
        public long HremployeeBasicInfoId { get; set; }
        public DateTime DateLoan { get; set; }
        public int Value { get; set; }
        public string Reason { get; set; }
        public int? MonthlyRepayment { get; set; }
        public DateTime? RepaymentStartDate { get; set; }
        public DateTime? RepaymentEndDate { get; set; }
        public int? CountRepayment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
