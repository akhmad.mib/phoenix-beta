﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrstatusEmployee : BaseEntity<string>
    {
        
        public string Name { get; set; }
        public string Ptkp { get; set; }
        public string HrlevelPositionId { get; set; }
        public string Tax { get; set; }
        public string Compare { get; set; }
        public string Month { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
