﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class FnmediaEstimate : BaseEntity<string>
    {
        
        public string GnexternalId { get; set; }
        public string FntransactionId { get; set; }
        public string FnapprovalStatus { get; set; }
        public DateTime? EstimateDate { get; set; }
        public string FnmediaPlanId { get; set; }
        public int? Amount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
