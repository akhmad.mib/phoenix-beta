﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities
{
    public partial class HrcompanyAccount : BaseEntity<string>
    {
        
        public string HrbankId { get; set; }
        public string HrbankbranchId { get; set; }
        public string HrbranchcomapanyId { get; set; }
        public string BankAccountId { get; set; }
        public string BranchName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
