﻿using System;
using System.Collections.Generic;

namespace Phoenix.Shared.Core.Entities{
    public partial class PmdcHeavyEquipment : BaseEntity<string> {
        
        public string Shift { get; set; }
        public int Operator { get; set; }
        public int Working { get; set; }
        public int UnitCode { get; set; }
        public TimeSpan HourStart { get; set; }
        public TimeSpan HourFinish { get; set; }
        public int Location { get; set; }
        public TimeSpan TimeDifference { get; set; }
        public string PmclientBriefId { get; set; }
        public DateTime Date { get; set; }
        public int Lattitude { get; set; }
        public int Longitude { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int? Isdeleted { get; set; }
    }
}
