﻿$(function () {
    InitApprovalTable(tableApproval, ajaxUrlIndex, paramsSearchIndex, columnsIndex);
    InitMyTaskTable(tableMyTask);
    InitDueTodayTable(tableDueToday);
    InitOverDueTable(tableOverDue);
});

var ajaxUrlIndex = API_PM + "/CurrencyBase/Search";
var ajaxUrlForm = WEB_PM + "/ProjectManagement/Home";
var ajaxUrlSave = API_PM + "/CurrencyBase/Create";
var ajaxUrlUpdate = API_PM + "/CurrencyBase/Update";
var ajaxUrlGetId = "/CurrencyBase/Get";
var ajaxUrlDeleteId = "/CurrencyBase/Delete";

var tableApproval = "tableApproval";
var tableMyTask = "tableMyTask";
var tableDueToday = "tableDueToday";
var tableOverDue = "tableOverDue";

var columnsApproval = [
    {
        "data": "Id",
        "title": "ID",
        "sClass": "ecol x20",
        orderable: false
    }
];
