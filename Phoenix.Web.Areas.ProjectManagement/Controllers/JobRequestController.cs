﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Shared.Constants;

namespace Phoenix.Web.Areas.ProjectManagement.Controllers
{
    [Area(PhoenixModule.PM)]
    public class JobRequestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List()
        {
            return View();
        }

        public IActionResult Detail()
        {
            return View();
        }

        public IActionResult JRDetail()
        {
            return View();
        }
    }
}