﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Phoenix.Web.Areas.ProjectManagement.Models;

namespace Phoenix.Web.Areas.ProjectManagement.Controllers
{
    [Area("ProjectManagement")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Notification()
        {
            return View();
        }
    }
}
